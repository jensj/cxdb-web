from __future__ import annotations

import json
import sys
from dataclasses import dataclass
from pathlib import Path
from typing import Generator

import numpy as np
from ase import Atoms
from ase.data import atomic_numbers, chemical_symbols

from camdweb.utils import read_atoms


@dataclass
class AtomsData:
    symbols: list[str]
    positions: np.ndarray
    cell: np.ndarray
    pbc: np.ndarray
    energy: float | None = None
    forces: np.ndarray | None = None
    stress: np.ndarray | None = None
    magmom: float | None = None
    magmoms: np.ndarray | None = None

    def __len__(self):
        return len(self.symbols)

    @classmethod
    def from_json(cls, path: Path | str) -> AtomsData:
        """Read from simplified ASE-json formatted file.

        Simplified meaning that ndarrays are written as plain lists
        without shape and dtype information.
        """
        if isinstance(path, str):
            path = Path(path)
        with path.open() as fd:
            dct = json.load(fd)['1']
        magmoms = dct.get('magmoms')
        forces = dct.get('forces')
        stress = dct.get('stress')
        atoms = AtomsData([chemical_symbols[Z] for Z in dct['numbers']],
                          np.array(dct['positions']),
                          np.array(dct['cell']),
                          np.array(dct['pbc']),
                          dct.get('energy'),
                          np.array(forces) if forces is not None else None,
                          np.array(stress) if stress is not None else None,
                          dct.get('magmom'),
                          np.array(magmoms) if magmoms is not None else None)
        return atoms

    def write_json(self, path: Path) -> None:
        """Write simplified ASE-json."""
        txt = json.dumps(self.as_dict(), indent=1)
        path.write_text(f'{{"1":\n{txt}}}\n')

    def as_dict(self):
        """Covert to dictionary with ndarray converted to lists.

        >>> a = AtomsData(['H'],
        ...               np.zeros((1, 3)),
        ...               np.eye(3),
        ...               np.zeros(3, bool),
        ...               energy=-1.0)
        >>> for key, val in a.as_dict().items():
        ...     print(key, val)
        numbers [1]
        positions [[0.0, 0.0, 0.0]]
        cell [[1.0, 0.0, 0.0], [0.0, 1.0, 0.0], [0.0, 0.0, 1.0]]
        pbc [False, False, False]
        energy -1.0
        """
        dct = {'numbers': self.atomic_numbers(),
               'positions': self.positions.tolist(),
               'cell': self.cell.tolist(),
               'pbc': self.pbc.tolist()}
        if self.energy is not None:
            dct['energy'] = self.energy
        if self.magmom is not None:
            dct['magmom'] = self.magmom
        if self.magmoms is not None:
            dct['magmoms'] = self.magmoms.tolist()
        if self.forces is not None:
            dct['forces'] = self.forces.tolist()
        if self.stress is not None:
            dct['stress'] = self.stress.tolist()
        return dct

    @staticmethod
    def from_atoms(atoms: Atoms) -> AtomsData:
        """Create AtomsData object from ASE Atoms object."""
        results = getattr(atoms.calc, 'results', {})
        return AtomsData(
            list(atoms.symbols),
            atoms.positions,
            np.array(atoms.cell),
            atoms.pbc,
            results.get('energy'),
            results.get('forces'),
            results.get('stress'),
            results.get('magmom'),
            results.get('magmoms'))

    def atomic_numbers(self) -> list[int]:
        return [atomic_numbers[symbol] for symbol in self.symbols]

    def atoms_object(self):
        """Convert to ASE Atoms object."""
        atoms = Atoms(self.symbols,
                      self.positions,
                      cell=self.cell,
                      pbc=self.pbc)
        results = {}
        if self.magmom is not None:
            results['magmom'] = self.magmom
        if self.magmoms is not None:
            results['magmoms'] = self.magmoms
        if self.energy is not None:
            results['energy'] = self.energy
        if self.forces is not None:
            results['forces'] = self.forces
        if self.stress is not None:
            results['stress'] = self.stress
        if results:
            atoms.calc = type(
                'Calc',
                (),
                {'results': results})()
        return atoms


def convert_to_json(fro: Path) -> None:
    """Convert from anything ASE can read to simplified json."""
    atoms = read_atoms(fro)
    to = fro.with_suffix('.json')
    AtomsData.from_atoms(atoms).write_json(to)


def _paths(arg: str) -> Generator[Path, None, None]:
    """Expand glob-petters."""
    if '*' in arg:
        yield from Path().glob(arg)
    else:
        yield Path(arg)


def main():
    """Use "python -m camdweb.atomsdata path ..." to convert to json."""
    n = 0
    for arg in sys.argv[1:]:
        for path in _paths(arg):
            convert_to_json(Path(arg))
            n += 1
    print('Converted files:', n)


if __name__ == '__main__':
    main()
