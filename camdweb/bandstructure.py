import dataclasses
from collections import defaultdict
from typing import List

import numpy as np
import plotly.graph_objs as go
from ase.dft.kpoints import BandPath
from scipy.integrate import trapezoid

from camdweb.c2db.asr_panel import Row
from camdweb.panels.panel import SkipPanel
from camdweb.figures import plotly2json


@dataclasses.dataclass
class PlotUtil:
    def layout(self, ylabel):
        return go.Layout(
            xaxis=self.xaxis(),
            yaxis=self.yaxis(title=ylabel),
            plot_bgcolor='white',
            hovermode='closest',
            margin=dict(t=70, r=20),
            font=dict(size=14),
            legend=dict(
                orientation='h',
                yanchor='bottom',
                y=1.01,
                xanchor='left',
                x=0.0,
                font=dict(size=14),
                itemsizing='constant',
                itemwidth=30))

    def xaxis(self):
        return go.layout.XAxis(
            title='k-points',
            range=[0, 1],
            ticks='',
            showticklabels=True,
            mirror=True,
            ticktext=self.kpoint_labels,
            tickvals=self.label_xcoords,
            **self.axisargs)

    def yaxis(self, title):
        return go.layout.YAxis(
            title=title,
            range=[self.emin, self.emax],
            zeroline=False,
            mirror='ticks',
            ticks='inside',
            tickwidth=2,
            zerolinewidth=2,
            **self.axisargs)

    def plot_bands(self, xcoords_k, energies_xk, name, marker):
        assert len(xcoords_k) == energies_xk.shape[-1]

        ndatasets = energies_xk.size // len(xcoords_k)
        xcoords_xk = np.tile(xcoords_k, ndatasets)

        return go.Scattergl(
            x=xcoords_xk.ravel(),
            y=energies_xk.ravel(),
            name=name,
            marker=marker,
            mode='markers',
            showlegend=True,
            hovertemplate='%{y:.3f} eV')

    def boringmarker(self, color):
        return dict(size=3, color=color)

    def fancymarker_and_also_colorbar(self, color):
        cbtitle = f'〈<i><b>S</b></i><sub>{self.spin_axisname}</sub>〉'

        return dict(
            size=3,
            color=color,
            colorscale='Viridis',
            showscale=True,
            colorbar=dict(
                tickmode='array',
                tickvals=[-1, 0, 1],
                ticktext=['-1', '0', '1'],
                title=dict(text=cbtitle,
                           side='right')
            ))


@dataclasses.dataclass
class PlotUtilPBE(PlotUtil):
    path: BandPath

    energy_nosoc_skn: np.ndarray
    energy_soc_mk: np.ndarray
    spin_zprojection_soc_mk: np.ndarray | None

    fermilevel: float | None
    emin: float
    emax: float
    spin_axisname: str | None

    xcname: str = 'PBE'  # XXX Should not take a default

    def __post_init__(self):
        xcoords, label_xcoords, labels = self.path.get_linear_kpoint_axis()

        self.xcoords = xcoords / xcoords[-1]
        self.kpoint_labels, self.label_xcoords = prettify_labels(
            labels, label_xcoords / xcoords[-1])

        self.axisargs = dict(
            showgrid=True,
            showline=True,
            linewidth=2,
            gridcolor='lightgrey',
            linecolor='black')

    def plot(self) -> go.Figure:
        """Plot band structure.

        This plots:
         * The ordinary (non-spin–orbit-coupled) band structure
         * The spin–orbit coupled band structure coloured by spin projection
         * Reference energy (Fermi level).
        """

        data = [self.plot_bands_boring()]
        if self.spin_zprojection_soc_mk is not None:
            data.append(self.plot_bands_fancy())
        if self.fermilevel is not None:
            data.append(self.plot_reference_energy_as_line())
            ylabel = '<i>E</i> - <i>E</i><sub>F</sub> [eV]'
        else:
            ylabel = '<i>E</i> - <i>E</i><sub>VBM</sub> [eV]'
        return plotly2json(go.Figure(
            data=data,
            layout=self.layout(ylabel=ylabel)))

    def subtract_reference_energy(self, vbm_reference):
        return dataclasses.replace(
            self,
            energy_nosoc_skn=self.energy_nosoc_skn - vbm_reference,
            energy_soc_mk=self.energy_soc_mk - vbm_reference,
            fermilevel=None if self.fermilevel is None
            else self.fermilevel - vbm_reference,
            emin=self.emin - vbm_reference,
            emax=self.emax - vbm_reference)

    def plot_bands_boring(self):
        return self.plot_bands(
            self.xcoords, self.energy_nosoc_skn.transpose(0, 2, 1),
            name=f'{self.xcname} no SOC',
            marker=self.boringmarker(color='#999999'))

    def plot_bands_fancy(self):
        perm = (-self.spin_zprojection_soc_mk).ravel().argsort()
        esoc_mk = self.energy_soc_mk.ravel()[perm]
        zsoc_mk = self.spin_zprojection_soc_mk.ravel()[perm]
        ndatasets = esoc_mk.size // len(self.xcoords)
        xcoords_mk = np.tile(self.xcoords, ndatasets)[perm]

        return self.plot_bands(
            xcoords_mk, esoc_mk, name=self.xcname,
            marker=self.fancymarker_and_also_colorbar(color=zsoc_mk))

    def plot_reference_energy_as_line(self):
        return go.Scatter(
            x=[0, 1],
            y=[self.fermilevel, self.fermilevel],
            mode='lines',
            line=dict(color=('rgb(0, 0, 0)'), width=2, dash='dash'),
            name='Fermi level',
            showlegend=False)


@dataclasses.dataclass
class PlotDatset:
    path: BandPath
    energy_soc_mk: np.ndarray
    fermilevel: float | None
    name: str
    color: str

    def __post_init__(self):
        xcoords, label_xcoords, labels = self.path.get_linear_kpoint_axis()

        self.xcoords = xcoords / xcoords[-1]

        self.kpoint_labels, self.label_xcoords = prettify_labels(
            labels, np.array([*label_xcoords]) / xcoords[-1])

    def ensureLabels(self, datasets: List['PlotDatset']):
        if len(datasets) == 0:
            return

        # Check that all datasets have the same kpoint labels and label xcoords
        # as this one.
        kpoint_labels = self.kpoint_labels
        label_xcoords = self.label_xcoords
        for dataset in datasets:
            if dataset.kpoint_labels != kpoint_labels:
                raise SkipPanel(f'kpoint labels do not match: {self.name} '
                                f'vs {dataset.name}')
            diff = np.array(dataset.label_xcoords) - label_xcoords
            if abs(diff).max() > 0.01:
                raise SkipPanel(f'label xcoords do not match: {self.name} '
                                f'vs {dataset.name}')

    def subtract_reference_energy(self, vbm_reference):
        return dataclasses.replace(
            self,
            energy_soc_mk=self.energy_soc_mk - vbm_reference,
            fermilevel=None if self.fermilevel is None
            else self.fermilevel - vbm_reference,
            name=self.name)


@dataclasses.dataclass
class PlotUtilComparison(PlotUtil):
    datasets: List[PlotDatset]
    emin: float
    emax: float

    def __post_init__(self):
        if len(self.datasets) > 1:
            self.datasets[0].ensureLabels(self.datasets[1:])

        self.label_xcoords = self.datasets[0].label_xcoords
        self.kpoint_labels = self.datasets[0].kpoint_labels

        self.axisargs = dict(
            showgrid=True,
            showline=True,
            linewidth=2,
            gridcolor='lightgrey',
            linecolor='black')

    def plot(self) -> go.Figure:
        """Plot band structure.

        This plots:
         * The given band structures
         * Reference energy (Fermi level).
        """

        data = self.plot_bands_boring()
        data.extend(self.plot_reference_energy_as_line())

        yl = f'<i>E</i> - <i>E</i><sub>VBM,{self.datasets[-1].name}</sub> [eV]'

        return plotly2json(go.Figure(
            data=data,
            layout=self.layout(ylabel=yl)))

    def subtract_reference_energy(self, vbm_reference):
        return dataclasses.replace(
            self,
            datasets=[dataset.subtract_reference_energy(vbm_reference)
                      for dataset in self.datasets],
            emin=self.emin - vbm_reference,
            emax=self.emax - vbm_reference)

    def plot_bands_boring(self):
        band_plots = []
        for dataset in self.datasets:
            band_plots.append(self.plot_bands(
                              dataset.xcoords, dataset.energy_soc_mk,
                              name=dataset.name,
                              marker=self.boringmarker(dataset.color)))

        return band_plots

    def plot_reference_energy_as_line(self):
        references = []
        for dataset in self.datasets:
            if dataset.fermilevel is None:
                continue
            references.append(go.Scatter(
                x=[0, 1],
                y=[dataset.fermilevel, dataset.fermilevel],
                mode='lines',
                line=dict(color=dataset.color, width=2, dash='dash'),
                name=f'{dataset.name} fermi level'))

        return references


def prettify_labels(orig_labels, label_xcoords):
    import re

    label_xcoords = [*label_xcoords]

    def pretty(kpt):
        if kpt == 'G':
            return 'Γ'

        # Convert Abc123 ----> Abc<sub>123</sub>:
        return re.sub('[0-9]+', lambda match:
                      rf'<sub>{match.group()}</sub>', kpt)

    labels = [pretty(name) for name in orig_labels]

    assert len(labels) == len(label_xcoords)

    i = 1
    while i < len(labels):
        if abs(label_xcoords[i - 1] - label_xcoords[i]) < 1e-6:
            # Merge two special points A and B at same kpoint into a composite
            # label "A,B" and shorten the list of labels:
            labels[i - 1] = f'{labels[i - 1]},{labels[i]}'
            labels.pop(i)
            label_xcoords.pop(i)
        else:
            i += 1

    return labels, label_xcoords


@dataclasses.dataclass
class PDOSData:
    """While this is an improvement over the original function, it is still far
    to easy to produce nonsense results. One should be particularly careful
    subtracting reference energies. Particularly, because some data comes from
    nosoc/soc and some from pdos and gs."""
    row: Row
    reference: float
    soc: bool
    results: str = 'results-asr.pdos.json'
    gsresults: str = 'results-asr.gs.json'

    def __post_init__(self):
        # Check if pdos data is stored in row
        self.pdos_soc_type = 'pdos_soc' if self.soc else 'pdos_nosoc'

        self.xlabel = 'Projected DOS [states / eV]'
        ylabel = "<i>E</i> - <i>E</i><sub>VBM</sub> [eV]"
        try:
            gap = self.row['gap']
        except AttributeError:
            gap = self.row.data[self.gsresults].get('gap')
        assert gap is not None
        if gap <= 0.0:
            ylabel = "<i>E</i> - <i>E</i><sub>F</sub> [eV]"
        self.ylabel = ylabel

    def plot(self):
        # check if pdos and soc pdos type in row data
        if (
                self.results in self.row.data and
                self.pdos_soc_type in self.row.data[self.results]
        ):
            data = self.row.data[self.results][self.pdos_soc_type]
        else:
            return
        e_e = data['energies'].copy()
        emin, emax = self.get_eminmax(efermi=data['efermi'])  # energy range of
        # plot in eV
        i1, i2 = abs(e_e - emin).argmin(), abs(e_e - emax).argmin()

        # Get color code
        pdos_syl = get_ordered_syl_dict(data['pdos_syl'], data['symbols'])
        color_yl = get_yl_colors(pdos_syl)

        # Figure out if pdos has been calculated for more than one spin channel
        spinpol = False
        for k in pdos_syl.keys():
            if int(k[0]) == 1:
                spinpol = True
                break

        # Plot pdos
        pdosint_s = defaultdict(float)
        plot_pdos = []
        for key in pdos_syl:
            pdos = pdos_syl[key]
            spin, symbol, lstr = key.split(',')
            spin = int(spin)
            sign = 1 if spin == 0 else -1

            # Integrate pdos to find suiting pdos range
            pdosint_s[spin] += trapezoid(y=pdos[i1:i2], x=e_e[i1:i2])
            # Label atomic symbol and angular momentum
            if spin == 0:
                label = '{} ({})'.format(symbol, lstr)
                color = dict(color=color_yl[key[2:]])
            else:
                label, color = None, None
            plot_pdos.append([self.smooth_y(y=pdos) * sign, e_e, label, color])

        # Set up axis limits
        xlim = self.set_xlim(pdosint_s=pdosint_s, spinpol=spinpol)

        return self.plotly_pdos(pdos=plot_pdos, ylim=[emin, emax], xlim=xlim)

    def plotly_pdos(self, pdos, xlim, ylim):
        import plotly.graph_objs as go
        axisargs = dict(showgrid=True, showline=True, linewidth=2,
                        gridcolor='lightgrey', linecolor='black')
        xaxis = go.layout.XAxis(
            title=self.xlabel, range=xlim, ticks='', showticklabels=True,
            mirror=True, **axisargs)
        yaxis = go.layout.YAxis(
            title=self.ylabel, mirror='ticks', ticks='inside', **axisargs,
            zeroline=False, tickwidth=2, zerolinewidth=2,
            range=[y - self.reference for y in ylim])
        layout = go.Layout(
            xaxis=xaxis, yaxis=yaxis, plot_bgcolor='white',
            margin=dict(t=70, r=20), font=dict(size=14), hovermode='closest',
            legend=dict(orientation='h', yanchor='bottom', xanchor='left',
                        x=0.0, font=dict(size=14), itemsizing='constant',
                        itemwidth=30, y=1.01))
        showlegend, data, reindex = True, [], int(len(pdos) / 2)
        for idx, (x, y, name, color) in enumerate(pdos):
            if color is None:
                showlegend, color = False, pdos[idx - reindex][-1]
            data.append(go.Scatter(x=x, y=y - self.reference, mode='lines',
                                   name=name,
                                   line=color, showlegend=showlegend))
        return plotly2json(go.Figure(data=data, layout=layout))

    def get_eminmax(self, efermi, evspan=3):
        data = self.row
        if not self.soc:
            data = self.row.data[self.gsresults]['gaps_nosoc']
            data = {k: v for k, v in data.items() if v is not None}
        emin = data.get('vbm', efermi) - evspan
        emax = data.get('cbm', efermi) + evspan
        return emin, emax

    def set_xlim(self, pdosint_s, spinpol):
        if spinpol:  # Use symmetric limits
            xmax = max(pdosint_s.values())
            return [-xmax * 0.5, xmax * 0.5]
        else:
            return [0, pdosint_s[0] * 0.5]

    def smooth_y(self, y, npts=3):
        return np.convolve(y, np.ones(npts) / npts, mode='same')


def get_yl_colors(dct_syl):
    # copy from asr.pdos
    from collections import OrderedDict

    from matplotlib import rcParams
    c, color_yl = 0, OrderedDict()
    mpl_colors = rcParams['axes.prop_cycle'].by_key()['color']
    for key in dct_syl:
        # Do not differentiate spin by color
        if int(key[0]) == 0:  # if spin is 0
            color_yl[key[2:]] = '{}'.format(mpl_colors[c])
            c += 1
            c = c % 10  # only 10 colors available in cycler
    return color_yl


def get_ordered_syl_dict(dct_syl, symbols):
    """Order a dictionary with syl keys.

    Parameters
    ----------
    dct_syl : dict
        Dictionary with keys f'{s},{y},{l}'
        (spin (s), chemical symbol (y), angular momentum (l))
    symbols : list
        Sort symbols after index in this list

    Returns
    -------
    outdct_syl : OrderedDict
        Sorted dct_syl

    """
    from collections import OrderedDict

    # Setup ssili (spin, symbol index, angular momentum index) key
    def ssili(syl):
        s, y, L = syl.split(',')
        # Symbols list can have multiple entries of the same symbol
        # ex. ['O', 'Fe', 'O']. In this case 'O' will have index 0 and
        # 'Fe' will have index 1.
        si = symbols.index(y)
        li = ['s', 'p', 'd', 'f'].index(L)
        return f'{s}{si}{li}'

    return OrderedDict(sorted(dct_syl.items(), key=lambda t: ssili(t[0])))
