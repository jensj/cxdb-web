from __future__ import annotations

import argparse
import json
from pathlib import Path

import rich.progress as progress

from camdweb.bidb.panels import BiDBAtomsPanel, BiDBBSBZPanel, BiDBStructure
from camdweb.html import (image, stoichiometry_input, Select, Range,
                          SearchFilter)
from camdweb.materials import Material, Materials
from camdweb.panels.misc import MiscPanel
from camdweb.web import CAMDApp
from camdweb.bidb.stackings import StackingsPanel
from camdweb.c2db.asr_panel import ASRPanel, read_result_file
from camdweb.utils import cod, doi, icsd, c2db


def read_material(path: Path,
                  uid: str) -> Material:
    material = Material.from_file(path / 'structure.xyz', uid)
    data = json.loads((path / 'data.json').read_text())
    material.columns.update(
        layergroup=data.pop('layer_group'),
        lgnum=data.pop('layer_group_number'),
        c2db_uid=data.pop('c2db_uid'),
        # pointgroup=data.get('pointgroup', None),
        # crystal_system=data.get('crystal_system', None),
        # bravais_type=data.get('bravais_type', None),
        # crystal_type=data.get('crystal_type', None)
    )
    material.columns.update(data)

    try:
        evac = read_result_file(path / 'results-asr.gs.json')['evac']
    except FileNotFoundError:
        pass
    else:
        material.columns['evac'] = evac
    return material


def main(argv: list[str] | None = None) -> CAMDApp:
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'pattern', nargs='*', default=['*/*/'],
        help='Glob-pattern: materials/<pattern>.  Default is "*/*".')
    args = parser.parse_args(argv)

    root = Path()
    p_layer = []
    for pattern in args.pattern:
        for p in (root / 'materials').glob(pattern):
            p_layer.append(p)

    mlist: list[Material] = []
    with progress.Progress() as pb:
        pid = pb.add_task('Reading materials:', total=len(p_layer))
        for bilayers in p_layer:
            tmp = []
            for bilayer in [p for p in bilayers.glob('*/') if p.is_dir()]:
                tmp.append(
                    read_material(bilayer, f'{bilayers.name}-{bilayer.name}'))
            for layer in tmp:
                layer.data.update(all_stackings=tmp)
            mlist.extend(tmp)
            pb.advance(pid)

    panels = [BiDBAtomsPanel(),
              BiDBStructure(),
              StackingsPanel(),
              BiDBBSBZPanel(),
              ASRPanel('fermisurface'),
              ASRPanel('raman'),
              MiscPanel()]

    materials = Materials(mlist, panels)

    materials.column_descriptions.update(
        label='Structure origin',
        c2db_uid='Monolayer (C2DB)',
        monolayer_uid='Monolayer ID',
        bilayer_uid='Bilayer ID',
        cod_id='COD id of parent bulk structure',
        icsd_id='ICSD id of parent bulk structure',
        magnetic='Magnetic',
        slide_stability='Slide stability',
        ebind='Binding energy [meV/Å<sup>2</sup>]',
        gap_pbe='Band gap [eV]',
        bravais_type='2D Bravais type',
        layergroup='Layer group',
        lgnum='Layer group number',
        pointgroup='Point group',
        has_inversion_symmetry='Inversion symmetry',
        number_of_layers='Number of layers',
        thickness='Thickness [Å]',
        interlayer_distance='Interlayer distance [Å]'
    )

    materials.html_formatters.update(
        cod_id=cod,
        icsd_id=icsd,
        doi=doi,
        c2db_uid=c2db)

    initial_columns = [
        'formula',
        'ebind',
        'slide_stability',
        'uid',
        'magnetic']

    app = CAMDApp(materials,
                  initial_columns=initial_columns,
                  initial_filter_string='slide_stability=Stable',
                  root=root)
    app.title = 'BiDB'
    app.logo = image('bidb-logo.png', alt='BiDB-logo')
    app.links = [
        ('CMR', 'https://cmr.fysik.dtu.dk'),
        ('About', 'https://cmr.fysik.dtu.dk/bidb/bidb.html')]

    app.form_parts[0] = SearchFilter(placeholder="Example: 'MoS2'")
    app.form_parts += [
        stoichiometry_input(materials.stoichiometries()),
        Range('Binding energy [meV/Å<sup>2</sup>]:', 'ebind'),
        Select('Slide stability:', 'slide_stability',
               ['', 'Stable', 'Unstable'], ['-', 'Stable', 'Unstable'],
               default='Stable'),
        Range('Band gap range [eV]:', 'gap_pbe'),
        Select('Magnetic:', 'magnetic', ['', '1', '0'],
               ['-', 'Yes', 'No'])]

    return app


def create_app():
    """Create the WSGI app."""
    app = main()
    return app.app


def check_all(pattern):  # pragma: no cover
    """Generate png-files."""
    bidb = main(pattern)
    for material in bidb.materials:
        print(material.uid)
        bidb.material_page(material.uid)


if __name__ == '__main__':
    main().app.run(host='0.0.0.0', port=8084, debug=True)
