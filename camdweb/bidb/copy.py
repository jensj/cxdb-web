"""
Create tree of materials with::

  $ mkdir MyBiDB
  $ cd MyBiDB
  $ cp /home/scratch3/jensj/CAMD-Web/BiDB/sample_with_duplicates.db .
  $ cp /home/scratch3/jensj/CAMD-Web/BiDB/folder_lists.json .
  $ python -m camdweb.bidb.copy sample_with_duplicates.db folder_lists.json
  UID_TRANSLATIONS

"""
import json
import shutil
import sys
from collections import defaultdict
from pathlib import Path

from ase import Atoms
from ase.db import connect
from ase.formula import Formula
from camdweb.utils import thickness
from camdweb.c2db.uid_translation import convert_uid

UID_MAP_PATH = 'uid_translation.json'


def copy_files(db_file: str,
               home: str = '/home/',
               uid_map_path: str = UID_MAP_PATH) -> None:
    missing_uids = set()

    # load old to new uid mapping
    uid_map = {}
    if Path(uid_map_path).is_file():
        with Path(uid_map_path).open('r') as uid_f:
            uid_map = json.load(uid_f)

    monolayers: defaultdict[str, dict[str, int]] = defaultdict(dict)
    for row in connect(db_file).select():
        f = Formula(row.formula)
        ab, xy, n = f.stoichiometry()
        n //= row.number_of_layers
        name = f'materials/{ab}/{n}{xy}'
        ids = monolayers[name]
        if row.monolayer_uid in ids:
            i = ids[row.monolayer_uid]
        else:
            i = len(ids) + 1
            ids[row.monolayer_uid] = i
        folder = Path(name + f'-{i}')
        # Stop collecting monolayer data
        if row.number_of_layers == 1:
            continue
        else:
            # remove bilayers with slide_stability = None from web pages
            if row.slide_stability in ['None', 'Badfit']:
                continue
            folder /= row.bilayer_uid.split('-', 2)[2]
        folder.mkdir(exist_ok=True, parents=True)
        atoms = row.toatoms()
        atoms.write(folder / 'structure.xyz')

        # These values never have data, so why are we even putting them here:
        # interlayer_magnetic_state, interlayer_magnetic_exchange,
        # dynamically_stable, ehull

        # refine displayed slide stability classifications
        # Stable-* -> Stable, Unstable-* -> Unstable, "-": Unverified,
        data = dict(row.key_value_pairs)
        if row.slide_stability.startswith('Stable'):
            data['slide_stability'] = 'Stable'
        elif row.slide_stability.startswith('Unstable'):
            data['slide_stability'] = 'Unstable'
        elif row.slide_stability == '-':
            data['slide_stability'] = 'Unverified'
        data['interlayer_distance'] = interlayer_distance(atoms)
        data['thickness'] = thickness(atoms)

        # get only one binding energy, the one we care to display
        tmp = [data.pop(k, '-') for k in ['binding_energy_gs',
                                          'binding_energy_zscan']]
        data['ebind'] = [v * 1000 for v in tmp if v != '-'][0]

        data['label'] = 'original24'
        try:
            data['c2db_uid'] = convert_uid(row.monolayer_uid, uid_map)
        except ValueError:
            missing_uids.add(row.monolayer_uid)
            data['c2db_uid'] = row.monolayer_uid

        # drop this information from data
        [data.pop(key, None) for key in ['space_group', 'space_group_number']]

        # write the data to the json file
        (folder / 'data.json').write_text(json.dumps(data, indent=2))
        dir = Path(row.folder.replace('/home/', home))

        if 0:
            for file in dir.glob('results-asr.*.json'):
                (folder / file.name).write_bytes(file.read_bytes())

        # write the pdos to data.json
        for name in ['pdos', 'bandstructure', 'projected_bandstructure',
                     'fermisurface', 'raman']:
            file = dir / f'results-asr.{name}.json'
            if file.is_file():
                txt = file.read_text()
                try:
                    json.loads(txt)
                except json.JSONDecodeError:
                    print(file)
                else:
                    (folder / file.name).write_text(txt)

    # Put logo in the right place:
    logo = Path('bidb-logo.png')
    if not logo.is_file():
        shutil.copyfile(Path(__file__).parent / 'logo.png', logo)

    if len(missing_uids) > 0:
        print('Logging missing uids. You should probably do something about '
              'this... I am not... the parent hyperlink is broken now.')
        (Path('.') / 'missing_uids.json').write_text(
            json.dumps(list(missing_uids)))

    # copy the webpage validation workflow script
    shutil.copyfile(Path(__file__).parent / 'wf.py', Path('wf.py'))


def interlayer_distance(bilayer: Atoms) -> float:
    n = len(bilayer) // 2
    return bilayer.positions[n:, 2].min() - bilayer.positions[:n, 2].max()


if __name__ == '__main__':
    copy_files(*sys.argv[1:])
