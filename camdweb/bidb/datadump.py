"""Create an ASE db-file containing "everything"."""
import json
from pathlib import Path

import rich.progress as progress
from ase.db import connect
from camdweb.utils import read_atoms


def bidb_folders():
    folders = list(Path().glob('materials/A*/*/*/'))
    with progress.Progress() as pb:
        pid = pb.add_task('Reading materials:', total=len(folders))
        for f in folders:
            yield f
            pb.advance(pid)


def create_db_file():
    db = connect('bidb.db')
    for f in bidb_folders():
        # uid = f'{f.parent.name}-{f.name}'
        atoms = read_atoms(f / 'structure.xyz')
        data = json.loads((f / 'data.json').read_text())
        db.write(atoms, **data)


if __name__ == '__main__':
    create_db_file()
