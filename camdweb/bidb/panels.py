from __future__ import annotations

from camdweb.c2db.bs_dos_bz_panel import BSDOSBZPanel
from camdweb.html import table
from camdweb.material import Material
from camdweb.panels.atoms import AtomsPanel
from camdweb.panels.structureinfo import CrystalStructurePanel


class BiDBBSBZPanel(BSDOSBZPanel):
    title = 'Band structure'
    info = """
    The single-particle band structure calculated with the PBE xc-functional
    including spin-orbit coupling (SOC). The color code shows the
    expectation value of the spin S<sub>i</sub> (where i=z for non-magnetic
    materials and otherwise is the magnetic easy axis). The effect of SOC on
    the band energies can be switching on/off.

    <br><br>Relevant articles:

    <br>Pakdel2024
    """

    def get_data(self, material):
        self.replace_shortname_ref('Pakdel2024')
        return super().get_data(material)


class BiDBStructure(CrystalStructurePanel):
    info = """
    The layer group of the bilayer and the space group of the bulk structure
    obtained by stacking the bilayer in AA order (see Fu et al.). The point
    group and the Bravais lattice type are also listed. The thickness of the
    material is defined as the vertical distance between the outermost atoms of
    the structure. The interlayer distance is defined as the smallest vertical
    distance between atoms in the upper and lower layer, respectively.

    <br><br>Relevant articles:

    <br>Fu2024"""

    def create_column_two(self, material: Material) -> str:
        return table(
            ['Structure data', ''],
            self.table_rows(
                material,
                ['crystal_type', 'formula', 'stoichiometry', 'natoms',
                 'area', 'volume', 'thickness', 'interlayer_distance']))


class BiDBAtomsPanel(AtomsPanel):
    info = """The material at a glance. The crystal symmetry is classified by
    the layer group (note that space groups are not appropriate for 2D
    materials, see Fu et al.). Links are provided to the monolayer in C2DB and,
    if available, to the parent layered bulk crystal in experimental databases
    (ICSD and/or COD). The slide stability and interlayer binding energy as
    well as some basic properties are also shown. Note that the PBE method
    typically underestimates the band gap.

    <br><br>Relevant articles:

    <br>Fu2024

    <br>Pakdel2024
    """

    def create_column_one(self,
                          material: Material) -> str:
        tables = [table(['Structure info', ''],
                        self.table_rows(material,
                                        ['layergroup', 'lgnum',
                                         'cod_id', 'icsd_id', 'label',
                                         'c2db_uid']),
                        col_spacing=''),
                  table(['Stability', ''],
                        self.table_rows(
                            material,
                            ['slide_stability', 'ebind'])),
                  table(['Basic properties', ''],
                        self.table_rows(material,
                                        ['magnetic', 'gap_pbe']))]

        return '\n'.join(tables)
