from camdweb.html import table
from camdweb.materials import Material
from camdweb.panels.panel import Panel, PanelData
from camdweb.figures import PLOTLY_SCRIPT as SCRIPT

HTML = """
<div class="container">
  <div class="row row-cols-1 row-cols-lg-2">
    <div class="col">
      <div id='stackings'></div>
    </div>
    <div class="col">
      {table}
    </div>
  </div>
</div>
"""


class StackingsPanel(Panel):
    title = 'All stackings'
    info = """Overview of all the calculated stacking configurations for the
    bilayer. All the bilayer structures are listed in the table according to
    their binding energy. Slide stability is only evaluated for the most stable
    bilayers with binding energy less than 3 meV/Å^2 of the most stable
    bilayer.

    <br><br>Relevant articles:

    <br>Pakdel2024
    """

    def get_data(self,
                 material: Material) -> PanelData:
        bilayers = material.data.get('all_stackings', [])  # list of all
        # bilayers
        rows = [(bilayer.ebind, bilayer.slide_stability, bilayer.uid)
                for bilayer in bilayers]
        tbl = table(
            ['Stacking',
             'Binding energy [meV/Å<sup>2</sup>]',
             'Slide stability'],
            [[f'<a href="{uid}">{uid}</a>' if uid != material.uid else
              f'{uid}',
              f'{e:.3f}',
              f'{st}']
             for e, st, uid in sorted(rows, reverse=True)])
        data = create_figure(bilayers, material)
        self.replace_shortname_ref('Pakdel2024')
        return PanelData(
            html=HTML.format(table=tbl),
            script=SCRIPT.format(data=data, id='stackings'),
            title=self.title,
            info=self.info)


def create_figure(bilayers: list[Material], material: Material) -> str:
    from camdweb.figures import CAMDFigure

    fig = CAMDFigure()
    fig.xaxis.title = 'Interlayer distance [Å]'
    fig.yaxis.title = 'Binding energy [meV/Å²]'
    hovertemplate = 'd: %{x} Å, Eb: %{y} meV/Å²<br>UID: %{customdata}'

    # Plot stable bilayers
    xdata = [bilayer.interlayer_distance for bilayer in bilayers
             if bilayer.slide_stability == 'Stable']
    ydata = [bilayer.ebind for bilayer in bilayers
             if bilayer.slide_stability == 'Stable']
    markers = ['x' if bilayer.uid == material.uid else 'circle'
               for bilayer in bilayers if bilayer.slide_stability == 'Stable']
    fig.scatter_plot(xdata,
                     ydata,
                     customdata=[bilayer.uid for bilayer in bilayers],
                     hovertemplate=hovertemplate,
                     marker=markers)

    # Plot unstable bilayers
    xdata = [bilayer.interlayer_distance for bilayer in bilayers
             if not bilayer.slide_stability == 'Stable']
    ydata = [bilayer.ebind for bilayer in bilayers
             if not bilayer.slide_stability == 'Stable']
    markers = ['x' if bilayer.uid == material.uid else 'circle'
               for bilayer in bilayers
               if not bilayer.slide_stability == 'Stable']
    fig.scatter_plot(xdata,
                     ydata,
                     customdata=[bilayer.uid for bilayer in bilayers],
                     hovertemplate=hovertemplate,
                     marker=markers)

    return fig.render()
