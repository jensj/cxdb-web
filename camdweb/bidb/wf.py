"""
Generate the paths
::
    python wf.py -p

Then test the server:
::
    python wf.py -t
"""
from camdweb.bidb.app import main as app
from camdweb.utils import check, check_as_workflow

N = 50


def workflow():
    from myqueue.workflow import run
    for n in range(N):
        run(function=check, args=[n, N, app], name=f'check-{n}', tmax='4h')


if __name__ == '__main__':
    check_as_workflow(app=app, N=N, pattern='*/*/')
