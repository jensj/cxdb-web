"""C2DB web-app.

The camdweb.c2db.copy module has code to convert ~cmr/C2DB-ASR/tree/
folders and friends (see PATTERNS variable below) to a canonical tree
layout.

Also contains web-app that can run off the tree of folders.

The goal is to have the code decoupled from ASE, GPAW, CMR and ASR.
Right now ASR webpanel() functions are still used
(see camdweb.c2db.asr_panel module).
"""
from __future__ import annotations

import argparse
import json
import re
from pathlib import Path

import rich.progress as progress
from bottle import HTTPError, redirect

from camdweb.c2db.superpanels import (ElectronicBandsPanel,
                                      ElectronicPropertiesPanel,
                                      LinearDeformationsPanel,
                                      MagneticPropertiesPanel,
                                      OpticalPropertiesPanel, StabilityPanel)
from camdweb.html import (Range, RangeX, SearchFilter, Select, image,
                          stoichiometry_input, structure_origin, table)
from camdweb.materials import Material, Materials
from camdweb.optimade.app import add_optimade
from camdweb.panels.atoms import AtomsPanel
from camdweb.panels.misc import MiscPanel
from camdweb.panels.panel import Panel
from camdweb.panels.structureinfo import CrystalStructurePanel
from camdweb.utils import cod, doi, icsd
from camdweb.web import CAMDApp


class C2DBAtomsPanel(AtomsPanel):
    info = """
    The material at a glance. The crystal symmetry is classified by the layer
    group (note that space groups are not appropriate for 2D materials, see Fu
    et al.). If available, links are provided to the parent layered bulk
    crystal in experimental databases (ICSD and/or COD) and to selected
    experimental paper(s) reporting on the mono/few-layer form of the material.
    Key stability indicators and some basic properties are also shown. Note
    that the PBE method typically underestimates the band gap while the HSE
    and GW methods are more accurate (see Haastrup et al.).

    <br><br>Relevant articles:

    <br>Fu2024

    <br>Haastrup2018
    """

    def create_column_one(self,
                          material: Material) -> str:
        html1 = table(['Structure info', ''],
                      self.table_rows(material,
                                      ['layergroup', 'lgnum', 'label',
                                       'cod_id', 'icsd_id', 'doi']),
                      col_spacing='')
        html2 = table(['Stability', ''],
                      self.table_rows(material,
                                      ['ehull', 'hform', 'dyn_stab']))
        # we want dos table to have no XC while this table has them specified
        # so we update the names here and allow table row to accept updated
        # names
        updated_names = {
            'gap': 'Band gap (PBE) [eV]', 'gap_hse': 'Band gap (HSE) [eV]',
            'gap_gw': 'Band gap (G₀W₀) [eV]'}
        html3 = table(['Basic properties', ''],
                      self.table_rows(material,
                                      ['is_magnetic', 'is_ferroelectric',
                                       'topology', 'dipz', 'gap', 'gap_hse',
                                       'gap_gw'],
                                      updated_names=updated_names),
                      )
        return '\n'.join([html1, html2, html3])


class C2DBApp(CAMDApp):
    """C2DB app with /row/<olduid> endpoint."""

    title = 'C2DB'
    logo = image('c2db-logo.png', alt='C2DB-logo')
    links = [
        ('CMR', 'https://cmr.fysik.dtu.dk'),
        ('About', 'https://cmr.fysik.dtu.dk/c2db/c2db.html')]

    def __init__(self,
                 materials: Materials,
                 initial_columns: list[str],
                 root: Path | None = None,
                 olduid2uid: dict[str, str] | None = None):
        super().__init__(materials,
                         initial_columns=initial_columns,
                         initial_filter_string='dyn_stab=Yes, ehull<0.2',
                         root=root)
        self.olduid2uid = olduid2uid or {}

    def route(self):
        super().route()
        self.app.route('/row/<olduid>')(self.material_page_old_uid)

    def material_page_old_uid(self, olduid: str) -> None:
        uid = self.olduid2uid.get(olduid)
        if uid is None:
            return HTTPError(body='Unknown old uid')
        return redirect(f'/material/{uid}')


def main(argv: list[str] | None = None) -> CAMDApp:
    """Create C2DB app."""
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'pattern', nargs='*', default=['*/*/*/'],
        help='Glob-pattern: materials/<pattern>.  Default is "*/*/*/".')
    args = parser.parse_args(argv)

    root = Path()
    folders = []
    for pattern in args.pattern:
        for p in (root / 'materials').glob(pattern):
            folders.append(p)

    olduid2uid = {}
    mlist: list[Material] = []
    with progress.Progress() as pb:
        pid = pb.add_task('Reading materials:', total=len(folders))
        for f in folders:
            uid = f'{f.parent.name}-{f.name}'
            material = Material.from_json(f / 'structure.json', uid)
            with (f / 'data.json').open() as fd:
                data = json.load(fd)
            material.columns.update(data)
            mlist.append(material)
            if hasattr(material, 'olduid'):
                olduid2uid[material.olduid] = uid
            pb.advance(pid)

    panels: list[Panel] = [
        C2DBAtomsPanel(),
        CrystalStructurePanel(),
        StabilityPanel(root=root),
        LinearDeformationsPanel(),
        ElectronicBandsPanel(),
        ElectronicPropertiesPanel(),
        MagneticPropertiesPanel(),
        OpticalPropertiesPanel(),
        MiscPanel()]

    materials = Materials(mlist, panels)

    materials.column_descriptions.update(
        has_inversion_symmetry='Inversion symmetry',
        evac='Vacuum level [eV]',
        hform='Heat of formation [eV/atom]',
        olduid='Old uid',
        magstate='Magnetic state',
        is_magnetic='Magnetic',
        is_magnetic_u='Magnetic +U',
        ehull='Energy above hull [eV/atom]',
        energy='Energy [eV]',
        spin_axis='Spin axis',
        efermi='Fermi level wrt. vacuum (PBE) [eV]',
        efermi_u='Fermi level wrt. vacuum (PBE+U) [eV]',
        efermi_hse='Fermi level wrt. vacuum (HSE06) [eV]',
        efermi_gw='Fermi level wrt. vacuum (G₀W₀) [eV]',
        dyn_stab='Dynamically stable',
        cod_id='COD id of parent bulk structure',
        icsd_id='ICSD id of parent bulk structure',
        doi='Mono/few-layer report(s)',
        layergroup='Layer group',
        lgnum='Layer group number',
        label='Structure origin',
        thickness='Thickness [Å]',
        bravais_type='2D Bravais type',
        international='Space group (bulk in AA-stacking)',
        pointgroup='Point group',
        number='Space group number (bulk in AA-stacking)',
        gap='Band gap (PBE) [eV]',
        gap_u='Band gap (PBE+U) [eV]',
        gap_hse='Band gap (HSE06) [eV]',
        gap_gw='Band gap (G₀W₀) [eV]',
        gap_dir='Direct band gap (PBE) [eV]',
        gap_dir_u='Direct band gap (PBE+U) [eV]',
        gap_dir_hse='Direct band gap (HSE06) [eV]',
        gap_dir_gw='Direct band gap (G₀W₀) [eV]',
        vbm='VBM wrt. vacuum (PBE) [eV]',
        vbm_u='VBM wrt. vacuum (PBE+U) [eV]',
        vbm_hse='VBM wrt. vacuum (HSE06) [eV]',
        vbm_gw='VBM wrt. vacuum (G₀W₀) [eV]',
        cbm='CBM wrt. vacuum (PBE) [eV]',
        cbm_u='CBM wrt. vacuum (PBE+U) [eV]',
        cbm_hse='CBM wrt. vacuum (HSE06) [eV]',
        cbm_gw='CBM wrt. vacuum (G₀W₀) [eV]',
        folder='Original file-system folder',
        magmom='Total magnetic moment [μ<sub>B</sub>]',
        dE_zx='Magnetic anisotropy energy, xz [meV/unit cell]',
        dE_zy='Magnetic anisotropy energy, yz [meV/unit cell]',
        J='Nearest neighbor exchange coupling [meV]',
        A='Single-ion anisotropy (out-of-plane) [meV]',
        lam='Anisotropic exchange (out-of-plane) [meV]',
        spin='Maximum value of S<sub>z</sub> at magnetic sites',
        N_nn='Number of nearest neighbors',
        P_spontaneous_norm='Spontaneous polarization [pC/m]',
        Px='<i>P<sub>x</sub></i>',
        Py='<i>P<sub>y</sub></i>',
        Pz='<i>P<sub>z</sub></i>',
        is_ferroelectric='Ferroelectric',
        halfmetal_gap='Half-metal gap [eV]',
        halfmetal_gap_dir='Direct half-metal gap [eV]',
        halfmetal_gap_hse='Half-metal gap (HSE06) [eV]',
        halfmetal_gap_dir_hse='Direct half-metal gap (HSE06) [eV]',
        topology='Topology',
        dipz='Out-of-plane dipole [e Å/unit cell]',
        evacdiff='Vacuum level shift',
        workfunction='Work function (avg. if finite dipole) [eV]',
        cbm_evac='CBM (vs. avg. vacuum) (PBE) [eV]',
        vbm_evac='VBM (vs. avg. vacuum) (PBE) [eV]',
    )
    for v in 'xyz':
        materials.column_descriptions[f'alpha{v}_el'] = (
            f'Interband polarizability ({v}) [Å]')
        materials.column_descriptions[f'alpha{v}_lat'] = (
            f'Static polarizability (phonons) ({v}) [Å]')
        materials.column_descriptions[f'alpha{v}'] = (
            f'Static polarizability (phonons + electrons) ({v}) [Å]')
    for v in 'xy':
        materials.column_descriptions[f'plasmafrequency_{v}'] = (
            f'Plasma frequency ({v}) [eV Å<sup>0.5</sup>]')

    materials.html_formatters.update(
        cod_id=cod,
        icsd_id=icsd,
        doi=doi)

    initial_columns = ['formula', 'ehull', 'hform', 'gap', 'is_magnetic',
                       'layergroup']

    # Example: root/materials/AB2/1MoS2/1/
    root = folders[0].parent.parent.parent.parent
    app = C2DBApp(materials,
                  initial_columns,
                  root,
                  olduid2uid)
    app.form_parts[0] = SearchFilter(placeholder="Example: 'MoS2'")
    app.form_parts += [
        stoichiometry_input(materials.stoichiometries()),
        Select('2D Bravais type', 'bravais_search',
               [''] + materials.bravais(),
               ['-'] + materials.bravais()),
        Select('Magnetic:', 'is_magnetic',
               ['', False, True], ['-', 'No', 'Yes']),
        Select('Dynamically stable:', 'dyn_stab',
               ['', 'Yes', 'No', 'Unknown'],
               ['-', 'Yes', 'No', 'Unknown'], default='Yes'),
        structure_origin(materials.labels()),
        Range('Energy above hull [eV/atom]:', 'ehull',
              nonnegative=True, default=('0.0', '0.2')),
        RangeX('Band gap range [eV]:', 'bg',
               ['gap', 'gap_hse', 'gap_gw'], ['PBE', 'HSE06', 'GW'])]

    return app


def test():  # pragma: no cover
    app = main(['AB2/*/*/'])
    app.material_page('1MoS2-1')


def create_app():  # pragma: no cover
    """Create the WSGI app."""
    app = main()
    add_optimade(app)
    return app.app


def check_all(pattern: str):  # pragma: no cover
    """Generate png-files."""
    c2db = main([str(path) for path in Path().glob(pattern)])
    for material in c2db.materials:
        print(material.uid)
        c2db.material_page(material.uid)


def check_def_pot():  # pragma: no cover
    from .asr_panel import read_result_file
    for path in Path().glob('A*/*/*/results-asr.deformationpotentials.json'):
        r = read_result_file(path)
        try:
            r['defpots_soc']
        except KeyError:
            f = json.loads(path.with_name('data.json').read_text())['folder']
            print(f)


def create_key_description_table():
    """... in reStructureText format."""
    app = main()
    for key, desc in sorted(app.materials.column_descriptions.items()):
        desc = re.sub(r'<sub>(.*)</sub>', r'\ `_{\1}`', desc)
        desc = re.sub(r'<sup>(.*)</sup>', r'\ `^{\1}`', desc)
        desc, _, unit = desc.partition(' [')
        if unit:
            unit = unit[:-1]
        print(f'    * - ``{key}``')
        print(f'      - {desc}')
        print(f'      - {unit}')


if __name__ == '__main__':
    app = main()
    try:
        import lark
    except ImportError:
        pass
    else:
        print('Lark:', lark.__version__)
        add_optimade(app)
    app.app.run(host='0.0.0.0', port=8081, debug=True)
