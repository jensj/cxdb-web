"""Hack to use webpanel() functions from ASR."""
from __future__ import annotations

import gzip
import importlib
from pathlib import Path

import matplotlib.pyplot as plt
from ase.db.core import KeyDescription
from ase.io.jsonio import decode
from camdweb.html import table, image, HTML_2COL as HTML
from camdweb.material import Material
from camdweb.panels.panel import Panel, PanelData, SkipPanel


def read_result_file(path: Path) -> dict:
    gz = path.with_suffix('.json.gz')
    if gz.is_file():
        with gzip.open(gz, 'rt') as fd:
            txt = fd.read()
    else:
        txt = path.read_text()
    dct = decode(txt)
    if 'kwargs' in dct:
        dct = dct['kwargs']['data']
    return dct


class Row:
    """Fake row object."""
    def __init__(self, material: Material):
        self.data = Data(material.folder)
        self.__dict__.update(material.columns)
        self.atoms = material.atoms
        self.cell = self.atoms.cell
        self.pbc = self.atoms.pbc
        self.symbols = self.atoms.symbols
        self.is_magnetic = getattr(material, 'is_magnetic', False)
        self.magstate = 'FM' if self.is_magnetic else 'NM'

    def toatoms(self):
        return self.atoms

    def get(self, name: str, default=None):
        if hasattr(self, name):
            return getattr(self, name)
        else:  # pragma: no cover
            return default

    def __contains__(self, key):
        return hasattr(self, key)

    def __getitem__(self, key):
        return getattr(self, key)


class Data:
    def __init__(self, folder: Path):
        self.folder = folder

    def get(self, name, default=None):
        try:
            dct = read_result_file(self.folder / name)
        except FileNotFoundError:
            return None
        dct = {key: val['kwargs']['data']
               if isinstance(val, dict) and 'kwargs' in val else val
               for key, val in dct.items()}
        return dct

    def __contains__(self, name):  # pragma: no cover
        assert name.startswith('results-asr.')
        p = self.folder / name
        return p.is_file() or p.with_suffix('.json.gz').is_file()

    def __getitem__(self, name):
        return self.get(name)


class ASRPanel(Panel):
    """Generic ASR-panel."""
    def __init__(self,
                 name: str):
        super().__init__()
        self.name = name
        mod = importlib.import_module(f'asr.{name}')
        self.webpanel = mod.webpanel
        self.result_class = mod.Result
        self.key_descriptions = {}
        for key, desc in getattr(self.result_class,
                                 'key_descriptions', {}).items():
            self.key_descriptions[key] = KeyDescription(key, desc)
        self.info = INFO.get(name, '')
        self.replace_shortname_ref('Haastrup2018')
        self.replace_shortname_ref('Gjerding2021')

    def get_data(self,
                 material: Material) -> PanelData:
        """Create row and result objects and call webpanel() function."""
        row = Row(material)
        dct = row.data.get(f'results-asr.{self.name}.json')
        if dct is None:
            raise SkipPanel
        if self.name == 'deformationpotentials' and 'defpots_soc' not in dct:
            raise SkipPanel  # pragma: no cover
        result = self.result_class(dct)
        (p, *_) = self.webpanel(result, row, self.key_descriptions)

        columns: list[list[str]] = [[], []]
        for i, column in enumerate(p['columns']):
            for thing in column:
                if thing is not None:  # pragma: no branch
                    html = thing2html(thing, material.folder)
                    columns[i].append(html)

        all_paths = []  # files to be created
        for desc in p.get('plot_descriptions', []):
            paths = [material.folder / filename
                     for filename in desc['filenames']]
            all_paths += paths
            for f in paths:
                if not f.is_file():
                    # Call plot-function:
                    desc['function'](row, *paths)
                    plt.close()
                    break

        assert all(path.is_file() for path in all_paths), all_paths

        return PanelData(
            HTML.format(col1='\n'.join(columns[0]),
                        col2='\n'.join(columns[1])),
            p['title'],
            info=self.info)


def thing2html(thing: dict, path: Path) -> str:
    """Convert webpanel() output to HTML."""
    if thing['type'] == 'figure':
        filename = thing['filename']
        html = image(path / filename)
    elif thing['type'] == 'table':
        rows = thing['rows']
        colspace = len(rows[0]) == 2 if rows else True
        header = thing.get('header')
        html = table(None if header == ['Property', 'Value'] else header, rows,
                     col_spacing='class="w-50"' if colspace else '')
    else:
        raise ValueError
    return html


INFO = {
    'phonons': """
    Phonon energies at the Γ-point and high-symmetry points at the
    Brillouin zone boundary.  The phonons are obtained by diagonalizing
    the mass weighted Hessian matrix. The latter is the second derivative
    of the energy wrt to atomic displacements and is calculated as a
    finite difference with displacements of 0.01 Å. A negative eigenvalue
    of the Hessian matrix indicates a dynamical instability of the atom
    positions. All energies are calculated with the PBE xc-functional.

    <br><br>Relevant articles:

    <br><a href=https://doi.org/10.1038/s41524-023-00977-x>S. Manti et al.,
    Exploring and machine learning structural instabilities in 2D materials,
    npj Comp. Mater. 9, 33 (2023).
    </a>""",
    'stiffness':
    """The stiffness tensor (C) is a rank-4 tensor that relates the stress
    in a material to the applied strain. In Voigt notation, C is expressed
    as a NxN matrix relating the N independent components of the stress and
    strain tensors. For a 2D material, N=3 corresponding to the two linear
    components (xx and yy) and the shear component (xy). C is calculated as
    a finite difference of the stress under an applied strain of 1% with
    full relaxation of atomic coordinates. A negative eigenvalue of C
    indicates a dynamical instability of the unit cell shape. All energies
    are calculated with the PBE xc-functional.

    <br><br>Relevant articles:
    <br>Haastrup2018""",
    'deformationpotentials':
    """The deformation potential is the derivative of the energy of the
    conduction band (CB) or valence band (VB) at a given k-point wrt
    applied strain. The deformation potential is calculated at the
    high-symmetry k-points and the k-points corresponding to the VBM and
    CBM. The latter may coincide with a high-symmetry k-point. The
    difference in deformation potential at the VBM and CBM equals the
    band gap deformation potential. The deformation potentials are
    calculated with the PBE xc-functional including spin orbit coupling.

    <br><br>Relevant articles:

    <br>Haastrup2018

    <br><a href=https://doi.org/10.1103/PhysRevB.94.245411>
    J. Wiktor, and A. Pasquarello,
    Absolute deformation potentials of two-dimensional materials.
    Physical Review B, 94, 245411 (2016).
    </a>""",
    'fermisurface':
    """The Fermi arc (2D version of the Fermi surface) is shown with the
    expectation value of the spin S<sub>i</sub> (where i=z for non-magnetic
    materials and otherwise is the magnetic easy axis) indicated by the
    color code. The band energies are calculated with the PBE
    xc-functional including spin-orbit coupling.

    <br><br>Relevant articles:
    <br>Haastrup2018""",
    'hse':
    """The single-particle band structure calculated with the HSE06
    xc-functional. The calculations are performed non-self-consistently with
    the wave functions from a PBE calculation. Spin-orbit interactions are
    included.

    <br><br>Relevant articles:

    <br>Haastrup2018""",
    'gw':
    """The quasiparticle (QP) band structure calculated within the G0W0
    approximation from a PBE starting point. The treatment of frequency
    dependence is numerically exact. A truncated Coulomb interaction is used
    to avoid screening from periodic images. The QP energies are
    extrapolated as 1/N to the infinite plane wave basis set limit.
    Spin-orbit interactions are included.

    <br><br>Relevant articles:

    <br>Haastrup2018

    <br><a href=https://doi.org/10.1103/PhysRevB.94.155406>
    F. Rasmussen et al. Efficient many-body calculations for two-dimensional
    materials using exact limits for the screened potential: Band gaps of MoS2,
    h-BN, and phosphorene, Phys. Rev. B 94 155406 (2016).
    </a>""",
    'borncharges':
    """The Born charge of an atom is defined as the derivative of the
    static macroscopic polarization wrt atomic displacements u<sub>i</sub> (
    i=x,y,z). The polarization is calculated as an integral over Berry
    phases. The Born charge is obtained as a finite difference of the
    polarization for displaced atomic configurations.

    <br><br>Relevant articles:

    <br><a href=https://doi.org/10.1021/acs.jpcc.0c01635>
    M. N. Gjerding et al.
    Efficient Ab-Initio Modeling of Dielectric Screening
    in 2D van der Waals Materials: Including Phonons,
    Substrates, and Doping, J. Phys. Chem. C 124 11609 (2020).
    </a>""",
    'shg':
    """The second-harmonic generation (SHG) spectrum is a second order
    optical response tensor, relating the electric polarization at
    frequency 2ω to the square of the applied electric field at
    frequency ω. The SHG tensor is of rank 3 corresponding to the
    polarization direction of the induced electric polarization and the
    two applied electric field vectors, respectively. The SHG spectra
    are obtained from second-order perturbation theory in the long wave
    length limit (q=0) using wave functions and energies from a PBE
    calculation. Spin-orbit interactions are not included. The
    calculations include a line-shape broadening of 50meV,
    and transitions of near degenerate bands are ignored when the energy
    difference is below 10meV.

    <br><br>Relevant articles:

    <br><a href=https://doi.org/10.1021/acsnano.1c00344>
    A. Taghizadeh et al.,
    Two-Dimensional Materials with Giant Optical Nonlinearities
    near the Theoretical Upper Limit,
    ACS Nano 15 7155 (2021).
    </a>""",
    'raman':
    """Raman spectroscopy relies on inelastic scattering of photons by
    optical phonons. The Stokes part of the Raman spectrum, corresponding to
    emission of a single Γ-point phonon is calculated for different
    incoming/outgoing photon polarizations using third order perturbation
    theory using the wave functions and energies from a PBE calculation.

    <br><br>Relevant articles:

    <br><a href=https://doi.org/10.1038/s41467-020-16529-6>
    A. Taghizadeh et al.
    A library of ab initio Raman spectra for automated identification
    of 2D materials. Nat Commun 11 3011 (2020).</a>""",
    'bse':
    """The optical absorption is calculated from the Bethe-Salpeter
    Equation (BSE) and the random phase approximation (RPA). The BSE
    calculation is only performed for selected materials with a small
    number of atoms in the unit cell. The BSE two-particle Hamiltonian
    is constructed using the wave functions from a PBE calculation with
    the direct band gap adjusted to match the direct band gap from a
    G0W0 calculation. Spin-orbit interactions are included. The BSE
    method accounts for excitonic effects. In the RPA calculations,
    the PBE band gap is also adjusted to match the G0W0 value.
    Spin-orbit interactions are not included in the RPA calculations.

    <br><br>Relevant articles:

    <br>Gjerding2021

    <br>Haastrup2018""",
    'piezoelectrictensor':
    """The piezoelectric tensor, c, is a rank-3 tensor relating the
    macroscopic polarization to an applied strain. In Voigt notation,
    c is expressed as a 3xN matrix relating the (x,y,z) components of the
    polarizability to the N independent components of the strain tensor. The
    polarization in a periodic direction is calculated as an integral over
    Berry phases. The polarization in a non-periodic direction is obtained
    by direct evaluation of the first moment of the electron density. The
    PBE xc-functional is employed for all calculations.

    <br><br>Relevant articles:

    <br>Gjerding2021""",
    'Basic electronic properties':
    """Electronic properties derived from a ground state DFT calculation
    with the PBE xc-functional.  For monolayers with an out-of-plane
    dipole, the vacuum level on the two sides of the layer will differ by
    a potential shift.  Band edge energies include spin orbit interactions
    and are given wrt the average value of the two vacuum levels.  For
    materials with a polar layer group and a finite band gap, the
    spontaneous polarization is calculated using the Berry phase formalism
    (see Kruse et al.).

    <br><br>Relevant articles:

    <br>Haastrup2018

    <br><br><a href=https://www.nature.com/articles/s41524-023-00999-5>M. Kruse
    et al., Two-dimensional ferroelectrics from high throughput computational
    screening, npj Comp. Mater. 9 45 (2023).</a>
    """,
    'Basic magnetic properties':
    """Heisenberg parameters, magnetic anisotropy, and magnetic moments. The
    Heisenberg parameters were calculated assuming that the magnetic energy of
    atom i can be represented as <br><br>

    E<sub>i</sub> = −1/2 J ∑<sub>j</sub> S<sub>i</sub> S<sub>j</sub>
    − 1/2 B ∑<sub>j</sub> S<sub>i</sub><sup>z</sup> S<sub>j</sub><sup>z</sup>
    − A S<sub>i</sub><sup>z</sup> S<sub>i</sub><sup>z</sup> <br><br>

    where J is the exchange coupling, B is anisotropic exchange, A is
    single-ion anisotropy and the sums run over nearest neighbours. The
    magnetic anisotropy is obtained from non-selfconsistent spin-orbit
    calculations where the xc-magnetic field from a scalar calculation is
    aligned in the x, y and z directions, respectively. All calculations employ
    the PBE xc-functional.

    <br><br>Relevant articles:

    <br><a href=https://iopscience.iop.org/article/10.1088/2053-1583/ab2c43>
    D. Torelli et al. High throughput computational screening for 2D
    ferromagnetic materials: the critical role of anisotropy and local
    correlations, 2D Mater. 6, 045018 (2019).</a>
    """,
}
