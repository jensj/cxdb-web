"""Optical polarizability."""
from __future__ import annotations

from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
from camdweb.html import image, HTML_1COL as HTML
from camdweb.panels.panel import Panel, PanelData, SkipPanel


class BerryPhase(Panel):
    title = 'Berry phase spectrum'
    info = """The spectrum is calculated by diagonalizing the Berry phase
    matrix obtained by parallel transport of the occupied Bloch states
    along the k1-direction for each value of k2. The eigenvalues can be
    interpreted as the charge centers of hybrid Wannier functions
    localised in the 1-direction and the colours show the expectation
    values of the spin for the corresponding Wannier functions. A
    gap-less spectrum is a minimal requirement for non-trivial
    topological invariants. The calculations are performed with the PBE
    xc-functional including spin-orbit coupling (SOC).

    <br><br>Relevant articles:

    <br><a href=https://doi.org/10.1103/PhysRevMaterials.3.024005>T. Olsen et
    al. Discovering two-dimensional topological insulators from high-throughput
    computations. Phys. Rev. Mater. 3 024005 (2019).</a>
    """

    def get_data(self, material):
        file = material.folder / 'results-asr.berry@calculate.json'

        if not file.is_file():
            raise SkipPanel

        png = material.folder / 'berry-phases0.png'
        if not png.is_file():
            from camdweb.c2db.asr_panel import read_result_file
            data = read_result_file(file)

            plot_phases(data, getattr(material, 'spin_axis', 'z'),
                        'berry-phases0.png', material.folder)

        img = image(material.folder / 'berry-phases0.png')
        return PanelData(
            HTML.format(col1=img),
            title=self.title,
            info=self.info)


def plot_phases(results: dict, spin_axis: str,
                f: str, folder: Path) -> None:
    phit_km = results.get('phi0_km')
    St_km = results.get('s0_km')

    if phit_km is None:
        raise SkipPanel

    if St_km is None:
        raise SkipPanel

    Nk = len(St_km)

    phi_km = np.zeros((len(phit_km) + 1, len(phit_km[0])), float)
    phi_km[1:] = phit_km
    phi_km[0] = phit_km[-1]
    S_km = np.zeros((len(phit_km) + 1, len(phit_km[0])), float)
    S_km[1:] = St_km
    S_km[0] = St_km[-1]
    S_km /= 2

    Nm = len(phi_km[0])
    phi_kM = np.tile(phi_km, (1, 2))
    phi_kM[:, Nm:] += 2 * np.pi
    S_kM = np.tile(S_km, (1, 2))
    Nk = len(S_kM)
    Nm = len(phi_kM[0])

    shape = S_kM.T.shape
    perm = np.argsort(S_kM.T, axis=None)
    phi_kM = phi_kM.T.ravel()[perm].reshape(shape).T
    S_kM = S_kM.T.ravel()[perm].reshape(shape).T

    plt.figure()
    plt.scatter(np.tile(np.arange(Nk), Nm)[perm],
                phi_kM.T.reshape(-1),
                cmap=plt.get_cmap('viridis'),
                c=S_kM.T.reshape(-1),
                s=5,
                marker='o')

    cbar = plt.colorbar()
    cbar.set_label(rf'$\langle S_{spin_axis}\rangle/\hbar$', size=16)

    plt.title(r'$\tilde k_2=0$', size=22)
    plt.xlabel(r'$\tilde k_1$', size=20)
    plt.ylabel(r'$\gamma_0$', size=20)

    plt.xticks([0, Nk / 2, Nk],
               [r'$-\pi$', r'$0$', r'$\pi$'], size=16)
    plt.yticks([0, np.pi, 2 * np.pi],
               [r'$0$', r'$\pi$', r'$2\pi$'], size=16)
    plt.axis((0, Nk, 0, 2 * np.pi))
    plt.tight_layout()
    plt.savefig(folder / f)
    plt.close()
