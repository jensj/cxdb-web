import json
import numpy as np
from pathlib import Path

from camdweb.html import table, image
from camdweb.panels.panel import Panel, PanelData, SkipPanel
from camdweb.c2db.asr_panel import Row
from camdweb.bandstructure import PlotUtilPBE, PDOSData
from camdweb.references import INFO
from camdweb.figures import PLOTLY_SCRIPT as SCRIPT

HTML = """
<div class="row row-cols-1 row-cols-lg-2">
  <div class="col">
    <div id='{bs}'></div>
    {bz}
  </div>
  <div class="col">
    <div id='{pdos}'></div>
    {bs_table}
  </div>
</div>
"""


class BSDOSBZPanel(Panel):
    def __init__(self, name: str = 'pbe'):
        """pbe (default), pbe+u, gw, hse"""
        super().__init__()
        self.info = INFO[name]
        self.title, self.suffix, self.plot_bs_only, names, self.xc = parse_xc(
            name)
        self.datafiles = [f'results-asr.{name}.json' for name in names]
        self.replace_shortname_ref('Haastrup2018')

    def get_data(self, material):
        bs = material.folder / self.datafiles[1]
        if not (bs.is_file() or bs.with_suffix('.json.gz').is_file()):
            raise SkipPanel

        row = Row(material)
        html = {'bs_table': '', 'bs': '', 'pdos': '', 'bz': ''}
        script, html['bs'] = self.bandstructure_fig(
            row, material.folder / f'bandstructure{self.suffix}.json')
        html['bs_table'] = self.bandstructure_table(row=row, material=material)

        if any('pdos' in data for data in self.datafiles):
            pdos_script, html['pdos'], html['bz'] = \
                self.pdos_and_bz_fig(row, material)
            script += pdos_script

        return PanelData(
            html=HTML.format(**html),
            script=script,
            title=self.title,
            info=self.info)

    def bandstructure_fig(self, row, file: Path) -> tuple[str, str]:
        if not file.is_file():
            if self.plot_bs_only:
                data = combined_bandstructure(
                    row=row, name=self.suffix, gs=self.datafiles[0],
                    bs=self.datafiles[1]).plot()
            else:
                data = bs_from_row(
                    row=row, gs=self.datafiles[0], bs=self.datafiles[1]).plot()
        else:
            data = json.loads(file.read_text())
        return SCRIPT.format(data=data, id=file.stem), file.stem

    def bandstructure_table(self, row, material) -> str:
        keys = [f'gap{self.suffix}', f'gap_dir{self.suffix}',
                f'vbm{self.suffix}', f'cbm{self.suffix}', 'evacdiff']
        if row.data[self.datafiles[0]].get(f'gap{self.suffix}', 0.0) <= 0.0:
            keys.append(f'efermi{self.suffix}')

        return table(['Properties [eV]', ''],
                     # remove units & xc from rows
                     [[key.replace(f'({self.xc}) [eV]', ''), val]
                      for key, val in self.table_rows(material, keys)])

    def pdos_and_bz_fig(self, row, material) -> tuple[str, str, str]:
        # only plot BZ with PDOS
        bz_file = material.folder / f'bz-with-gaps{self.suffix}.png'
        if not bz_file.is_file():
            bz_with_band_extremums(row, bz_file, self.datafiles[0])

        file = material.folder / f'pdos{self.suffix}.json'
        if not file.is_file():
            data = plotter_pdos(
                row=row, results=self.datafiles[2], gs=self.datafiles[0])
        else:
            data = json.loads(file.read_text())
        return (SCRIPT.format(data=data, id=file.stem), file.stem,
                image(bz_file, 'BZ'))


def parse_xc(name):
    plot_bs_only = False
    if name.startswith('pbe'):
        suffix = '_u' if name.endswith('+u') else ''
        names = [r + suffix for r in ['gs', 'bandstructure', 'pdos']]
        xc = name.upper()
        title = f'Band structure and DOS ({xc})'
    elif name in ['gw', 'hse']:
        plot_bs_only = True
        suffix = f'_{name}'
        names = [name, 'bandstructure']
        xc = 'G₀W₀' if name == 'gw' else 'HSE06'
        title = f'Band structure ({xc}@PBE)'
    else:
        raise ValueError('Unknown suffix: {}'.format(name))
    return title, suffix, plot_bs_only, names, xc


def combined_bandstructure(row, name, gs, bs):
    from camdweb.bandstructure import PlotUtilComparison, PlotDatset
    dctpbe = row.data.get(bs)
    dct = row.data.get(gs)
    gaps = row.data[gs]
    fermilevel_gw = dct[f'efermi{name}_soc']
    fermilevel_dft = dctpbe['bs_soc']['efermi']

    vbm = gaps.get(f'vbm{name}')
    cbm = gaps.get(f'cbm{name}')

    energy_dft_mn = dctpbe['bs_soc']['energies']
    energy_gw_mn = dct['bandstructure']['e_int_mk']
    path = dct['bandstructure']['path']
    pathdft = dctpbe['bs_nosoc']['path']

    # XXX: This decision should be centralized one place in the code
    if gaps.get(f'gap{name}', 0.0) > 0.0:
        # Semiconductor
        fermilevel_gw = None
        fermilevel_dft = None
        reference = vbm
    else:
        # Metallic
        reference = fermilevel_gw

    ylabelnames = {'_gw': 'GW', '_hse': 'HSE'}

    GWData = PlotDatset(path=path,
                        fermilevel=fermilevel_gw,
                        energy_soc_mk=energy_gw_mn,
                        name=ylabelnames[name],
                        color='orange')

    PBEData = PlotDatset(path=pathdft,
                         fermilevel=fermilevel_dft,
                         energy_soc_mk=energy_dft_mn,
                         name='PBE',
                         color='#aaaaaa')

    return PlotUtilComparison(
        datasets=[PBEData, GWData],
        emin=(fermilevel_gw if vbm is None else vbm) - 3,
        emax=(fermilevel_gw if cbm is None else cbm) + 3
    ).subtract_reference_energy(vbm_reference=reference)


def bs_from_row(row, bs, gs):
    dct = row.data.get(bs)
    gaps = row.data[gs]
    fermilevel_soc = dct['bs_soc']['efermi']
    assert np.allclose(dct['bs_soc']['path'].kpts,
                       dct['bs_nosoc']['path'].kpts)

    vbm, cbm = gaps['vbm'], gaps['cbm']
    energy_soc_mk = dct['bs_soc']['energies']
    energy_nosoc_skn = dct['bs_nosoc']['energies']

    # XXX: This decision should be centralized one place in the code
    if gaps['gap'] > 0.0:
        # Semiconductor
        fermilevel_soc = None
        reference = vbm
    else:
        # Metallic
        reference = fermilevel_soc

    return PlotUtilPBE(
        energy_soc_mk=energy_soc_mk,
        energy_nosoc_skn=energy_nosoc_skn,
        spin_zprojection_soc_mk=dct['bs_soc']['sz_mk'],
        path=dct['bs_nosoc']['path'],
        fermilevel=fermilevel_soc,
        emin=(fermilevel_soc if vbm is None else vbm) - 3,
        emax=(fermilevel_soc if cbm is None else cbm) + 3,
        spin_axisname=row.get('spin_axis', 'z')  # XXX crazy to have a default
    ).subtract_reference_energy(vbm_reference=reference)


def plotter_pdos(row, results: str, gs: str):
    soc = 'nosoc'
    if results in row.data and f'pdos_{soc}' in row.data[results]:
        pdos_data = row.data[results][f'pdos_{soc}']
        gs_data = row.data[gs]
    else:
        return

    if gs_data['gap'] > 0.0:
        # Semiconductor
        reference = gs_data[f'gaps_{soc}']['vbm']
        if reference is None:
            reference = gs_data[f'gaps_{soc}']['efermi']
    else:
        # Metallic
        reference = pdos_data['efermi']

    pdos = PDOSData(row=row, soc=False, reference=reference, results=results,
                    gsresults=gs)
    return pdos.plot()


def bz_with_band_extremums(row, fname, gs):
    from ase.geometry.cell import Cell
    from matplotlib import pyplot as plt
    import numpy as np

    c2db_symmetry_eps = 0.1
    ndim = sum(row.pbc)

    # Standardize the cell rotation via Bravais lattice roundtrip:
    lat = Cell(row.cell).get_bravais_lattice(pbc=row.pbc,
                                             eps=c2db_symmetry_eps)
    cell = lat.tocell()

    plt.figure(figsize=(4, 4))
    lat.plot_bz(vectors=False, pointstyle={'c': 'k', 'marker': '.'})

    gsresults = row.data.get(gs)
    cbm_c = gsresults['k_cbm_c']
    vbm_c = gsresults['k_vbm_c']
    op_scc = row.data[
        'results-asr.structureinfo.json']['spglib_dataset']['rotations']
    if cbm_c is not None:
        if not row.is_magnetic:
            op_scc = np.concatenate([op_scc, -op_scc])
        ax = plt.gca()
        icell_cv = cell.reciprocal()
        vbm_style = {'marker': 'o', 'facecolor': 'w',
                     'edgecolors': 'C0', 's': 50, 'lw': 2,
                     'zorder': 4}
        cbm_style = {'c': 'C1', 'marker': 'o', 's': 20, 'zorder': 5}
        cbm_sc = np.dot(op_scc.transpose(0, 2, 1), cbm_c)
        vbm_sc = np.dot(op_scc.transpose(0, 2, 1), vbm_c)
        cbm_sv = np.dot(cbm_sc, icell_cv)
        vbm_sv = np.dot(vbm_sc, icell_cv)

        if ndim < 3:
            ax.scatter([vbm_sv[:, 0]], [vbm_sv[:, 1]], **vbm_style,
                       label='VBM')
            ax.scatter([cbm_sv[:, 0]], [cbm_sv[:, 1]], **cbm_style,
                       label='CBM')

            # We need to keep the limits set by ASE in 3D, else the aspect
            # ratio goes haywire.  Hence this bit is also for ndim < 3 only.
            xlim = np.array(ax.get_xlim()) * 1.4
            ylim = np.array(ax.get_ylim()) * 1.4
            ax.set_xlim(xlim)
            ax.set_ylim(ylim)
        else:
            ax.scatter([vbm_sv[:, 0]], [vbm_sv[:, 1]],
                       [vbm_sv[:, 2]], **vbm_style, label='VBM')
            ax.scatter([cbm_sv[:, 0]], [cbm_sv[:, 1]],
                       [cbm_sv[:, 2]], **cbm_style, label='CBM')

        plt.legend(loc='upper center', ncol=3, prop={'size': 9})

    plt.tight_layout()
    plt.savefig(fname)
