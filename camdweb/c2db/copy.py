"""Copy C2DB files from ASR-layout to uniform tree structure.

Tree structure::

   <stoichiometry>/<formula-units><formula>/<id>/

Example::

   AB2/1MoS2/1/
   AB2/1MoS2/2/
   ...

Build tree like this::

    $ cd /tmp
    $ mkdir tree
    $ cd tree
    $ python -m camdweb.c2db.copy <root-dir> <pattern> <pattern> ...

"""
from __future__ import annotations

import argparse
import json
import shutil
from collections import defaultdict
from pathlib import Path

import rich.progress as progress
from ase import Atoms
from ase.formula import Formula
from camdweb.c2db.emass import get_emass_data
import numpy as np

from camdweb import ColVal
from camdweb.c2db.asr_panel import read_result_file
from camdweb.c2db.convex_hull import update_chull_data
from camdweb.c2db.oqmd123 import read_oqmd123_data
from camdweb.utils import all_dirs, process_pool, read_atoms, thickness
from camdweb.atomsdata import AtomsData

ROOT = Path('/home/niflheim2/cmr/C2DB-ASR')
MAG_FILES = [
    'magnetic_anisotropy',
    'exchange',
    'magstate',
    'bandstructure',
    'pdos'
]
RESULT_FILES = [
    'stiffness',
    'phonons',
    'deformationpotentials',
    'hse',
    'gw',
    'borncharges',
    'shg',
    'raman',
    'bse',
    'piezoelectrictensor',
    'plasmafrequency',
    'polarizability',
    'gs',
    'gs@calculate',
    'fermisurface',
    'shift',
    'structureinfo',
    'spontaneous_polarization',
    'collect_spiral',
    'dmi',
    'spinorbit',
    'spinorbit@calculate',
    'berry@calculate',
    *MAG_FILES]
PATTERNS = [
    'tree/A*/*/*/',
    'ICSD-COD/*el/*/',
    'adhoc_materials/*/',
    'tree_LDP/A*/*/*/',
    'tree_CDVAE/A*/*/*/',
    'tree_intercalated/A*/*/*/',
    'push-manti-tree/A*/*/*/',
    'tree_Wang23/A*/*/*/',
    'elrashidy24-tree/A*/*/*/']
PATTERNS_U = [
    'c2db_U4/tree/A*/*/*/']


class NumpyEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        if isinstance(obj, np.int64):
            return int(obj)
        return json.JSONEncoder.default(self, obj)


def atoms_to_uid_name(atoms: Atoms) -> str:
    """Convert Atoms obejct to name.

    >>> atoms_to_uid_name(Atoms('H2OH2O'))
    '2OH2'
    >>> atoms_to_uid_name(Atoms('AuAg'))
    '1AgAu'
    """
    f = atoms.symbols.formula
    stoi, reduced, nunits = f.stoichiometry()
    return f'{nunits}{reduced}'


def copy_materials(root: Path,
                   patterns: list[str],
                   update_chull: bool = True,
                   processes: int = 1,
                   write_new_uid_files: bool = False,
                   pbe_u: bool = True) -> None:
    dirs = all_dirs(root, patterns)
    print('Folders:', len(dirs))

    things: list[tuple[Path, str, str | None]] = []
    known = 0
    with progress.Progress() as pb:
        pid = pb.add_task('Finding UIDs:', total=len(dirs))
        for dir in dirs:
            try:
                uidfile = json.loads((dir / 'uid.json').read_text())
                uid = uidfile.get('uid', uidfile.get('uid_no_u'))
            except FileNotFoundError:
                try:
                    name = atoms_to_uid_name(
                        read_atoms(dir / 'structure.json'))
                except FileNotFoundError:
                    pass
                else:
                    things.append((dir, name, None))
            else:
                name, tag = uid.split('-')
                things.append((dir, name, uid))
                known += 1
            pb.advance(pid)

    print('Known materials:', known)
    print('New materials:', len(things) - known)

    # Find largest tag numbers for each formula:
    maxtags: defaultdict[str, int] = defaultdict(int)
    for dir, name, uid in things:
        if uid is not None:
            name, tag = uid.split('-')
            maxtags[name] = max(maxtags[name], int(tag))

    work = []
    for dir, name, uid in things:
        if uid is None:
            maxtags[name] += 1
            uid = f'{name}-{maxtags[name]}'
            if write_new_uid_files:
                (dir / 'uid.json').write_text(
                    json.dumps({'uid': uid}, indent=2))
            else:
                uid += 'temp'

        name, tag = uid.split('-')
        stoichiometry, _, nunits = Formula(name).stoichiometry()
        folder = Path(f'materials/{stoichiometry}/{name}/{tag}')
        work.append((dir, folder, uid))

    parent_folders = set()
    for _, folder, _ in work:
        parent_folders.add(folder.parent)
    for folder in parent_folders:
        folder.mkdir(exist_ok=True, parents=True)

    with process_pool(processes) as pool:
        with progress.Progress() as pb:
            pid = pb.add_task('Copying materials:', total=len(work))
            for _ in pool.imap_unordered(worker, work):
                pb.advance(pid)

    if update_chull:
        # Calculate hform, ehull, ...
        try:
            oqmd_path = Path('oqmd123.json.gz')
            atomic_energies, refs = read_oqmd123_data(oqmd_path)
        except FileNotFoundError:
            raise FileNotFoundError(
                f'Could not find {oqmd_path}.\n'
                'Please download the oqmd123.db file:\n\n'
                '   wget https://cmr.fysik.dtu.dk/_downloads/oqmd123.db\n\n'
                'or\n\n'
                '   curl https://cmr.fysik.dtu.dk/_downloads/oqmd123.db'
                ' -o owmd123.db\n\n'
                'and convert to json with:\n\n'
                '   python -m camdweb.c2db.oqmd123 <path-to-oqmd123.db>\n')
        update_chull_data(atomic_energies, refs)

    if pbe_u:
        dirs = all_dirs(root, PATTERNS_U)
        print('Collecting PBE+U:', len(dirs))
        for fro in dirs:
            copy_pbeu(fro=fro)

    # Put logo in the right place:
    logo = Path('c2db-logo.png')
    if not logo.is_file():
        shutil.copyfile(Path(__file__).parent / 'logo.png', logo)

    # copy the webpage validation workflow script
    shutil.copyfile(Path(__file__).parent / 'wf.py', Path('wf.py'))

    print('DONE')


def worker(args):  # pragma: no cover
    """Used by Pool."""
    copy_material(*args)


def copy_material(fro: Path,
                  to: Path,
                  uid: str) -> None:  # pragma: no cover
    structure_file = fro / 'structure.json'
    if not structure_file.is_file():
        return
    atoms = read_atoms(structure_file)

    to.mkdir(exist_ok=True)

    def rrf(name: str) -> dict:
        return read_result_file(fro / f'results-asr.{name}.json')

    # None values will be removed later
    data: dict[str, ColVal | None] = {'folder': str(fro)}

    # Read uid and perhaps olduid:
    try:
        data.update(json.loads((fro / 'uid.json').read_text()))
    except FileNotFoundError:
        data['uid'] = uid
    else:
        assert data['uid'] == uid

    structure = rrf('structureinfo')
    data['international'] = structure['spacegroup']
    data['number'] = structure['spgnum']
    data['pointgroup'] = str(structure['pointgroup'])
    data['has_inversion_symmetry'] = structure['has_inversion_symmetry']
    lgnum = structure.get('lgnum')
    if lgnum is not None:
        data['lgnum'] = lgnum
        data['layergroup'] = structure['layergroup']
        try:
            data['bravais_type'] = json.loads(
                (fro / 'bravais_type.json').read_text())['bravais_type']
            data['bravais_search'] = str(data['bravais_type']).split(' ')[0]
        except FileNotFoundError:
            pass
    else:
        data['layergroup'] = '?'
        data['lgnum'] = -1

    data['thickness'] = thickness(atoms)

    try:
        data['label'] = rrf('c2db.labels')['label']
    except FileNotFoundError:
        pass

    vacuumlevel = 0.0
    try:
        gs = rrf('gs')
    except FileNotFoundError:
        pass
    else:
        data['gap_dir_nosoc'] = gs['gap_dir_nosoc']
        vacuumlevel = gs['evac']
        data['evac'] = vacuumlevel
        data['efermi'] = gs['efermi'] - vacuumlevel
        data.update(standard_gs(gs=gs, vacuumlevel=vacuumlevel))
        if 'evacdiff' in gs:
            data['evacdiff'] = gs['evacdiff']
        if 'dipz' in gs:
            data['dipz'] = gs['dipz']
    try:
        ph = rrf('phonons')
    except FileNotFoundError:
        dyn_stab_phonons = 'unknown'
    else:
        data['minhessianeig'] = ph['minhessianeig']
        dyn_stab_phonons = ph['dynamic_stability_phonons']
        maxphononfreq = ph['omega_kl'][0].max()

    try:
        ph = rrf('stiffness')
    except FileNotFoundError:
        dyn_stab_stiffness = 'unknown'
    else:  # pragma: no cover
        dyn_stab_stiffness = ph['dynamic_stability_stiffness']

    if dyn_stab_phonons == 'unknown' or dyn_stab_stiffness == 'unknown':
        data['dyn_stab'] = 'Unknown'
    else:
        data['dyn_stab'] = 'Yes' if (dyn_stab_phonons == 'high' and
                                     dyn_stab_stiffness == 'high') else 'No'

    for x in ['hse', 'gw']:
        try:
            r = rrf(x)
        except FileNotFoundError:
            pass
        else:  # pragma: no cover
            gap = r.get(f'gap_{x}', 0.0)
            if gap <= 0.0:  # Metallic
                continue
            data.update(
                standard_gs(gs=r, vacuumlevel=vacuumlevel, suffix=f'_{x}'))
            efermi = r.get(f'efermi_{x}_soc')
            if efermi is not None:
                data[f'efermi_{x}'] = efermi - vacuumlevel

    try:
        bse = rrf('bse')
    except FileNotFoundError:
        pass
    else:  # pragma: no cover
        data['E_B'] = bse.get('E_B')

    try:
        pol = rrf('polarizability')
    except FileNotFoundError:
        pass
    else:
        for a in 'xyz':
            data[f'alpha{a}_el'] = pol[f'alpha{a}_el']
        dct = {'omega_w': pol['frequencies'].tolist()}
        for v in 'xyz':
            alpha = pol[f'alpha{v}_w']
            dct[f'alpha{v}_re_w'] = alpha.real.tolist()
            dct[f'alpha{v}_im_w'] = alpha.imag.tolist()
        (to / 'opt-polarizability.json').write_text(json.dumps(dct))

    try:
        irpol = rrf('infraredpolarizability')
    except FileNotFoundError:
        pass
    else:
        for a in 'xyz':
            data[f'alpha{a}_lat'] = irpol.get(f'alpha{a}_lat')
            if f'alpha{a}_el' in data:
                data[f'alpha{a}'] = (
                    data[f'alpha{a}_el'] +  # type: ignore[operator]
                    data[f'alpha{a}_lat'])
        alpha_wvv = irpol['alpha_wvv']
        dct = {'maxphononfreq': maxphononfreq,
               'omega_w': irpol['omega_w'].tolist(),
               'alpha_re_wvv': alpha_wvv.real.tolist(),
               'alpha_im_wvv': alpha_wvv.imag.tolist()}
        (to / 'ir-polarizability.json').write_text(json.dumps(dct))

    try:
        dct = rrf('plasmafrequency')
    except FileNotFoundError:
        pass
    else:
        for v in 'xy':
            data[f'plasmafrequency_{v}'] = dct[f'plasmafrequency_{v}']
    data['energy'] = atoms.get_potential_energy()

    try:
        info = json.loads((fro / 'info.json').read_text())
    except FileNotFoundError:
        pass
    else:
        for key in ['icsd_id', 'cod_id', 'doi']:
            if key in info:
                data[key] = info[key]

    AtomsData.from_atoms(atoms).write_json(to / 'structure.json')

    try:
        bc = rrf('bader')['bader_charges']
    except FileNotFoundError:
        pass
    else:
        (to / 'bader.json').write_text(
            json.dumps({'charges': bc.tolist()}))

    try:
        emass_data = rrf('effective_masses')
    except FileNotFoundError:
        pass
    else:
        emass_webpanel_data = get_emass_data(emass_data, atoms)
        with open(to / 'emass.json', 'w') as file:
            json.dump(emass_webpanel_data, file, indent=4, cls=NumpyEncoder)

    data = copy_magnetic_info(data, fro, to, mode='')

    try:
        berry = rrf('berry')
    except FileNotFoundError:
        pass
    else:
        topology = berry['Topology']
        if topology != 'Not checked':
            data['topology'] = topology

    try:
        spont_pol = rrf('spontaneous_polarization')
    except FileNotFoundError:
        pass
    else:
        data['is_ferroelectric'] = bool(spont_pol['Ferroelectric'])
        data['P_spontaneous_norm'] = float(spont_pol['P'])

    try:
        data.update(json.loads((fro / 'halfmetal.json').read_text()))
    except FileNotFoundError:
        pass

    try:
        data.update(json.loads((fro / 'halfmetal_hse.json').read_text()))
    except FileNotFoundError:
        pass

    # Copy result json-files:
    copy_result_files(to=to, fro=fro, result_files=RESULT_FILES, mode='')

    path = to / 'data.json'
    if path.is_file():
        olddata = json.loads(path.read_text())
        data['ehull'] = olddata.get('ehull')
        data['hform'] = olddata.get('hform')

    # Remove None values:
    data = {key: value for key, value in data.items() if value is not None}

    path.write_text(json.dumps(data, indent=0))


def copy_result_files(to: Path, fro: Path, result_files: list[str], mode: str):
    for name in result_files:
        result = fro / f'results-asr.{name}.json'
        target = to / result.name.replace(name, name + mode)
        gzipped = target.with_suffix('.json.gz')
        if result.is_file() and not (target.is_file() or gzipped.is_file()):
            shutil.copyfile(result, target)


def copy_pbeu(fro: Path) -> None:
    path = fro / 'uid.json'
    if not path.is_file():
        return
    name, tag = json.loads(path.read_text())['uid_no_u'].split('-')
    stoichiometry, _, nunits = Formula(name).stoichiometry()
    to = Path(f'materials/{stoichiometry}/{name}/{tag}')

    # this should always be here since these are added on top of existing data
    path = to / 'data.json'
    data = json.loads(path.read_text())
    data = copy_magnetic_info(data, fro, to, mode='_u')
    copy_result_files(to=to, fro=fro, result_files=RESULT_FILES, mode='_u')

    try:
        gs = read_result_file(fro / 'results-asr.gs.json')
    except FileNotFoundError:
        pass
    else:
        vacuumlevel = gs['evac']
        data['efermi_u'] = gs['efermi'] - vacuumlevel
        data.update({
            f'{k}_u': v for k, v in standard_gs(gs, vacuumlevel).items()
        })
    data = {key: value for key, value in data.items() if value is not None}
    path.write_text(json.dumps(data, indent=0))


def copy_magnetic_info(data: dict,
                       fro: Path,
                       to: Path,
                       mode: str = '') -> dict:  # pragma: no cover
    def rrf(name: str) -> dict:
        return read_result_file(fro / f'results-asr.{name}.json')

    try:
        magstate = rrf('magstate')
    except FileNotFoundError:
        pass
    else:
        data[f'is_magnetic{mode}'] = magstate['is_magnetic']
        if data[f'is_magnetic{mode}']:
            write_magmoms(fro, to, mode=mode)
        data[f'magmom{mode}'] = magstate.get('magmom', 0.0)

    try:
        ma = rrf('magnetic_anisotropy')
    except FileNotFoundError:
        pass
    else:
        data[f'spin_axis{mode}'] = ma['spin_axis']
        data[f'dE_zx{mode}'] = float(ma['dE_zx'])
        data[f'dE_zy{mode}'] = float(ma['dE_zy'])

    try:
        exch = rrf('exchange')
    except FileNotFoundError:
        pass
    else:
        data[f'J{mode}'] = exch['J']
        data[f'lam{mode}'] = exch['lam']
        data[f'A{mode}'] = float(exch['A'])
        data[f'spin{mode}'] = exch['spin']
        data[f'N_nn{mode}'] = int(exch['N_nn'])

    return data


def standard_gs(gs: dict, vacuumlevel: float, suffix: str = ''):
    data = {f'gap{suffix}': gs.get(f'gap{suffix}', 0.0),
            f'gap_dir{suffix}': gs.get(f'gap_dir{suffix}', 0.0)}
    vbm = gs.get(f'vbm{suffix}')
    cbm = gs.get(f'cbm{suffix}')
    if vbm is not None:
        vbm -= vacuumlevel
    if cbm is not None:
        cbm -= vacuumlevel
    data[f'vbm{suffix}'] = vbm
    data[f'cbm{suffix}'] = cbm
    return data


def write_magmoms(fro: Path,
                  to: Path, mode: str) -> None:  # pragma: no cover
    def rrf(name: str) -> dict:
        return read_result_file(fro / f'results-asr.{name}.json')

    # None values will be removed later
    data: dict[str, ColVal | None] = {'folder': str(fro)}

    magstate = rrf('magstate')
    magmoms = magstate['magmoms']
    data['magmoms'] = magmoms.tolist() if magmoms is not None else None

    try:
        orbmag_a = rrf('orbmag')['orbmag_a']
        data['orbmag_a'] = orbmag_a.tolist() if orbmag_a is not None else None
    except FileNotFoundError:
        pass

    # Remove None values:
    data = {key: value for key, value in data.items() if value is not None}

    (to / f'magmoms{mode}.json').write_text(json.dumps(data, indent=0))


def main(argv: list[str] | None = None):
    parser = argparse.ArgumentParser()
    parser.add_argument('root', help='Root of ASR-tree to copy from. '
                        'Example: "~cmr/C2DB-ASR/".', type=Path)
    patterns = ', '.join(f'"{p}"' for p in PATTERNS)
    parser.add_argument(
        'pattern', nargs='+',
        help='Glob pattern like "tree/A*/*/*/". '
        f'Use "ALL" to get all the standard patterns: {patterns}.')
    parser.add_argument('-s', '--skip-convex-hulls', action='store_true')
    parser.add_argument('-w', '--write-uid-json-files', action='store_true')
    parser.add_argument('-p', '--processes', type=int, default=1)
    parser.add_argument('-u', '--pbe-u', type=int, default=0)
    args = parser.parse_args(argv)
    if args.pattern == ['ALL']:  # pragma: no cover
        args.pattern = PATTERNS
        args.pbe_u = True
    copy_materials(args.root, args.pattern, not args.skip_convex_hulls,
                   args.processes, args.write_uid_json_files, args.pbe_u)


if __name__ == '__main__':
    main()
