"""Create various tar-files and ASE db-files containing "everything".

$ tar -czf c2db.tar.gz \
  --exclude "result*.json" --exclude "*.png" \
  C2DB/A* C2DB/convex-hulls/

"""
import json
from pathlib import Path
import shutil

import rich.progress as progress
from ase.db import connect
from camdweb.utils import read_atoms


def c2db_folders():
    folders = list(Path().glob('materials/A*/*/*/'))
    with progress.Progress() as pb:
        pid = pb.add_task('Reading materials:', total=len(folders))
        for f in folders:
            yield f
            pb.advance(pid)


def create_db_file():
    db = connect('c2db.db')
    for f in c2db_folders():
        # uid = f'{f.parent.name}-{f.name}'
        atoms = read_atoms(f / 'structure.xyz')
        data = json.loads((f / 'data.json').read_text())
        data.pop('energy')
        data.pop('pointgroup')  # can be "1" which ase.db doesn't allow!
        data.pop('magmom', None)
        db.write(atoms, **data)


def create_data_dump():
    data = Path('data')
    for f in c2db_folders():
        to = data / f
        to.mkdir(parents=True, exist_ok=True)
        for name in ['structure.xyz',
                     'data.json',
                     'results-asr.stiffness.json',
                     'results-asr.gw.json',
                     'magmoms.json']:
            fro = f / name
            if fro.is_file():
                shutil.copy(fro, to)
        if (f / 'results-asr.hse.json').is_file():
            add_hse_stuff(f, to)


def add_hse_stuff(path, to):
    from ase.io.ulm import ulmopen
    from camdweb.c2db.asr_panel import read_result_file
    dct = json.loads((path / 'data.json').read_text())
    orig = Path(dct['folder'])
    try:
        gpw = ulmopen(orig / 'hse_nowfs.gpw')
    except FileNotFoundError:
        print(orig)
        return
    bzkpts = gpw.wave_functions.kpts.bzkpts
    ibzkpts = gpw.wave_functions.kpts.ibzkpts
    bz2ibz = gpw.wave_functions.kpts.bz2ibz
    hse = read_result_file(orig / 'results-asr.hse@calculate.json')
    e_mk = hse['hse_eigenvalues_soc']['e_hse_mk']
    if e_mk.shape[1] != len(bzkpts):
        assert e_mk.shape[1] == len(ibzkpts)
        e_mk = e_mk[:, bz2ibz]
    efermi = read_result_file(orig / 'results-asr.hse.json')['efermi_hse_soc']
    (to / 'hse.json').write_text(
        json.dumps(
            {'efermi': efermi,
             'bzkpts': bzkpts.tolist(),
             'eig_soc_mk': e_mk.round(6).tolist()},
            indent=0))


if __name__ == '__main__':
    # create_db_file()
    create_data_dump()
    # add_hse_stuff(Path('AB2/1MoS2/1'), Path())
