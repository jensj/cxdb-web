from __future__ import annotations

from camdweb.html import table, HTML_ROW as HTML
from camdweb.material import Material
from camdweb.c2db.asr_panel import Row
from camdweb.panels.panel import Panel, PanelData, SkipPanel


class BasicElectronicPropertiesPanel(Panel):
    datafiles = ['results-asr.gs.json']
    title = 'Basic electronic properties'
    info = """Electronic properties derived from a ground state density
    functional theory calculation.
    """

    def get_data(self, material: Material) -> PanelData:
        gs = material.folder / self.datafiles[0]
        if not (gs.is_file() or gs.with_suffix('.json.gz').is_file()):
            raise SkipPanel

        if round(material.gap, 4) <= 0:
            raise SkipPanel

        html = HTML.format(
            table=self.basic_properties_table(material=material)
        )

        return PanelData(html, title=self.title, info=self.info)

    def basic_properties_table(self, material: Material) -> str:
        data = Row(material).data.get(self.datafiles[0])
        if data['vbm'] is not None:
            data['vbm_evac'] = data['vbm'] - data['evac']
            data['cbm_evac'] = data['cbm'] - data['evac']

        table_rows = self.table_dict(data,
                                     ['dipz', 'evacdiff', 'workfunction',
                                      'cbm_evac', 'vbm_evac'])

        return table(['', ''], table_rows)
