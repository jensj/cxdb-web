import numpy as np
from scipy.constants import eV, m_e, hbar

hbar2pi = hbar * 2 * np.pi
unit_to_electron_mass = hbar2pi**2 / (2 * m_e) * 1e20 / eV


def is_band_warped(warping_parameter):
    return abs(warping_parameter) > (1.5 * 1e-3)


def get_emass_data(data, atoms) -> dict:
    webpanel_data = {}
    band_names = ['vbm', 'cbm']
    if data:
        unit_cell = data['unit_cell']
        webpanel_data['unit_cell'] = unit_cell
        for band_name in band_names:
            if band_name + '_data' not in data:
                continue
            band_data = data[band_name + '_data']
            webpanel_band_data = {}
            for key in band_data:
                band_data[key] = np.asarray(band_data[key])

            band_warped = is_band_warped(band_data['iems_warping'])
            webpanel_band_data['warping'] = band_data['iems_warping']
            coords_ibz = _map_to_IBZ(band_data['coords_cartesian'], atoms)
            coords_kbasis = coords_ibz @ unit_cell.T
            webpanel_band_data['coords'] = coords_kbasis.flatten()

            phi = band_data['iems_phi']
            iems_coefficients_k = band_data['iems_coefficients_ks'][:, 0]
            if band_warped:
                iems_coefficients_k = band_data['iems_coefficients_ks'][:, 0]
                inverse_max_emass = np.min(np.abs(iems_coefficients_k))
                inverse_min_emass = np.max(np.abs(iems_coefficients_k))
                if len(np.unique(np.sign(iems_coefficients_k))) > 1:
                    inverse_max_emass = 0

                max_emass_angle = phi[np.argmin(np.abs(iems_coefficients_k))]
                max_emass_direction = np.array(
                    [np.cos(max_emass_angle), np.sin(max_emass_angle)])
                min_emass_angle\
                    = phi[np.argmax(np.abs(iems_coefficients_k))]
                min_emass_direction = np.array(
                    [np.cos(min_emass_angle), np.sin(min_emass_angle)])
            else:
                eigvals = band_data['fit_eigvals']
                eigvecs = band_data['fit_eigvecs']
                max_emass_idx = np.argmin(abs(eigvals))
                min_emass_idx = (1 - max_emass_idx) % 2
                inverse_max_emass = np.abs(eigvals[max_emass_idx]) / 2
                inverse_min_emass = np.abs(eigvals[min_emass_idx]) / 2
                if np.sign(eigvals[0]) != np.sign(eigvals[1]):
                    inverse_max_emass = 0
                max_emass_direction = eigvecs[:, max_emass_idx]
                min_emass_direction = eigvecs[:, min_emass_idx]

            if inverse_min_emass > 0:
                min_emass = (1 / inverse_min_emass) * unit_to_electron_mass
            else:
                min_emass = np.inf

            if inverse_max_emass > 0:
                max_emass = (1 / inverse_max_emass) * unit_to_electron_mass
            else:
                max_emass = np.inf

            webpanel_band_data['min_emass'] = min_emass
            webpanel_band_data['max_emass'] = max_emass
            webpanel_band_data['min_emass_direction'] = min_emass_direction
            webpanel_band_data['max_emass_direction'] = max_emass_direction
            if band_warped:
                m_dos = band_data['iems_m_dos'] * unit_to_electron_mass
            else:
                if inverse_max_emass * inverse_min_emass > 0:
                    m_dos = unit_to_electron_mass\
                        / np.sqrt(inverse_max_emass * inverse_min_emass)
                else:
                    m_dos = np.inf
            webpanel_band_data['m_dos'] = m_dos
            X = band_data['contour_kx']
            Y = band_data['contour_ky']
            f0 = band_data['fit_f0']
            Z = band_data['contour_energies'] - f0

            webpanel_band_data['X'] = X
            webpanel_band_data['Y'] = Y
            webpanel_band_data['Z'] = Z
            energy_levels = band_data['barrier_levels']
            dx = X[0, 1] - X[0, 0]
            barrier_R = band_data['barrier_R']
            barrier_found = False
            if np.any(np.diff(barrier_R) > 2.9 * dx):
                discont_idx = np.nonzero(
                    np.diff(barrier_R) > 2.9 * dx)[0][0]
                dist_to_barrier = barrier_R[discont_idx]  # size of cbm in 1/Å
                barrier_found = True
            # depth of cbm in meV
                extremum_depth = energy_levels[discont_idx] * 1000

            else:
                dist_to_barrier = barrier_R.max()
                extremum_depth = energy_levels.max() * 1000

            webpanel_band_data['barrier_found'] = barrier_found
            webpanel_band_data['dist_to_barrier'] = dist_to_barrier
            webpanel_band_data['extremum_depth'] = extremum_depth

            webpanel_data[band_name] = webpanel_band_data
    return webpanel_data


def _map_to_IBZ(kpts, atoms, tolerance=1e-3, debug=False):

    def angle_between_points(p1, p2, p3):
        v1 = p2 - p1
        v2 = p3 - p1
        angle = np.arctan2(np.linalg.det([v1, v2]), np.dot(v1, v2))
        return angle

    def is_point_inside_polygon(points, polygon_sides):
        num_vertices = polygon_sides.shape[0]
        inside_polygon = np.zeros(points.shape[0], dtype=bool)

        for i in range(points.shape[0]):
            point = points[i]
            point_is_vertex = np.any(np.all(np.isclose(polygon_sides,
                                                       point), axis=1))
            if point_is_vertex:
                inside_polygon[i] = True
                continue
            angle_sum = 0
            for j in range(num_vertices):
                p1 = polygon_sides[j]
                p2 = polygon_sides[(j + 1) % num_vertices]
                angle = angle_between_points(point, p1, p2)
                if np.isclose(abs(angle), np.pi):
                    #  hack to make the loop logic work
                    #  we want inside_polygon to be true in this case
                    angle_sum = 2 * np.pi
                    break
                angle_sum += angle
            inside_polygon[i] = np.isclose(abs(angle_sum), 2 * np.pi)
        return inside_polygon

    def _map_single_k_to_ibz(kpt, sym_op_scc, ibz_polygon, debug):
        #  map out k-points with lattice symmetry operations
        unique_kpts = get_equivalent_kpts(kpt, sym_op_scc)
        kpt_is_in_ibz = is_point_inside_polygon(unique_kpts, ibz_polygon)
        if debug and kpt_is_in_ibz.sum() > 1:
            print('multiple k in ibz found!')
        kpts_in_ibz = unique_kpts[kpt_is_in_ibz]
        if len(kpts_in_ibz) == 0:
            if debug:
                print('no ibz kpt found for ', kpt)
            kpt_in_ibz = kpt
            # sym_op = np.identity(2)
        else:
            kpt_in_ibz = kpts_in_ibz[0]
            # sym_op = sym_op_scc[kpt_is_in_ibz][0]
        return kpt_in_ibz  # , sym_op

    def get_equivalent_kpts(kpt, sym_op_scc):
        unique_kpts = [kpt]
        for sym_op_cc in sym_op_scc:
            potential_new_kpt = sym_op_cc @ kpt
            append_if_unique(unique_kpts, potential_new_kpt)
        return np.asarray(unique_kpts)

    def append_if_unique(_list, new_entry):
        if not np.any(np.all(np.isclose(new_entry, _list), axis=1)):
            _list.append(new_entry)
        return _list

    kpts = np.asarray(kpts)
    if len(kpts.shape) == 1:
        kpts = kpts[np.newaxis]

    reciprocal_cell = atoms.cell.reciprocal()[:2, :2]
    cell = np.linalg.inv(reciprocal_cell)

    kpts_kbasis = _map_to_1BZ(kpts, atoms) @ cell
    sym_op_scc = find_lattice_symmetry(atoms, tolerance)

    #  construct ibz as polygon
    special_points = atoms.cell.bandpath(pbc=atoms.pbc, npoints=0,
                                         eps=tolerance).special_points
    ibz_polygon = np.asarray(list(special_points.values()))[:, :2]
    kpts_ibz = []
    for kpt in kpts_kbasis:
        kpt_ibz = _map_single_k_to_ibz(kpt, sym_op_scc, ibz_polygon, debug)
        kpts_ibz.append(kpt_ibz)

    kpts_ibz = np.asarray(kpts_ibz) @ reciprocal_cell
    return kpts_ibz


def _map_to_1BZ(_kpts, atoms):
    reciprocal_unit_cell = np.array(atoms.cell.reciprocal())
    unit_cell = np.array(atoms.cell)
    kpts_shape = _kpts.shape
    kpts = _kpts.copy()
    if len(kpts_shape) == 1:
        kpts = kpts.reshape(1, -1)
    if kpts_shape[-1] == 2:
        kpts = np.concatenate((kpts, np.zeros((len(kpts), 1))), axis=1)
        truncate_output = True
    else:
        truncate_output = False
    kpts_kbasis = kpts @ unit_cell.T
    # the lines below remove integer factors of reciprocal basis vectors
    # such that the elements of kpts_kbasis lie in the interval [-0.5,0.5)
    kpts_kbasis[:, 0] = (kpts_kbasis[:, 0] + 0.5) % 1 - 0.5
    kpts_kbasis[:, 1] = (kpts_kbasis[:, 1] + 0.5) % 1 - 0.5
    k_ka = kpts_kbasis @ reciprocal_unit_cell
    G1 = reciprocal_unit_cell[0]
    G2 = reciprocal_unit_cell[1]
    NearestReciprocalNeighbors =\
        np.array([np.array([0, 0, 0]), G1, G2, -G1, -G2, G1 - G2,
                  G2 - G1, G1 + G2, -G1 - G2])
    k1BZ_ka = np.zeros(k_ka.shape)

    for i, k in enumerate(k_ka):
        distToNeighbors = np.linalg.norm(k - NearestReciprocalNeighbors,
                                         axis=1)
        closestNeighborIdx = np.argmin(distToNeighbors)
        repetitions = 0
        while closestNeighborIdx != 0 and repetitions < 10:
            repetitions += 1
            k = k - NearestReciprocalNeighbors[closestNeighborIdx]
            distToNeighbors = np.linalg.norm(
                k - NearestReciprocalNeighbors, axis=1)
            closestNeighborIdx = np.argmin(distToNeighbors)
        if repetitions == 10:
            print(
                'Warning - map to 1BZ did not converge in %d iterations!'
                % repetitions, flush=True)
        k1BZ_ka[i, 0] = k[0]
        k1BZ_ka[i, 1] = k[1]
    if truncate_output:
        k1BZ_ka = k1BZ_ka[:, :2]
    return k1BZ_ka.reshape(kpts_shape)


def find_lattice_symmetry(atoms, tolerance=1e-7):
    # This function was hijacked from gpaw.symmetry
    """Determine list of symmetry operations."""
    # Symmetry operations as matrices in 123 basis.
    # Operation is a 3x3 matrix, with possible elements -1, 0, 1, thus
    # there are 3**9 = 19683 possible matrices:
    combinations = 1 - np.indices([3] * 9)
    U_scc = combinations.reshape((3, 3, 3**9)).transpose((2, 0, 1))
    cell_cv = atoms.cell
    pbc_c = atoms.pbc
    # The metric of the cell should be conserved after applying
    # the operation:
    metric_cc = cell_cv.dot(cell_cv.T)
    metric_scc = np.einsum('sij, jk, slk -> sil',
                           U_scc, metric_cc, U_scc,
                           optimize=True)
    mask_s = abs(metric_scc - metric_cc).sum(2).sum(1) <= tolerance
    U_scc = U_scc[mask_s]

    # Operation must not swap axes that don't have same PBC:
    pbc_cc = np.logical_xor.outer(pbc_c, pbc_c)
    mask_s = ~U_scc[:, pbc_cc].any(axis=1)
    U_scc = U_scc[mask_s]

    # Operation must not invert axes that are not periodic:
    mask_s = (U_scc[:, np.diag(~pbc_c)] == 1).all(axis=1)
    U_scc = U_scc[mask_s]

    op_scc = U_scc[:, :2, :2]

    return op_scc
