"""Pick the Hubbard-U folders that we want to show.

Only those with a gap.

Write a uid.json file in those folders with a uid_no_u value.
"""

from camdweb.c2db.copy import ROOT, PATTERNS_U
from ase.db import connect
import json
from pathlib import Path


def main():
    old_uids = set()
    for row in connect(ROOT / 'c2db_U4/u_materials.db').select('vbm'):
        gap = row.cbm - row.vbm
        assert gap > 0, (row.folder, row.uid)
        old_uids.add(row.uid)
    print(len(old_uids))
    old2new_uids = json.loads(Path('uid_translation.json').read_text())
    n = 0
    for path in ROOT.glob(PATTERNS_U[0]):
        info = path / 'info.json'
        d = json.loads(info.read_text())
        old_uid = d['uid_noU']
        if old_uid in old_uids:
            if old_uid in old2new_uids:
                uid_no_u = old2new_uids[old_uid]
                p = path / 'uid.json'
                assert not p.is_file()
                p.write_text(json.dumps({'uid_no_u': uid_no_u}))
                n += 1
            else:
                print(old_uid, '?')
    print(n)


if __name__ == '__main__':
    main()
