"""Basic magnetic properties."""
from __future__ import annotations

import json

from camdweb.html import table, complex_table_head, HTML_4COL as HTML
from camdweb.panels.panel import Panel, PanelData, SkipPanel
from camdweb.c2db.asr_panel import INFO


class BasicMagneticProperties(Panel):
    title = 'Basic magnetic properties'

    def __init__(self, name: str = 'PBE'):
        super().__init__()
        info = INFO['Basic magnetic properties']
        self.name = name
        if name.lower() == 'pbe':
            self.mode = ''
        elif name.lower() == 'pbe+u':
            info = info.replace('PBE xc-functional', 'PBE+U xc-functional')
            self.mode = '_u'
        self.datafiles = [f'magmoms{self.mode}.json']
        self.title = f'{self.title} ({self.name})'
        self.info = info

    def get_data(self, material):
        if not getattr(material, f'is_magnetic{self.mode}', False):
            raise SkipPanel

        # Some of the rows of magmoms and ma table should always
        # be present so no need to check if they are in data
        magmoms = self.magmoms_table(material)
        mag_aniso = self.magnetic_anisotropy_table(material)

        tab_heisenberg = ''
        if f'J{self.mode}' in material:
            tab_heisenberg = self.exchange_table(material)

        tab_hm = ''
        if f'halfmetal_gap{self.mode}' in material:
            tab_hm = self.halfmetal_table(material)

        return PanelData(
            HTML.format(col1=mag_aniso, col2=tab_heisenberg, col3=magmoms,
                        col4=tab_hm),
            title=self.title,
            info=self.info,
        )

    def magmoms_table(self, material):
        data = json.loads((material.folder / self.datafiles[0]).read_text())
        magmoms_header = [['Atom index_span=1,2', 'Atom type_span=1,2',
                           'Local magnetic moment (μ<sub>B</sub>)_span=2,1'],
                          ['Spin', 'Orbital']]
        magmoms_head = complex_table_head(magmoms_header, col_spacing='')
        if 'orbmag_a' not in data:
            magmoms_rows = [[str(a), symbol, f'{magmom:.3f}', '--']
                            for a, (symbol, magmom)
                            in enumerate(zip(material.atoms.symbols,
                                             data['magmoms']))]
        else:
            magmoms_rows = [[str(a), symbol, f'{magmom:.3f}', f'{orbmag:.3f}']
                            for a, (symbol, magmom, orbmag)
                            in enumerate(zip(material.atoms.symbols,
                                             data['magmoms'],
                                             data['orbmag_a']))]

        return table(magmoms_head, magmoms_rows, col_spacing='')

    def exchange_table(self, material):
        base_names = ['J', 'A', 'lam', 'spin', 'N_nn']
        name_keys = [f'{k}{self.mode}' for k in base_names]
        names = {n: self.column_descriptions.get(b, b)
                 for b, n in zip(base_names, name_keys)}
        rows = self.table_rows(
            material,
            name_keys,
            updated_names=names
        )
        return table(['Heisenberg model', ''], rows)

    def magnetic_anisotropy_table(self, material):
        base_names = ['magmom', 'dE_zx', 'dE_zy']
        name_keys = [f'{k}{self.mode}' for k in base_names]
        names = {n: self.column_descriptions.get(b, b)
                 for b, n in zip(base_names, name_keys)}
        rows = self.table_rows(
            material,
            name_keys,
            updated_names=names
        )
        return table(['', ''], rows)

    def halfmetal_table(self, material):
        rows = self.table_rows(
            material,
            [f'{k}{self.mode}' for k in ['halfmetal_gap', 'halfmetal_gap_dir',
             'halfmetal_gap_hse', 'halfmetal_gap_dir_hse']])
        return table(['', ''], rows)
