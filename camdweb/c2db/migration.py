import json
from collections import defaultdict
from pathlib import Path

from ase.formula import Formula
from camdweb.c2db.copy import (PATTERNS, ROOT, atoms_to_uid_name,
                               read_result_file)
from camdweb.utils import all_dirs, read_atoms

fro = Path('/home/niflheim/joaso/paper_calcs/htp-spiral_paper/n1-magnets/tree')
fro = Path('/home/niflheim/jensj/spin-spiral/')
to = Path('/home/niflheim2/cmr/C2DB-ASR/')


def copy_spinspiral_data_to_tree():  # pragma: no cover
    dirs = [dir
            for pattern in PATTERNS
            for dir in to.glob(pattern)
            if dir.name[0] != '.']
    paths = {}
    for dir in dirs:
        fp = dir / 'results-asr.database.material_fingerprint.json'
        try:
            olduid = read_result_file(fp)['uid']
        except FileNotFoundError:
            print('No fingerprint:', fp)
            continue
        f = Formula(olduid.split('-')[0])
        stoi, reduced, nunits = f.stoichiometry()
        path = fro / f'{stoi}/{f}/{olduid}'
        paths[path] = dir

    print(len(paths))
    for path in fro.glob('A*/*/*/'):
        if path not in paths:
            print(path)
            continue
        dir = paths.pop(path)
        for name in ['collect_spiral',
                     'dmi',
                     'spinorbit',
                     'spinorbit@calculate']:
            f1 = path / f'results-asr.{name}.json'
            if f1.is_file():
                f2 = dir / f1.name
                assert not f2.is_file()
                if 1:
                    f2.write_bytes(f1.read_bytes())
                else:
                    print(f1, f2)
    print(len(paths))


def create_initial_uids_for_c2db(root: Path = ROOT,
                                 patterns: list[str] = PATTERNS) -> None:
    """Used at the beginning to bootstrap things."""
    names: defaultdict[str, int] = defaultdict(int)
    new = []
    for dir in all_dirs(root, patterns):
        print(dir)
        try:
            atoms = read_atoms(dir / 'structure.json')
        except FileNotFoundError:
            print('ERROR:', dir)
            continue
        energy = atoms.get_potential_energy()
        uid = None
        olduid = None
        try:
            uid_data = json.loads((dir / 'uid.json').read_text())
        except FileNotFoundError:
            pass
        else:
            uid = uid_data['uid']
            olduid = uid_data.get('olduid')

        if olduid is None:
            fp = dir / 'results-asr.database.material_fingerprint.json'
            try:
                olduid = read_result_file(fp)['uid']
            except FileNotFoundError:
                pass

        name = atoms_to_uid_name(atoms)
        if uid:
            number = int(uid.split('-')[1])
            names[name] = max(names[name], number)
        else:
            new.append((name, energy, dir, olduid))

    print(len(new))
    for name, _, dir, olduid in sorted(new):
        number = names[name] + 1
        uid = f'{name}-{number}'
        names[name] = number
        uid_data = {'uid': uid}
        if olduid:
            uid_data['olduid'] = olduid
        (dir / 'uid.json').write_text(json.dumps(uid_data, indent=2))


if __name__ == '__main__':
    copy_spinspiral_data_to_tree()
