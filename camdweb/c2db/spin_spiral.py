from typing import Any

import matplotlib.pyplot as plt
import numpy as np

from camdweb.c2db.asr_panel import read_result_file
from camdweb.html import image, table
from camdweb.material import Material
from camdweb.panels.panel import Panel, PanelData

HTML = """
<div class="row row-cols-1 row-cols-lg-2">
  <div class="col">
    {image1}
  </div>
  <div class="col">
    {table1}
    {table2}
  </div>
</div>
<div class="row row-cols-1 row-cols-lg-2">
  <div class="col">
    {image2}
  </div>
  <div class="col">
    {table3}
  </div>
</div>
"""

endash = '<span>&#8211;</span>'


class SpinSpiralPanel(Panel):
    datafiles = ['results-asr.collect_spiral.json',
                 'results-asr.spinorbit.json',
                 'results-asr.spinorbit@calculate.json']
    title = 'Spin spirals'
    info = """The spin spiral energy is shown as a function of ordering
        vector q for planar spin spirals. The vector that minimizes the energy
        defines the ground state magnetic order Q_min. We state the electronic
        band gap of the magnetic ground state as well as the spiral dispersion
        band width (difference between minimum and maximum energies),
        which provides a measure of the magnetic interactions. The spin-orbit
        energy E_soc is shown as a stereographic plot of the upper hemisphere,
        where the direction represents the magnetization direction for
        ferromagnets (Q_min at the zone center) and the direction of the normal
        to the spiral plane otherwise. For non-centrosymmetric ferromagnets the
        Dzyaloshinskii-Moriya (DM) vector at the zone center is stated as well.

        <br><br>Relevant articles:

        <br><a href=https://doi.org/10.48550/arXiv.2309.11945>
        J. Sødequist and T. Olsen,
        Magnetic order in the computational 2D materials database (C2DB) from
        high throughput spin spiral calculations.
        arXiv preprint arXiv:2309.11945
        </a>
        """

    def get_data(self,
                 material: Material) -> PanelData:
        folder = material.folder

        spiral_data = read_result_file(folder / self.datafiles[0])
        soc_data = read_result_file(folder / self.datafiles[1])
        soccalc_data = read_result_file(folder / self.datafiles[2])

        imgpath1, imgpath2, tbl1_rows, tbl3_rows = make_figures(
            spiral_data, soc_data, soccalc_data, material)

        try:
            dmi_data = read_result_file(folder / 'results-asr.dmi.json')
        except FileNotFoundError:
            tbl2 = ''
        else:
            tbl2_rows = make_dmi_table(dmi_data)
            tbl2 = table(['Dzyaloshinskii-Moriya vector @ &Gamma;'], tbl2_rows)

        img1 = image(imgpath1, alt='Spin spiral dispersion')
        tbl1 = table(['Spin spiral properties', ''], tbl1_rows)
        img2 = image(imgpath2, alt='Magnetic anisotropy energy')
        tbl3 = table(['Spin' + endash +
                      'orbit properties @ <b>Q</b><sub>min</sub>'],
                     tbl3_rows)

        return PanelData(HTML.format(image1=img1, table1=tbl1, table2=tbl2,
                                     image2=img2, table3=tbl3),
                         title=self.title,
                         info=self.info)


def make_figures(spiral_data: dict[str, Any],
                 soc_data: dict[str, Any],
                 soccalc_data: dict[str, Any],
                 material: Material):
    folder = material.folder

    bmin, qmin = spiral_data['minimum']
    tbl1_rows = [['<b>Q</b><sub>min</sub>',
                  np.array2string(np.array(spiral_data['Qmin']),
                                  precision=3)],
                 ['Band gap (<b>Q</b><sub>min</sub>) [eV]',
                  f'{spiral_data["gaps"][bmin][qmin]:.2f}'],
                 ['Spiral bandwidth [meV]',
                  f'{spiral_data["bandwidth"]:.1f}']]

    tbl3_rows = [['Spin' + endash + 'orbit bandwidth [meV]',
                  f'{soc_data["soc_bw"]:.1f}'],
                 ['Spin' + endash + 'orbit minimum ' +
                  '(<i>&theta;</i>, <i>&phi;</i>)',
                  f'({soc_data["theta_min"]:.1f}, {soc_data["phi_min"]:.1f})']]

    imgpath1 = folder / 'spin_spiral_bs.png'
    imgpath2 = folder / 'spinorbit.png'

    if not imgpath1.is_file():
        plot_spiral_dispersion(spiral_data, material)
    if not imgpath2.is_file():
        plot_stereographic_energies(soccalc_data, material)

    return imgpath1, imgpath2, tbl1_rows, tbl3_rows


def plot_spiral_dispersion(spiral_data, material):
    path, energies_bq, lm_bqa = extract_data(spiral_data)

    # Process path
    Q, x, X = path.get_linear_kpoint_axis()
    nwavepoints = 5
    q_v = np.linalg.norm(2 * np.pi * path.cartesian_kpts(), axis=-1)
    wavepointsfreq = round(len(q_v) / nwavepoints)

    # Process magmoms
    magmom_bq = np.linalg.norm(lm_bqa, axis=3)
    mommin = np.min(magmom_bq * 0.9)
    mommax = np.max(magmom_bq * 1.05)

    # Process energies
    e0 = energies_bq[0][0]
    emin = np.min((energies_bq[energies_bq != 0] - e0) * 1000)
    emax = np.max((energies_bq[energies_bq != 0] - e0) * 1000)
    nbands, nqpts = np.shape(energies_bq)

    symbols = material.atoms.symbols

    fig = plt.figure(figsize=((1 + np.sqrt(5)) / 2 * 4, 4))
    ax1 = plt.gca()

    # Non-cumulative length of q-vectors to find wavelength
    ax2 = ax1.twiny()
    add_wavelength_axis(ax2, Q, q_v, wavepointsfreq)

    # Magnetic moment axis
    ax3 = ax1.twinx()
    for bidx in range(nbands):
        e_q = energies_bq[bidx]
        m_q = magmom_bq[bidx]

        splitarg = np.argwhere(e_q == 0.0).flatten()
        if splitarg is []:
            energies_sq = [e_q]
            magmom_sq = [m_q]
            q_s = [Q]
        else:
            energies_sq = np.split(e_q, splitarg)
            magmom_sq = np.split(m_q, splitarg, axis=0)
            q_s = np.split(Q, splitarg)

        for q, energies_q, magmom_q in zip(q_s, energies_sq, magmom_sq):
            if len(q) == 0 or len(q[energies_q != 0]) == 0:
                continue
            magmom_q = magmom_q[energies_q != 0]
            q = q[energies_q != 0]
            energies_q = energies_q[energies_q != 0]
            if len(q) == 0:
                continue

            # Setup main energy plot
            hnd = plot_energies(ax1, q, (energies_q - e0) * 1000,
                                emin, emax, x, X)

            # Add the magnetic moment plot
            plot_magmoms(ax3, q, magmom_q, mommin, mommax, symbols)

            # Ensure unique legend entries
            handles, labels = plt.gca().get_legend_handles_labels()
            by_label = dict(zip(labels, handles))
            updict = {'Energy': hnd[0]}
            updict.update(by_label)

    # Add legend
    fig.legend(updict.values(), updict.keys(), loc="upper right",
               bbox_to_anchor=(1, 1), bbox_transform=ax1.transAxes)

    ax1.margins(x=0, y=1e-3 * (emax - emin))
    ax3.margins(x=0, y=1e-3 * (mommax - mommin))
    ax1.set_ylabel('Spin spiral energy [meV]')
    ax1.set_xlabel('q vector [Å$^{-1}$]')

    ax2.set_xlabel(r"Wave length $\lambda$ [Å]")
    ax3.set_ylabel(r"Mag. mom. $|\mathbf{m}|$ [$\mu_B$]")

    plt.tight_layout()
    plt.savefig(material.folder / 'spin_spiral_bs.png')


def extract_data(data):
    path = data['path']
    energies_bq = data['energies']
    if len(energies_bq.shape) == 1:
        energies_bq = np.array([energies_bq])
        lm_bqa = np.array([data['local_magmoms']])
    else:
        lm_bqa = data['local_magmoms']
    return path, energies_bq, lm_bqa


def plot_energies(ax, q, energies, emin, emax, x, X):
    hnd = ax.plot(q, energies, c='C0', marker='.', label='Energy')

    ax.set_xticks(x)
    ax.set_xticklabels([i.replace('G', r"$\Gamma$") for i in X])
    for xc in x:
        if xc != min(q) and xc != max(q):
            ax.axvline(xc, c='gray', linestyle='--')
    return hnd


def add_wavelength_axis(ax, q, q_v, wavepointsfreq):
    # Add spin wavelength axis
    def tick_function(X):
        with np.errstate(divide='ignore'):
            lmda = 2 * np.pi / X
        return [f"{z:.1f}" for z in lmda]

    ax.set_xticks(q[::wavepointsfreq])
    ax.set_xticklabels(tick_function(q_v[::wavepointsfreq]))


def plot_magmoms(ax, q, magmoms_qa, mommin, mommax, symbols):
    unique = list(set(symbols))
    colors = [f'C{i}' for i in range(1, len(unique) + 1)]
    mag_c = {unique[i]: colors[i] for i in range(len(unique))}

    for a in range(magmoms_qa.shape[-1]):
        magmom_q = magmoms_qa[:, a]
        ax.plot(q, magmom_q, c=mag_c[symbols[a]],
                marker='.', label=f'{symbols[a]}' + r' $|\mathbf{m}|$')

    ax.set_ylim([mommin, mommax])


def get_projected_points(theta_tp, phi_tp):
    def stereo_project_point(inpoint, axis=0, r=1, max_norm=1):
        point = np.divide(inpoint * r, inpoint[axis] + r)
        point[axis] = 0
        return point

    theta_tp *= np.pi / 180
    phi_tp *= np.pi / 180
    x = np.sin(theta_tp) * np.cos(phi_tp)
    y = np.sin(theta_tp) * np.sin(phi_tp)
    z = np.cos(theta_tp)
    points = np.array([x, y, z]).T
    projected_points = []
    for p in points:
        projected_points.append(stereo_project_point(p, axis=2))

    return projected_points


def plot_stereographic_energies(soccalc_data, material,
                                display_sampling=False):
    soc = (soccalc_data['soc_tp'] - min(soccalc_data['soc_tp'])) * 10**3
    projected_points = get_projected_points(soccalc_data['theta_tp'],
                                            soccalc_data['phi_tp'])
    _plot_stereographic_energies(projected_points, soc,
                                 material, display_sampling)


def _plot_stereographic_energies(projected_points, soc,
                                 material, display_sampling=False):
    from matplotlib.colors import Normalize
    from scipy.interpolate import griddata

    plt.figure(figsize=(5 * 1.25, 5))
    ax = plt.gca()

    norm = Normalize(vmin=min(soc), vmax=max(soc))
    X, Y, Z = np.array(projected_points).T
    xi = np.linspace(min(X), max(X), 100)
    yi = np.linspace(min(Y), max(Y), 100)
    zi = griddata((X, Y), soc, (xi[None, :], yi[:, None]))
    ax.contour(xi, yi, zi, 15, linewidths=0.5, colors='k', norm=norm)
    ax.contourf(xi, yi, zi, 15, cmap=plt.cm.jet, norm=norm)
    if display_sampling:
        ax.scatter(X, Y, marker='o', c='k', s=5)

    ax.axis('equal')
    ax.set_xlim(-1.05, 1.05)
    ax.set_ylim(-1.05, 1.05)
    ax.set_xticks([])
    ax.set_yticks([])
    cbar = plt.colorbar(plt.cm.ScalarMappable(norm=norm, cmap='jet'), ax=ax)
    cbar.ax.set_ylabel(r'$E_\mathrm{soc} [meV]$')
    plt.savefig(material.folder / 'spinorbit.png')


def make_dmi_table(dmi_data):
    D_nqv = dmi_data['D_nqv']
    D_nv = []
    for D_qv in D_nqv:
        D_nv.append(D_qv[0])
    D_nv = np.round(np.array(D_nv), 2) + 0.

    # Estimate error
    en_nq = dmi_data['en_nq']
    abs_error_threshold = 1.1e-7
    rel_error_threshold = 1.1e-8

    error_marker = []
    for n in range(len(en_nq)):
        abs_error = en_nq[n][0] - en_nq[n][1]
        try:
            rel_error = abs_error / en_nq[n][0]
        except ZeroDivisionError:
            rel_error = 0

        if abs(abs_error) > abs_error_threshold or \
           abs(rel_error) > rel_error_threshold:
            error_marker.append('*')
        else:
            error_marker.append('')

    tbl2_rows = [['<b>D</b>(q<sub>' + ['a1', 'a2', 'a3'][n] +
                  '</sub>) (meV / Å<sup>-1</sup>)',
                 f"{np.array2string(D_nv[n], precision=2)}{error_marker[n]}"]
                 for n in range(len(D_nv))]

    return tbl2_rows
