from camdweb.c2db.asr_panel import Row
from camdweb.html import table, HTML_1COL as HTML
from camdweb.material import Material
from camdweb.panels.panel import Panel, PanelData


class SpontaneousPolarizationPanel(Panel):
    datafiles = ['results-asr.spontaneous_polarization.json']
    title = 'Spontaneous polarization'
    info = """A macroscopic electric polarization density can occur in
    materials with a polar point group. We distinguish between permanent and
    spontaneous polarization. In the latter case, the ground state structure
    is ‘close’ to a non-polar structure, and therefore can be switched
    leading ferroelectric behavior. Spontaneous polarizations have been
    calculated by integrating the Berry phase along an insulating path
    connecting the polar ground state to the nearby non-polar structure.

    <br><br>Relevant articles:

    <br><a href=https://doi.org/10.1038/s41524-023-00999-5>
    M. Kruse et al.,
    Two-dimensional ferroelectrics from high throughput computational screening
    npj Computational Materials
    </a>
    """

    def get_data(self,
                 material: Material) -> PanelData:
        return PanelData(HTML.format(col1=self.polarization_table(material)),
                         title=self.title,
                         info=self.info)

    def polarization_table(self, material: Material) -> str:
        data = Row(material).data.get(self.datafiles[0])
        return table(['Spontaneous polarization vector component [pC/m]', ''],
                     self.table_dict(data, ['Px', 'Py', 'Pz']))
