from pathlib import Path

from camdweb.c2db.asr_panel import ASRPanel
from camdweb.c2db.berry_phase import BerryPhase
from camdweb.c2db.bs_dos_bz_panel import BSDOSBZPanel
from camdweb.c2db.polarizability import IRPolarizability, OpticalPolarizability
from camdweb.c2db.shift_current import ShiftCurrentPanel
from camdweb.c2db.spin_spiral import SpinSpiralPanel
from camdweb.c2db.spontaneous_polarization import SpontaneousPolarizationPanel
from camdweb.material import Material
from camdweb.panels.bader import BaderPanel
from camdweb.panels.convex_hull import ConvexHullPanel
from camdweb.panels.emass import EmassPanel
from camdweb.panels.panel import Panel, PanelData
from camdweb.c2db.magnetic_properties import BasicMagneticProperties

OQMD = 'https://cmrdb.fysik.dtu.dk/oqmd123/material/'

CONVEX_HULL_INFO = """The heat of formation (ΔH) is defined as the internal
energy of a compound relative to the standard states of the constituent
elements at T=0 K. The energy above the convex hull is the internal energy
relative to the most stable (possibly mixed) phase of the constituent
elements at T=0 K. The convex hull energy is evaluated with a reference
database containing the C2DB monolayers and all bulk materials from the OQMD
database with up to 3 different elements. All energies are calculated with the
PBE xc-functional.

<br><br>Relevant articles:

<br>Haastrup2018
"""


class StabilityPanel(Panel):
    html = ''
    title = 'Stability'
    info = """The stability of a material can be classified into thermodynamic
    stability and dynamical stability. The former is a global concept
    quantifying the energetic stability of the material relative to other
    possible phases involving the same chemical elements. The latter is a
    local Boolean property indicating whether the structure represents a local
    minimum on the potential energy surface.
    """

    def __init__(self, root: Path):
        self.subpanels = [
            ConvexHullPanel(
                sources={
                    'OQMD': ('Bulk crystals from OQMD123',
                             f'<a href={OQMD}/{{uid}}>{{formula:html}}</a>'),
                    'C2DB': ('Monolayers from C2DB',
                             '{formula:html} (<a href={uid}>{uid}</a>)')},
                info=CONVEX_HULL_INFO,
                root=root),
            ASRPanel('phonons')]

    def get_data(self, material: Material) -> PanelData:
        return self.construct_superpanels(material=material)


class LinearDeformationsPanel(Panel):
    html = ''
    title = 'Linear deformations'
    info = """Response of various physical properties to small strain fields,
    including the response of the stress field (stiffness tensor),
    the valence and conduction band energies (deformation potential),
    and the macroscopic polarization (piezoelectric tensor).
    """
    subpanels = [
        ASRPanel('deformationpotentials'),
        ASRPanel('stiffness'),
        ASRPanel('piezoelectrictensor')]

    def get_data(self, material: Material) -> PanelData:
        return self.construct_superpanels(material=material)


class ElectronicBandsPanel(Panel):
    html = ''
    title = 'Electronic bands'
    info = """Single-particle band structures and derived quantities like
    the projected density of states (PDOS), the effective masses of
    semiconductors/insulators, and the Fermi arc of metals. For some
    materials the band structure obtained with higher level approaches are
    available. The work function and valence/conduction band energies
    provided in the table are referenced to the vacuum potential. For 2D
    materials with an out-of-plane dipole, the vacuum level will differ on
    the two sides of the layer. In such cases, the average vacuum potential
    is used as reference.
    """
    subpanels = [
        BSDOSBZPanel(),
        BSDOSBZPanel('pbe+u'),
        BSDOSBZPanel('hse'),
        BSDOSBZPanel('gw'),
        ASRPanel('fermisurface'),
        EmassPanel(),
    ]

    def get_data(self, material: Material) -> PanelData:
        return self.construct_superpanels(material=material)


class ElectronicPropertiesPanel(Panel):
    html = ''
    title = 'Electronic properties'
    info = """Various electronic ground state properties that are not directly
    derivable from the band structure. All calculations employ the PBE
    xc-functional.
    """
    subpanels = [
        ASRPanel('borncharges'),
        BaderPanel(),
        SpontaneousPolarizationPanel(),
        BerryPhase(),
    ]
    # Ferroelectric polarization (table with three numbers)

    def get_data(self, material: Material) -> PanelData:
        return self.construct_superpanels(material=material)


class MagneticPropertiesPanel(Panel):
    html = ''
    title = 'Magnetic properties'
    info = """Basic magnetic properties and parameters extracted to fit a
    Heisenberg model. In addition to the PBE xc-functional, the PBE+U method
    is employed for materials containing the elements {V, Cr, Mn, Fe, Co,
    Ni, Cu}. For materials with non-trivial magnetic order, the full spin
    spiral ground state obtained with the PBE method is provided.
    """
    subpanels = [
        BasicMagneticProperties(),
        BasicMagneticProperties(name='PBE+U'),
        SpinSpiralPanel(),
    ]

    def get_data(self, material: Material) -> PanelData:
        return self.construct_superpanels(material=material)


class OpticalPropertiesPanel(Panel):
    html = ''
    title = 'Optical properties'
    info = """Various linear and non-linear optical properties. Note that
    the optical polarizability and optical absorbance are essentially the
    same quantities apart from normalization.
    """
    subpanels = [
        OpticalPolarizability(),
        IRPolarizability(),
        ASRPanel('bse'),
        ASRPanel('shg'),
        ShiftCurrentPanel(),
        ASRPanel('raman'),
    ]

    def get_data(self, material: Material) -> PanelData:
        return self.construct_superpanels(material=material)
