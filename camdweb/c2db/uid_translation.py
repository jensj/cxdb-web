"""
Simple script to generate a translation file for converting
old_uid to new_uid in the c2db database.

Usage:
    python -m camdweb.c2db.uid_translation

This will generate a file 'uid_translation.json' in the current
working directory.
"""

import json
from pathlib import Path
from camdweb.c2db.copy import ROOT, PATTERNS


def collect_uids(root: Path, patterns: list[str]) -> dict:
    uid_map = {}
    for pattern in patterns:
        for dir in root.glob(pattern):
            uid_file = dir / 'uid.json'
            if uid_file.is_file():
                uid_data = json.loads(uid_file.read_text())
                if 'olduid' in uid_data:
                    uid = uid_data['uid']
                    olduid = uid_data['olduid']
                    uid_map[olduid] = uid
                uid_file = dir / 'uid_structure_large_vacuum.json'
                if uid_file.is_file():
                    uid_data = json.loads(uid_file.read_text())
                    olduid = uid_data['large_vacuum_uid']
                    uid_map[olduid] = uid
    print('Old uids found:', len(uid_map))
    return uid_map


def convert_uid(olduid: str, uid_map: dict) -> str:
    if olduid not in uid_map:
        raise ValueError(
            f'Old C2DB uid {olduid} not found in uid translation file')
    return uid_map[olduid]


def main():
    uid_map = collect_uids(ROOT, PATTERNS)
    output_file = Path.cwd() / 'uid_translation.json'
    with output_file.open('w') as f:
        json.dump(uid_map, f, indent=2)
    print(f'UID map saved to {output_file}')


if __name__ == '__main__':
    main()
