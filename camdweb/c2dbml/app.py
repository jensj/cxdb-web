"""
C2DB-ML web-app
================
Look for TODO's in the code and update them.
"""
from __future__ import annotations

import argparse
import json
from pathlib import Path

import rich.progress as progress
from camdweb.html import (stoichiometry_input, image, table)
from camdweb.materials import Material, Materials
from camdweb.panels.panel import Panel
# from camdweb.c2db.superpanels import StabilityPanel
from camdweb.panels.atoms import AtomsPanel
from camdweb.panels.structureinfo import CrystalStructurePanel
from camdweb.web import CAMDApp


class C2DBMLAtomsPanel(AtomsPanel):
    def create_column_one(self,
                          material: Material) -> str:
        html1 = table(['Structure info', ''],
                      self.table_rows(material,
                                      ['layergroup', 'lgnum', 'label',
                                       'cod_id', 'icsd_id', 'doi']),
                      col_spacing='')
        html2 = table(['Stability', ''],
                      self.table_rows(material,
                                      ['dyn_stab']))
        html3 = table(['Basic properties', ''],
                      self.table_rows(material,
                                      ['is_magnetic', 'gap', 'magmom']))
        return '\n'.join([html1, html2, html3])


class C2DBMLApp(CAMDApp):
    title = 'C2DB-ML'
    logo = image('c2dbml-logo.png', alt='C2DB-ML-logo')
    links = [
        ('CMR', 'https://cmr.fysik.dtu.dk'),
        ('About', 'https://cmr.fysik.dtu.dk/c2dbml/c2dbml.html')]
    # TODO: Update link

    def __init__(self,
                 materials: Materials,
                 initial_columns: list[str],
                 root: Path | None = None):
        super().__init__(materials,
                         initial_columns=initial_columns,
                         root=root)


def main(argv: list[str] | None = None) -> CAMDApp:
    """Create C2DB-ML app."""
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'pattern', nargs='*', default=['*/*/*/'],
        help='Glob-pattern: materials/<pattern>.  Default is "*/*/*/".')
    args = parser.parse_args(argv)

    root = Path()
    folders = []
    for pattern in args.pattern:
        for p in (root / 'materials').glob(pattern):
            folders.append(p)

    mlist: list[Material] = []
    with progress.Progress() as pb:
        pid = pb.add_task('Reading materials:', total=len(folders))
        for f in folders:
            uid = f'{f.parent.name}-{f.name}'
            material = Material.from_json(f / 'structure.json', uid)
            data = json.loads((f / 'data.json').read_text())
            material.columns.update(data)
            mlist.append(material)
            pb.advance(pid)

    panels: list[Panel] = [
        C2DBMLAtomsPanel(),
        CrystalStructurePanel()]
# StabilityPanel()] TODO: Add StabilityPanel after phonon calculations

    materials = Materials(mlist, panels)

    materials.column_descriptions.update(
        has_inversion_symmetry='Inversion symmetry',
        evac='Vacuum level [eV]',
        hform='Heat of formation [eV/atom]',
        magstate='Magnetic state',
        is_magnetic='Magnetic',
        ehull='Energy above hull [eV/atom]',
        energy='Energy [eV]',
        dyn_stab='Dynamically stable',
        cod_id='COD id of parent bulk structure',
        icsd_id='ICSD id of parent bulk structure',
        doi='Mono/few-layer report(s)',
        layergroup='Layer group',
        lgnum='Layer group number',
        label='Structure origin',
        thickness='Thickness [Å]',
        bravais_type='2D Bravais type',
        international='Space group (bulk in AA-stacking)',
        pointgroup='Point group',
        number='Space group number (bulk in AA-stacking)',
        gap='Predicted band gap (PBE) [eV]',
        folder='Original file-system folder',
        magmom='Total magnetic moment [μ<sub>B</sub>]'
    )
    initial_columns = ['formula', 'gap', 'is_magnetic',
                       'layergroup']
    root = folders[0].parent.parent.parent
    app = C2DBMLApp(materials,
                    initial_columns,
                    root)
    app.form_parts += [
        stoichiometry_input(materials.stoichiometries())]
    return app


def create_app():  # pragma: no cover
    """Create the WSGI app."""
    return main([str(path) for path in Path().glob('A*/')]).app


def check_all(pattern: str):  # pragma: no cover
    """Generate png-files."""
    c2dbml = main([str(path) for path in Path().glob(pattern)])
    for material in c2dbml.materials:
        print(material.uid)
        c2dbml.material_page(material.uid)


if __name__ == '__main__':
    main().app.run(host='0.0.0.0', port=8089, debug=True)  # TODO: Update port
