"""
TODO: Run structureinfo in the material trees. :DONE
TODO: Add the phonons bs panel.
TODO: Add tests for c2dbml.

"""
from __future__ import annotations
import argparse
import os
from pathlib import Path
import json
import shutil
from collections import defaultdict
# from camdweb.c2db.convex_hull import update_chull_data # FIGURE THIS ONE OUT

from camdweb import ColVal
import rich.progress as progress
from ase.formula import Formula
from camdweb.c2db.asr_panel import read_result_file
from camdweb.utils import process_pool, read_atoms
from camdweb.c2db.copy import all_dirs, thickness, atoms_to_uid_name
from camdweb.atomsdata import AtomsData


ROOT = Path('/home/scratch3/pmely/ML_C2DB')

PATTERNS = [
    'cdvae/tree/A*/*/*/',
    'ldp/tree/A*/*/*/',]

RESULT_FILES = [
    'structureinfo',
    'stiffness_ml',
    'phonons_ml']


def copy_materials(root: Path,
                   patterns: list[str],
                   processes: int = 1) -> None:
    dirs = all_dirs(root, patterns)
    print(len(dirs), 'folders')

    names: defaultdict[str, int] = defaultdict(int)
    work = []
    with progress.Progress() as pb:
        pid = pb.add_task('Finding UIDs:', total=len(dirs))
        for dir in dirs:
            try:
                uid = json.loads((dir / 'uid.json').read_text())['uid']
            except FileNotFoundError:
                try:
                    name = atoms_to_uid_name(
                        read_atoms(dir / 'structure_ml.json'))
                except FileNotFoundError:
                    pb.advance(pid)
                    continue
                names[name] += 1
                number = names[name]
                uid = f'{name}-{number}t'
            name, tag = uid.split('-')
            stoichiometry, _, nunits = Formula(name).stoichiometry()
            folder = Path(f'materials/{stoichiometry}/{name}/{tag}')
            work.append((dir, folder, uid))
            pb.advance(pid)

    parent_folders = set()
    for _, folder, _ in work:
        parent_folders.add(folder.parent)
    for folder in parent_folders:
        folder.mkdir(exist_ok=True, parents=True)

    with process_pool(processes) as pool:
        with progress.Progress() as pb:
            pid = pb.add_task('Copying materials:', total=len(work))
            for _ in pool.imap_unordered(worker, work):
                pb.advance(pid)

    # Put logo in the right place:
    logo = Path('c2dbml-logo.png')
    if not logo.is_file():
        shutil.copyfile(Path(__file__).parent / 'logo.png', logo)

    print('DONE')


def worker(args):  # pragma: no cover
    """Used by Pool."""
    copy_material(*args)


def copy_material(fro: Path, to: Path, uid: str) -> None:
    structure_file = fro / 'structure_ml.json'
    if not structure_file.is_file():
        return
    atoms = read_atoms(structure_file)
    to.mkdir(exist_ok=True)
    data: dict[str, ColVal | None] = {'folder': str(fro)}
    try:
        data.update(json.loads((fro / 'uid.json').read_text()))
    except FileNotFoundError:
        data['uid'] = uid
    else:
        assert data['uid'] == uid

    def rrf(name: str) -> dict:
        return read_result_file(fro / f'results-asr.{name}.json')

    structure = rrf('structureinfo')
    data['international'] = structure['spacegroup']
    data['number'] = structure['spgnum']
    data['pointgroup'] = str(structure['pointgroup'])
    data['has_inversion_symmetry'] = structure['has_inversion_symmetry']
    lgnum = structure.get('lgnum')
    if lgnum is not None:
        data['lgnum'] = lgnum
        data['layergroup'] = structure['layergroup']
    else:
        data['layergroup'] = '?'
        data['lgnum'] = -1
    data['thickness'] = thickness(atoms)

    for name in RESULT_FILES:
        result = fro / f'results-asr.{name}.json'
        target = to / result.name
        if result.is_file() and not target.is_file():
            shutil.copyfile(result, target)

    try:
        ph = rrf('phonons_ml')
    except FileNotFoundError:
        dyn_stab_phonons = 'unknown'
    else:
        data['minhessianeig'] = ph['minhessianeig']
        dyn_stab_phonons = ph['dynamic_stability_phonons']

    try:
        ph = rrf('stiffness_ml')
    except FileNotFoundError:
        dyn_stab_stiffness = 'unknown'
    else:  # pragma: no cover
        dyn_stab_stiffness = ph['dynamic_stability_stiffness']

    if dyn_stab_phonons == 'unknown' or dyn_stab_stiffness == 'unknown':
        data['dyn_stab'] = 'Unknown'
    else:
        data['dyn_stab'] = 'Yes' if (dyn_stab_phonons == 'high' and
                                     dyn_stab_stiffness == 'high') else 'No'

    pred_json = fro / 'predicted_data.json'
    if pred_json.is_file():
        with pred_json.open('r') as f:
            pred_data = json.load(f)
        data['gap'] = pred_data.get('predicted_gap')
        pred_magmom = pred_data.get('predicted_magmom')
        if isinstance(pred_magmom, list):
            pred_magmom = pred_magmom[0]
        data['magmom'] = pred_magmom

    # TODO: Get these predictions from the classifier model
        if pred_magmom == 0:
            data['is_magnetic'] = False
        else:
            data['is_magnetic'] = True

    AtomsData.from_atoms(atoms).write_json(to / 'structure.json')
    path = to / 'data.json'

    data = {key: value for key, value in data.items() if value is not None}
    path.write_text(json.dumps(data, indent=1))


if __name__ == '__main__':
    processes = min(8, os.cpu_count() or 8)     # Mypy
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--processes', type=int, default=processes)
    parser.add_argument('root', nargs='?', default=ROOT)
    parser.add_argument('patterns', nargs='*', default=PATTERNS)
    args = parser.parse_args()

    copy_materials(Path(args.root), args.patterns, args.processes)
