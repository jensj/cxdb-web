from pathlib import Path
from ase.io import read
from ase.db import connect

ROOT = Path('/home/niflheim/akhound/9_2DADB/')
# ROOT = Path('/home/jensj/mount/niflheim/akhound/9_2DADB/')


def create_database():
    uids = set()
    db = connect('2dalloys.db')
    for path in ROOT.glob('*/tree/*/*/*/'):
        dims, _, _, phase = path.parts[-5:-1]
        traj = path / 'relax.traj'
        if not traj.is_file():
            print('No traj-file:', path)
            continue
        try:
            atoms = read(traj)
        except StopIteration:
            print(path)
            continue

        f = atoms.symbols.formula
        stoi, reduced, nunits = f.stoichiometry()
        nalloys = {'Unary': 1,
                   'Binary': 2,
                   'Ternary': 3,
                   'Quaternary': 4,
                   'Pentanary': 5}[dims]

        # Fix names:
        phase = {'1T': 'T1', '2H': 'H1'}.get(phase, phase)

        n = int(phase[1])
        prototype = {'T': 'CdI2', 'H': 'MoS2'}[phase[0]]

        uid = f'{nunits}{reduced}-{nalloys}-{prototype}-{n}'
        assert uid not in uids
        uids.add(uid)
        db.write(atoms=atoms, uid=uid, nalloys=nalloys, prototype=prototype)


if __name__ == '__main__':
    create_database()
