"""CRYSP web-app."""
from __future__ import annotations

import argparse
import json
from pathlib import Path

import rich.progress as progress

from camdweb.panels.panel import Panel
from camdweb.materials import Material, Materials
from camdweb.crysp.panels import (RefractiveIndexPanel, CryspAtomsPanel,
                                  CryspStructure, DOSPanel)
from camdweb.html import Range, SearchFilter, image, stoichiometry_input
from camdweb.utils import doi, icsd
from camdweb.web import CAMDApp


def main(argv: list[str] | None = None) -> CAMDApp:
    """Create CRYSP app."""
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'pattern', nargs='*', default=['*/*/*/'],
        help='Glob-pattern: materials/<pattern>.  Default is "*/*/*/".')
    args = parser.parse_args(argv)

    root = Path()
    folders = []
    for pattern in args.pattern:
        for p in (root / 'materials').glob(pattern):
            folders.append(p)

    mlist: list[Material] = []
    with progress.Progress() as pb:
        pid = pb.add_task('Reading materials:', total=len(folders))
        for f in folders:
            uid = f'{f.parent.name}-{f.name}'
            data = json.loads((f / 'data.json').read_text())
            material = Material.from_json(f / 'structure.json', uid)
            material.columns.update(data)
            material.default_repeat_unitcell = 2
            mlist.append(material)
            pb.advance(pid)

    panels: list[Panel] = [
        CryspAtomsPanel(),
        CryspStructure(),
        DOSPanel(),
        RefractiveIndexPanel()]

    materials = Materials(mlist, panels)
    materials.column_descriptions.update(
        label='Structure origin',
        uid='Unique ID',
        bravais_type='Bravais type',
        international='Space group (intl.)',
        number='Space group number',
        pointgroup='Point group',
        has_inversion_symmetry='Inversion symmetry',
        gap='Band gap [eV]',
        gap_dir='Direct band gap [eV]',
        is_magnetic='Magnetic',
        magmom='Total magnetic moment [μ<sub>B</sub>]',
        energy='Energy [eV]',
        ehull='Energy above hull [eV/atom]',
        hform='Heat of formation [eV/atom]',
        n_avg='Average static refractive index',
    )
    materials.html_formatters.update(icsd_id=icsd, doi=doi)

    initial_columns = ['formula', 'gap', 'is_magnetic', 'n_avg']
    app = CAMDApp(materials, initial_columns, root=root)
    app.title = 'CRYSP'
    app.logo = image('crysp-logo.png', alt='CRYSP-logo')
    app.links = [
        ('CMR', 'https://cmr.fysik.dtu.dk'),
        ('About', 'https://cmr.fysik.dtu.dk/crysp/crysp.html')]
    app.form_parts[0] = SearchFilter(placeholder="Example: 'NaCl'")
    app.form_parts += [
        stoichiometry_input(materials.stoichiometries()),
        Range('Average static refractive index', 'n_avg', nonnegative=True),
        Range('Band gap range [eV]:', 'gap', nonnegative=True)]

    return app


def create_app():  # pragma: no cover
    return main().app


if __name__ == '__main__':
    app = main()
    app.app.run(host='0.0.0.0', port=8086, debug=True)
