"""Copy old CRYSP files from ASR-layout to uniform tree structure.

Tree structure::

   materials/<stoichiometry>/<spacegroup>/<combined-id>/
"""
from __future__ import annotations

import argparse
import json
import shutil
from collections import defaultdict
from pathlib import Path

import numpy as np
import rich.progress as progress
from numpy import sqrt

from ase.formula import Formula

from camdweb import ColVal
from camdweb.c2db.asr_panel import read_result_file
from camdweb.c2db.copy import atoms_to_uid_name
from camdweb.c2db.copy import standard_gs
from camdweb.utils import all_dirs, process_pool, read_atoms
from camdweb.atomsdata import AtomsData

ROOT = Path('/home/niflheim2/markas/All-dielectric-nano-photonics/oqmd_trees/')
RESULT_FILES = [
    'structureinfo',
    'gs',
    'gs@calculate',
    'bandstructure',
    'pdos',
]
PATTERNS = [
    'tree-*/A*/*/*/',
]


def worker(args):  # pragma: no cover
    """Used by Pool."""
    copy_material(*args)


def copy_materials(root: Path,
                   patterns: list[str],
                   processes: int = 1,
                   write_new_uid_files: bool = True) -> None:
    dirs = all_dirs(root, patterns)
    print('Folders:', len(dirs))

    things: list[tuple[Path, str, str | None]] = []
    known = 0
    with progress.Progress() as pb:
        pid = pb.add_task('Finding UIDs:', total=len(dirs))
        for dir in dirs:
            try:
                uid = json.loads((dir / 'uid.json').read_text())['uid']
            except FileNotFoundError:
                try:
                    name = atoms_to_uid_name(
                        read_atoms(dir / 'structure.json'))
                except FileNotFoundError:
                    pass
                else:
                    things.append((dir, name, None))
            else:
                name, tag = uid.split('-')
                things.append((dir, name, uid))
                known += 1
            pb.advance(pid)

    # Find largest tag numbers for each formula:
    maxtags: defaultdict[str, int] = defaultdict(int)
    for dir, name, uid in things:
        if uid is not None:
            name, tag = uid.split('-')
            maxtags[name] = max(maxtags[name], int(tag))

    work = []
    for dir, name, uid in things:
        if uid is None:
            maxtags[name] += 1
            uid = f'{name}-{maxtags[name]}'
            if write_new_uid_files:
                (dir / 'uid.json').write_text(
                    json.dumps({'uid': uid}, indent=2))
            else:
                uid += 'temp'

        name, tag = uid.split('-')
        stoichiometry, _, nunits = Formula(name).stoichiometry()
        folder = Path(f'materials/{stoichiometry}/{name}/{tag}')
        work.append((dir, folder, uid))

    parent_folders = set()
    for _, folder, _ in work:
        parent_folders.add(folder.parent)

    with process_pool(processes) as pool:
        with progress.Progress() as pb:
            pid = pb.add_task('Copying materials:', total=len(work))
            for _ in pool.imap_unordered(worker, work):
                pb.advance(pid)

    # Put logo in the right place:
    logo = Path('crysp-logo.png')
    if not logo.is_file():
        shutil.copyfile(Path(__file__).parent / 'logo.png', logo)

    # copy the webpage validation workflow script
    shutil.copyfile(Path(__file__).parent / 'wf.py', Path('wf.py'))

    print('DONE')


def copy_material(fro: Path,
                  to: Path,
                  uid: str) -> None:  # pragma: no cover
    # quit if either of these files is missing
    polarizability_file = fro / 'results-asr.polarizability.json'
    structure_file = fro / 'structure.json'
    if not structure_file.is_file() or not polarizability_file.is_file():
        return
    to.mkdir(exist_ok=True, parents=True)
    atoms = read_atoms(structure_file)
    AtomsData.from_atoms(atoms).write_json(to / 'structure.json')

    def rrf(name: str) -> dict:
        return read_result_file(fro / f'results-asr.{name}.json')

    # None values removed later
    data: dict[str, ColVal | None] = {'folder': str(fro)}

    # compute n (refractive index) and k (extinction coefficient)
    pdata = polarizability_to_n_k_eps(rrf('polarizability'))
    data['n_avg'] = pdata['n_avg']
    (to / 'refractive-index-data.json').write_text(json.dumps(pdata, indent=0))

    try:
        data.update(json.loads((fro / 'uid.json').read_text()))
    except FileNotFoundError:
        data['uid'] = uid
    else:
        assert data['uid'] == uid

    structure = rrf('structureinfo')
    data['international'] = structure['spacegroup']
    data['number'] = structure['spgnum']
    data['pointgroup'] = str(structure.get('pointgroup', ''))
    data['has_inversion_symmetry'] = structure['has_inversion_symmetry']
    try:
        data['bravais_type'] = json.loads(
            (fro / 'bravais_type.json').read_text())['bravais_type']
    except FileNotFoundError:
        pass

    try:
        gs = rrf('gs')
    except FileNotFoundError:
        pass
    else:
        data['gap_dir_nosoc'] = gs['gap_dir_nosoc']
        data['efermi'] = gs['efermi']
        data.update(standard_gs(gs=gs, vacuumlevel=0.0))
    data['energy'] = atoms.get_potential_energy()

    try:
        magstate = rrf('magstate')
    except FileNotFoundError:
        pass
    else:
        data['is_magnetic'] = magstate['is_magnetic']
        data['magmom'] = magstate.get('magmom', 0.0)

    # info.json should be written out to old data so it has a label
    try:
        info = json.loads((fro / 'info.json').read_text())
    except FileNotFoundError:
        pass
    else:
        for key in ['icsd_id', 'cod_id', 'doi']:
            if key in info:
                data[key] = info[key]
        data['label'] = 'oqmd123'

    try:
        chull = rrf('convex_hull')
    except FileNotFoundError:
        pass
    else:  # pragma: no cover
        data['ehull'] = chull['ehull']
        data['hform'] = chull['hform']

    # Copy result json-files:
    for name in RESULT_FILES:
        result = fro / f'results-asr.{name}.json'
        target = to / result.name
        gzipped = target.with_suffix('.json.gz')
        if result.is_file() and not (target.is_file() or gzipped.is_file()):
            shutil.copyfile(result, target)

    # Remove None values
    data = {key: value for key, value in data.items() if value is not None}
    (to / 'data.json').write_text(json.dumps(data, indent=0))


def polarizability_to_n_k_eps(alpha, frequency_cutoff: float = 50.0) -> dict:
    """Convert the dynamic polarizability to dielectric function, then to the
    refractive index (n) and extinction coefficient (k)."""
    frequencies = alpha['frequencies']
    i2 = abs(frequencies - frequency_cutoff).argmin()
    data, n = {}, []  # n - refractive index
    for i in 'xyz':
        # convert dynamic polarizability to epsilon (dielectric function)
        # Valid only for 3D systems
        epsilon = 1 + 4 * np.pi * alpha[f'alpha{i}_w'][:i2]
        eps_i, eps_r = epsilon.imag, epsilon.real
        data[f'eps_r_{i * 2}'] = list(epsilon.real)
        data[f'eps_i_{i * 2}'] = list(epsilon.imag)
        data[f'n_{i * 2}'] = list(
            sqrt(0.5 * (sqrt(eps_r**2 + eps_i**2) + eps_r)))
        data[f'k_{i * 2}'] = list(
            sqrt(0.5 * (sqrt(eps_r**2 + eps_i**2) - eps_r)))
        n.append(data[f'n_{i * 2}'])
    data['energy'] = list(frequencies[:i2])

    # compute the average static index of refraction @ E=0 (omega=0)
    assert np.abs(frequencies[0]) < 0.001
    data['n_avg'] = np.mean(np.array(n), 0)[0]
    return data


def main(argv: list[str] | None = None):
    parser = argparse.ArgumentParser()
    parser.add_argument('root', help='Root of ASR-tree to copy from.'
                        'Use "DEFAULT" to collect standard data.')
    patterns = ', '.join(f'"{p}"' for p in PATTERNS)
    parser.add_argument('pattern', nargs='+', help='Glob pattern like '
                        '"tree/A*/*/*/". Use ALL to set the standard patterns:'
                        f' {patterns}.\nUse TEST to use testing patterns.')
    parser.add_argument('-p', '--processes', type=int, default=1)
    args = parser.parse_args(argv)
    if args.root == 'DEFAULT':  # pragma: no cover
        args.root = ROOT
    if any(p in ['ALL', 'TEST'] for p in args.pattern):  # pragma: no cover
        args.pattern = PATTERNS if args.pattern == ['ALL'] else ['A*/*/*/']
    copy_materials(Path(args.root), args.pattern, args.processes)


if __name__ == '__main__':
    main()
