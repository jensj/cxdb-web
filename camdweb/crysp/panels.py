from __future__ import annotations

import json

from camdweb.html import table, downloadable
from camdweb.material import Material
from camdweb.c2db.bs_dos_bz_panel import BSDOSBZPanel
from camdweb.panels.atoms import AtomsPanel
from camdweb.panels.panel import Panel, PanelData, SkipPanel
from camdweb.panels.structureinfo import CrystalStructurePanel
from camdweb.figures import CAMDFigure, PLOTLY_SCRIPT as SCRIPT

HTML = """
<div class="row">
  <div class="col">
    <label>Download:</label>
    {json}
    {csv}
  </div>
</div>
<div class="row row-cols-2">
  <div class="col">
    <div id='refractive'></div>
  </div>
  <div class="col">
    <div id='extinction'></div>
  </div>
    <div class="col">
    <div id='epsilon_r'></div>
  </div>
  <div class="col">
    <div id='epsilon_i'></div>
  </div>
</div>
"""


class CryspAtomsPanel(AtomsPanel):
    info = """
    Crystal structure and unit cell dimensions.  Links to the parent
    experimental databases (OQMD) are provided if available.
    """

    def create_column_one(self,
                          material: Material) -> str:
        html1 = table(['Structure info', ''],
                      self.table_rows(material,
                                      ['international', 'number', 'label',
                                       'cod_id', 'icsd_id', 'doi']),
                      col_spacing='')
        html2 = table(['Basic properties', ''],
                      self.table_rows(material,
                                      ['is_magnetic', 'gap', 'magmom']))
        return '\n'.join([html1, html2])


class CryspStructure(CrystalStructurePanel):
    info = """The space group of the bulk structure. The point group and the
    Bravais lattice type are also listed."""


class DOSPanel(BSDOSBZPanel):
    title = 'Band structure and DOS'
    info = """
    The single-particle band structure calculated with the PBE xc-functional
    including D3 corrections.

    <br><br>Relevant articles:

    <br>Haastrup2018
    """


class RefractiveIndexPanel(Panel):
    title = 'Refractive indices'
    info = 'o/'
    datafiles = ['refractive-index-data.json']

    def get_data(self, material):
        file = material.folder / self.datafiles[0]
        if not file.is_file() or material.gap <= 0.0:
            raise SkipPanel
        self.replace_shortname_ref('Haastrup2018')
        data = json.loads((material.folder / self.datafiles[0]).read_text())

        def Fig(xtitle, ytitle, plots, data):
            fig = CAMDFigure()
            fig.yaxis.title = ytitle
            fig.xaxis.title = xtitle
            fig.xaxis.ranges = [0, material.gap + 10]
            for xkey, ykey, label in plots:
                fig.line_plot(data[xkey], data[ykey], label)
            fig.plot_reference_line(value=material.gap, name='E<sub>gap</sub>')
            return fig

        elems = ['xx', 'yy', 'zz']
        eps_r = Fig('Energy [eV]', '&#949;<sub>real</sub>',
                    [('energy', f'eps_r_{cc}',
                     f'&#949;<sub>{cc}</sub>') for cc in elems],
                    data)
        eps_i = Fig('Energy [eV]', '&#949;<sub>imag</sub>',
                    [('energy', f'eps_i_{cc}',
                     f'&#949;<sub>{cc}</sub>') for cc in elems],
                    data)
        n = Fig('Energy [eV]', 'Refractive index, n',
                [('energy', f'n_{cc}',
                 f'n<sub>{cc}</sub>') for cc in elems],
                data)
        k = Fig('Energy [eV]', 'Extinction coefficient, &#954;',
                [('energy', f'k_{cc}',
                 f'&#954;<sub>{cc}</sub>') for cc in elems],
                data)

        d_json = downloadable(path=material.folder, file=self.datafiles[0])
        d_csv = downloadable(path=material.folder,
                             file='refractive-index-data.csv')
        return PanelData(
            title=self.title,
            info=self.info,
            html=HTML.format(json=d_json, csv=d_csv),
            script=(SCRIPT.format(data=n.render(), id='refractive') +
                    SCRIPT.format(data=k.render(), id='extinction') +
                    SCRIPT.format(data=eps_i.render(), id='epsilon_i') +
                    SCRIPT.format(data=eps_r.render(), id='epsilon_r')))
