"""CrystalBank web-app.

This module has code to convert ~crystalbank*/tree/ folders and friends
(see PATTERNS variable below) to canonical tree layout.

To generate the materials folder structures from a taskblaster tree execute:
::
    python -m camdweb.crystalbank.copy <DATA> <PATTERNS>
with defaults for DATA=TEST and PATTERS=ALL or a sequence of strings.

To generate the convex hull information for the materials run:
::
    python -m camdweb.crystalbank.convex_hull materials/A*/*/*/

Once all data is there you can start the server:
::
    python -m camdweb.crystalbank.app
"""
from __future__ import annotations

import argparse
import json
from pathlib import Path

import rich.progress as progress
from camdweb.crystalbank.overview import CrystalBankAtomsPanel
from camdweb.html import stoichiometry_input, SearchFilter, image
from camdweb.materials import Material, Materials
from camdweb.panels.convex_hull import ConvexHullPanel
from camdweb.panels.panel import Panel
from camdweb.panels.structureinfo import CrystalStructurePanel
from camdweb.web import CAMDApp
from camdweb.utils import oqmd

CONVEX_HULL_INFO = """The heat of formation (ΔH) is defined as the internal
energy of a compound relative to the standard states of the constituent
elements at T=0 K. The energy above the convex hull is the internal energy
relative to the most stable (possibly mixed) phase of the constituent
elements at T=0 K.  All energies are calculated with the
PBE xc-functional.
"""


class CrystalBankStructure(CrystalStructurePanel):
    info = """The space group of the bulk structure. The point group and the
    Bravais lattice type are also listed."""


def main(argv: list[str] | None = None) -> CAMDApp:
    """Create CrystalBank app."""
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'pattern', nargs='*', default=['*/*/*/'],
        help='Glob-pattern: materials/<pattern>.  Default is "*/*/*/".')
    args = parser.parse_args(argv)

    root = Path()
    folders = []
    for pattern in args.pattern:
        for p in (root / 'materials').glob(pattern):
            folders.append(p)

    mlist: list[Material] = []
    with progress.Progress() as pb:
        pid = pb.add_task('Reading materials:', total=len(folders))
        for folder in folders:
            uid = f'{folder.parent.name}-{folder.name}'
            data = json.loads((folder / 'data.json').read_text())
            material = Material.from_json(folder / 'structure.json', uid)
            data['volume_per_atom'] = material.volume / material.natoms
            material.columns.update(data)
            material.default_repeat_unitcell = 1
            mlist.append(material)
            pb.advance(pid)

    # overwrite the default structure info context
    panels: list[Panel] = [
        CrystalBankAtomsPanel(),
        ConvexHullPanel(
            sources={'OQMD': ('Bulk crystals',
                              '{formula:html} (<a href={uid}>{uid}</a>)')},
            info=CONVEX_HULL_INFO,
            root=root),
        CrystalBankStructure()]

    mlist.sort(key=lambda material: material.formula)
    materials = Materials(mlist, panels)
    materials.column_descriptions.update(
        crystal_type='Crystal reference',
        crystal_system='Crystal system',
        bravais_type='Bravais type',
        international='Space group (intl.)',
        number='Space group number',
        pointgroup='Point group',
        has_inversion_symmetry='Inversion symmetry',
        energy='Total energy [eV]',
        hform='Heat of formation [eV/atom]',
        ehull='Energy above hull [eV/atom]',
        volume_per_atom='Volume per atom [Å³]',
        magstate='Magnetic state',
        fmax='Maximum force [eV/Å]',
        smax='Maximum stress [eV/Å³]',
        gap='Band gap (PBE) [eV]',
        gap_dir='Direct band gap (PBE) [eV]',
        oqmd='OQMD ref. ID')
    materials.html_formatters['oqmd'] = oqmd

    app = CAMDApp(
        materials,
        initial_columns=['formula', 'gap', 'magstate', 'hform', 'ehull',
                         'volume_per_atom'],
        root=root)

    app.logo = image('crystalbank-logo.png', alt='CrystalBank-logo')
    app.title = 'CrystalBank'
    app.links = [
        ('CMR', 'https://cmr.fysik.dtu.dk')]
    app.form_parts[0] = SearchFilter(placeholder="Example: 'NaCl'")
    app.form_parts += [
        stoichiometry_input(materials.stoichiometries())]

    return app


def create_app():
    app = main(['*/*/*/'])
    return app.app


if __name__ == '__main__':
    app = main()
    app.app.run(host='0.0.0.0', port=8086, debug=True)
