import argparse
import json
from collections import defaultdict
from math import inf, isfinite
from pathlib import Path

import rich.progress as progress
from ase.formula import Formula
from camdweb.panels.convex_hull import (calculate_ehull_energies,
                                        group_references)
from camdweb.utils import process_pool


def update_chull_data(folders: list[Path],
                      processes: int = 1) -> None:
    """Update ehull and hform values.

    Will calculate ehull and hform energies from data files like::

      AB2/1MoS2/23525/data.json

    Also writes json files containing all
    reference data for each convex-hull plot::

      convex_hulls/MoS.json
                  /Mo.json
                  /S.json
                  /...
    """
    paths: list[Path] = []
    for folder in folders:
        paths += folder.glob('**/data.json')
    print('Materials:', len(paths))

    atomic_energies: defaultdict[str, float] = defaultdict(lambda: inf)
    refs0 = {}
    for path in paths:
        data = json.loads(path.read_text())
        if 'energy' not in data:
            continue
        formula, oqmdid = path.parts[-3:-1]
        uid = f'{formula}-{oqmdid}'
        f = Formula(formula)
        count = f.count()
        energy = data['energy']
        refs0[uid] = (count, energy)
        if len(count) == 1:
            symbol, n = next(iter(count.items()))
            atomic_energies[symbol] = min(atomic_energies[symbol], energy / n)
    print('Materials without energy:', len(paths) - len(refs0))

    print('Calculating heat of formations:', end=' ')
    refs = {}
    for uid, (count, energy) in refs0.items():
        energy -= sum(n * atomic_energies[symbol]
                      for symbol, n in count.items())
        if isfinite(energy):
            refs[uid] = (count, energy)
    print(len(refs))

    # Find all convex-hulls:
    # Create map from uid to sorted tuple of symbols:
    groups = group_references({uid: tuple(sorted(count))
                               for uid, (count, _) in refs.items()})

    print('Writing convex-hull files:', len(groups))
    folder = Path('convex-hulls')
    folder.mkdir(exist_ok=True)
    for symbols, uids in groups.items():
        data = {}
        for uid in uids:
            source = 'OQMD'
            data[uid] = refs[uid] + (source,)
        (folder / (''.join(symbols) + '.json')).write_text(
            json.dumps(data, indent=2))

    # Calculate energy above hull:
    ehull_energies = {}
    with process_pool(processes) as pool:
        with progress.Progress(*progress.Progress.get_default_columns(),
                               progress.TimeElapsedColumn()) as pb:
            pid = pb.add_task('Calculating ehull:', total=len(groups))
            work = ({uid: refs[uid] for uid in uids}
                    for symbols, uids in groups.items())
            for energies in pool.imap_unordered(calculate_ehull_energies,
                                                work,
                                                chunksize=30):
                ehull_energies.update(energies)
                pb.advance(pid)

    print('Updating data.json files')
    for path in paths:
        formula, oqmdid = path.parts[-3:-1]
        uid = f'{formula}-{oqmdid}'
        if uid not in refs:
            continue
        count, energy = refs[uid]
        natoms = sum(count.values())
        data = json.loads(path.read_text())
        data['ehull'] = ehull_energies[uid] / natoms
        data['hform'] = energy / natoms
        path.write_text(json.dumps(data, indent=2))


def main(argv: list[str] | None = None):
    parser = argparse.ArgumentParser()
    parser.add_argument('folder', nargs='+')
    parser.add_argument('-p', '--processes', type=int, default=1)
    args = parser.parse_args(argv)
    update_chull_data([Path(folder) for folder in args.folder],
                      args.processes)


if __name__ == '__main__':
    main()
