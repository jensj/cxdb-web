"""CrystalBank web-app.

This module has code to convert ~oqmd*/tree/ folders and friends
(see PATTERNS variable below) to canonical tree layout.

Also contains a simple web-app that can run off the tree of folders.

python -m camdweb.crystalbank.copy
"""
from __future__ import annotations

import argparse
import json
import shutil
from pathlib import Path
from typing import TYPE_CHECKING
from camdweb.atomsdata import AtomsData

if TYPE_CHECKING:
    from taskblaster.repository import Repository
    from taskblaster.records import Record

ROOT = Path('/home/scratch3/workflows/crysp/')
PATTERNS = [
    'relax_nm', 'relax3d_magnetic', 'relaxresults_nm', 'results', 'finalizer',
    'gsresults', 'postprocess', 'gs', 'pbe_u4_gs', 'pbe_u6_gs', 'pbe_u8_gs',
    'pbe_d3_u4_gs', 'pbe_d3_u6_gs', 'pbe_d3_u8_gs'
]


def copy_materials(root: Path = ROOT, patterns: list = PATTERNS) -> None:
    from taskblaster.records import get_record_from_repo
    from taskblaster.repository import Repository
    from taskblaster.state import State

    # glob over root to find patterns
    with Repository.find(root) as repo:
        for node in repo.tree([root]).nodes():
            # collect only done calculations
            if node.state is not State.done:
                continue
            # continue only if the task name is in the patterns
            if node.name.split('/')[-1] not in patterns:
                continue

            # get the task's record
            try:
                record = get_record_from_repo(node.name, repo)
            except FileNotFoundError:
                continue
            print('copy mats:', record.name)
            copy_material(record, repo, root)

    # Put logo in the right place
    logo = Path('crystalbank-logo.png')
    if not logo.is_file():
        shutil.copyfile(Path(__file__).parent / 'logo.png', logo)

    shutil.copyfile(Path(__file__).parent / 'wf.py', Path('wf.py'))
    print('DONE')


def copy_material(record: Record, repo: Repository,
                  relative_to: Path = Path('.')) -> None:
    """
    This is the function we use to convert TaskBlaster trees into an easy
    webpage-displayable layout. From dir path, read the output/input.json.
    """
    def get_folder_from_record(record):
        from ase.formula import Formula
        symbols, oqmd_id = record.name.split('/')[:2]
        ab, xy, n = Formula(symbols).stoichiometry()
        return Path(f'materials/{ab}/{n}{xy}', oqmd_id)

    # get data from taskblaster action
    try:
        results = repo.view(
            tree=['tree/' + record.name],
            action='todb',
            relative_to=Path(relative_to))
    except ValueError:
        return
    except FileNotFoundError:
        return

    # for each result update data.json file on the disk
    for data, write_structure in results:
        folder = get_folder_from_record(record=record)
        folder.mkdir(exist_ok=True, parents=True)

        # copy relevant data to folder
        if write_structure:
            atom_keys = [key for key in data if 'structure' in key]
            for key in atom_keys:
                atoms = data.pop(key)
                AtomsData.from_atoms(atoms).write_json(folder / f'{key}.json')

        data, listdata = update_json_data(folder=folder, update=data)
        (folder / 'magmoms.json').write_text(json.dumps(listdata, indent=0))
        (folder / 'data.json').write_text(json.dumps(data, indent=2))


def update_json_data(folder: Path, update: dict):
    # get the data in the folder if it exists
    existing_data = {}
    if (folder / 'data.json').is_file():
        existing_data.update(json.loads((folder / 'data.json').read_text()))
    if (folder / 'magmoms.json').is_file():
        existing_data.update(json.loads((folder / 'magmoms.json').read_text()))
    update.update(existing_data)

    listdata = {k: v for k, v in update.items() if isinstance(v, list)}
    data = {k: v for k, v in update.items() if not isinstance(v, list)}
    return data, listdata


def main(argv: list[str] | None = None):
    parser = argparse.ArgumentParser()
    parser.add_argument('root', help='Path to the taskblaster tree.')
    patterns = ', '.join(f'"{p}"' for p in PATTERNS)
    parser.add_argument(
        'pattern', nargs='+',
        help='Pattern matching the task name such as "relax3d_magnetic". '
        f'Use "ALL" to get all the standard patterns: {patterns}.')
    args = parser.parse_args(argv)
    if args.root == 'DEFAULT':  # pragma: no cover
        args.root = ROOT
    elif args.root == 'TEST':  # pragma: no cover
        args.root = Path('/home/tara/webpage/CAMD-Web-test-data/CRYSTALBANK/')
    if args.pattern == ['ALL']:  # pragma: no cover
        args.pattern = PATTERNS
    copy_materials(args.root, args.pattern)


if __name__ == '__main__':
    main()
