from __future__ import annotations

from camdweb.html import table
from camdweb.material import Material
from camdweb.panels.atoms import AtomsPanel


class CrystalBankAtomsPanel(AtomsPanel):
    info = """
    Crystal structure and unit cell dimensions.  Links to the parent
    experimental databases (OQMD) are provided if available.
    """

    def create_column_one(self, material: Material) -> str:
        return table(
            None,
            self.table_rows(material,
                            ['gap', 'gap_dir', 'magstate', 'fmax',
                             'smax', 'oqmd']))
