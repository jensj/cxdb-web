from __future__ import annotations

import json
from dataclasses import field, dataclass
from pathlib import Path
from abc import abstractmethod

import plotly
import plotly.graph_objs as go

PLOTLY_SCRIPT = """
<script type="text/javascript">
window.addEventListener("resize", function() {{
  Plotly.Plots.resize(document.getElementById('{id}'));
}});
</script>

<script type='text/javascript'>
var graphs = {data};
Plotly.newPlot('{id}', graphs, {{}});
</script>
"""


def default_kwargs():
    return dict(showgrid=True, showline=True, linewidth=2,
                gridcolor='lightgrey', linecolor='black')


@dataclass
class Axis:
    coord: str
    title: str
    ranges: list | None
    kwargs: dict = field(default_factory=default_kwargs)

    @staticmethod
    def _defaultX():
        return Axis('x', '', None,
                    kwargs=dict(**default_kwargs(), ticks='',
                                showticklabels=True,
                                mirror=True))

    @staticmethod
    def _defaultY():
        return Axis('y', '', None, kwargs=dict(
                    **default_kwargs(),
                    zeroline=False,
                    mirror='ticks',
                    ticks='inside',
                    tickwidth=2,
                    zerolinewidth=2))

    def __post_init__(self):
        assert self.coord in {'x', 'y'}

    @property
    def as_plotly(self):
        cls = go.layout.XAxis if self.coord == 'x' else go.layout.YAxis
        return cls(**self._dct)

    @property
    def _dct(self):
        return dict(title=self.title,
                    range=self.ranges,
                    **self.kwargs)


class BasePlot:
    def __init__(self, x, y, color=None, label=None):
        self.xdata = x
        self.ydata = y
        self.label = label
        self.color = color

    @abstractmethod
    def as_plotly(self):
        pass


class LinePlot(BasePlot):
    @property
    def as_plotly(self):
        return go.Scattergl(
            x=self.xdata, y=self.ydata, name=self.label, mode='lines',
            line={'color': self.color} if self.color else self.color,
            showlegend=True)


class ScatterPlot(BasePlot):
    def __init__(self, x, y, label=None, color=None, size=None,
                 hovertemplate=None, customdata=None, marker=None):
        super().__init__(x, y, label)
        self.color = color
        self.size = size
        self.hovertemplate = hovertemplate
        self.customdata = customdata
        self.marker = marker

    @property
    def as_plotly(self):
        return go.Scattergl(x=self.xdata,
                            y=self.ydata,
                            name=self.label,
                            marker=dict(color=self.color,
                                        size=self.size,
                                        symbol=self.marker),
                            mode='markers',
                            hovertemplate=self.hovertemplate,
                            customdata=self.customdata,
                            showlegend=False)


class ReferenceLine:
    def __init__(self, value, name, vertical, color: str = 'black',
                 width: float = 2.0, line: str = 'dash'):
        self.value = value
        self.name = name
        self.vertical = vertical
        self.color = color
        self.width = width
        self.line = line

    def add_to_figure(self, fig):
        dct = dict(
            type='line', annotation_text=self.name, line=dict(
                color=self.color, width=self.width, dash=self.line))
        if self.vertical:
            fig.add_vline(x=self.value, **dct)
        else:
            fig.add_hline(y=self.value, **dct)


# This doesn't really need to be a dataclass
@dataclass
class CAMDFigure:
    xaxis: Axis = field(default_factory=Axis._defaultX)
    yaxis: Axis = field(default_factory=Axis._defaultY)
    data: list[BasePlot] = field(default_factory=list)
    references: list[ReferenceLine] = field(default_factory=list)
    legend: list[str] | None = None
    default_colors = (
        '#1f77b4', '#ff7f0e', '#2ca02c', '#d62728', '#9467bd', '#8c564b',
        '#e377c2', '#7f7f7f', '#bcbd22', '#17becf')

    def color_by(self, index: int) -> str:
        index = index - len(self.default_colors) \
            if index >= len(self.default_colors) else index
        return self.default_colors[index]

    @property
    def plotly_layout(self):
        return go.Layout(
            xaxis=self.xaxis.as_plotly,
            yaxis=self.yaxis.as_plotly,
            plot_bgcolor='white',
            hovermode='closest',
            margin=dict(t=70, r=20),
            font=dict(size=14),
            legend=dict(
                orientation='h',
                yanchor='bottom',
                y=1.01,
                xanchor='left',
                x=0.0,
                font=dict(size=14),
                itemsizing='constant',
                itemwidth=30)
        )

    def plot_reference_line(self, value: float,
                            name: str, vertical: bool = True):
        self.references.append(ReferenceLine(value, name, vertical))

    def line_plot(self, x, y, label=None):
        color = self.color_by(index=len(self.data))
        self.data.append(LinePlot(x, y, color=color, label=label))

    def scatter_plot(self, x, y, label=None, customdata=None,
                     hovertemplate=None, size: int = 10, marker='circle'):
        color = self.color_by(index=len(self.data))
        self.data.append(ScatterPlot(x, y, label=label,
                         hovertemplate=hovertemplate,
                         customdata=customdata,
                         size=size, color=color,
                         marker=marker))

    def render(self, to: Path | None = None):
        fig = go.Figure(data=[d.as_plotly for d in self.data],
                        layout=self.plotly_layout)
        for reference in self.references:
            reference.add_to_figure(fig)
        return plotly2json(fig, to)


def plotly2json(fig, to: Path | None = None,
                encoder=plotly.utils.PlotlyJSONEncoder) -> str:
    fig_str = json.dumps(fig, cls=encoder)
    if to:
        to.write_text(fig_str)
        return ''
    return fig_str
