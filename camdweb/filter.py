"""Code for efficient filtering of rows.

Example filter string:

* ``Cu>1``: more than 1 Cu atom
* ``gap>1.1``: "gap" larger than 1.1
* ``xc=PBE``: "xc" equal to "PBE"
* ``MoS2``: one or more MoS2 formula units

Strings can be combined with ``,`` (and) and ``|`` (or).  Grouping can be
done using ``(`` and ``)``:

* ``(Cu>1, gap>1.1) | Fe=0``
"""
from collections import defaultdict

import numpy as np
from ase.data import chemical_symbols

from camdweb import ColVal
from camdweb.utils import formula_dict_to_string


class Index:
    def __init__(self,
                 rows: list[tuple[dict[str, int], dict[str, ColVal]]],
                 max_int_range: int = 350,
                 strict=True):
        """Indices for speeding up row filtering.

        The *rows* argument is a list of two element tuples:

        1) Atom count as a dict: {'H': 2, 'O': 1}.

        2) Key-value pairs: {'ok': True, 'gap': 1.1, ...}.

        >>> rows = [({'B': 2, 'N': 2}, {'gap': 3.6}),
        ...         ({'C': 2}, {'gap': 0.0})]
        >>> i = Index(rows)
        Rows: 2
        Strings: 0
        Integers: 3
        Bools: 0
        Floats: 1
        Int-floats: 0
        >>> i.float_key('gap', '>', 0.0)
        {np.int32(0)}
        """

        integers = defaultdict(list)
        floats = defaultdict(list)
        self.bools = defaultdict(set)
        self.bools_true = defaultdict(set)
        self.strings: defaultdict[str, dict[str, set[int]]] = defaultdict(dict)
        self.natoms: dict[int, int] = {}
        self.reduced: defaultdict[str, set[int]] = defaultdict(set)
        self.ids = set()

        for i, (count, keys) in enumerate(rows):
            nunits = np.gcd.reduce(list(count.values()))
            reduced = formula_dict_to_string(
                {symbol: n // nunits for symbol, n in count.items()})
            self.reduced[reduced].add(i)
            self.natoms[i] = sum(count.values())
            self.ids.add(i)
            for symbol, n in count.items():
                integers[symbol].append((n, i))
            for name, value in keys.items():
                if isinstance(value, str):
                    dct = self.strings[name]
                    if value not in dct:
                        dct[value] = {i}
                    else:
                        dct[value].add(i)
                elif isinstance(value, float):
                    floats[name].append((value, i))
                elif isinstance(value, bool):
                    self.bools[name].add(i)
                    if value:
                        self.bools_true[name].add(i)
                else:
                    integers[name].append((int(value), i))

        self.integers = {}
        self.floats = {}
        ni = 0  # number of ints converted to floats
        for name, idata in integers.items():
            idata.sort()
            ids = np.array([i for value, i in idata], dtype=np.int32)
            indices = [0]
            nmin = idata[0][0]
            nmax = idata[-1][0]
            if nmax - nmin > max_int_range:
                # Avoid too wide range of integer-index
                values = np.array([value
                                   for value, i in idata], dtype=np.int32)
                self.floats[name] = (values, ids)
                ni += 1
                continue
            m = nmin
            for j, (n, i) in enumerate(idata):
                if n > m:
                    indices += [j] * (n - m)
                    m = n
            indices.append(j + 1)
            self.integers[name] = (
                nmin,
                nmax,
                np.array(ids, dtype=np.int32),
                np.array(indices, dtype=np.int32))

        for name, fdata in floats.items():
            assert name not in self.integers, name
            fdata.sort()
            ids = np.array([i for value, i in fdata], dtype=np.int32)
            values = np.array([value for value, i in fdata])
            self.floats[name] = (values, ids)

        sets = [set(dct)  # type: ignore[call-overload]
                for dct in [self.strings,
                            self.floats,
                            self.integers,
                            self.bools]]
        if len(set.union(*sets)) != sum(len(s) for s in sets):
            raise ValueError(f'Some keys have multiple types: {sets}')

        print(f'Rows: {len(rows)}\n'
              f'Strings: {len(self.strings)}\n'
              f'Integers: {len(self.integers)}\n'
              f'Bools: {len(self.bools)}\n'
              f'Floats: {len(self.floats) - ni}\n'
              f'Int-floats: {ni}')

    def has(self, name):
        if name in self.floats:
            _, ids = self.floats[name]
            return set(ids)
        if name in self.integers:
            _, _, ids, _ = self.integers[name]
            return set(ids)
        if name in self.strings:
            result = set()
            for ids in self.strings[name].values():
                result.update(ids)
            return result
        if name in self.bools:
            return self.bools[name]
        return set()

    def key(self,
            name: str,
            op: str,
            value: bool | int | float | str) -> set[int]:
        """Check "key op value" expression."""
        if name in chemical_symbols:
            n = value  # number of atoms
            assert isinstance(n, int)
            if n < 0:
                raise SyntaxError(f'{name} {op} {n} does not make sense!')

            # Special cases where no atoms is a solution:
            # X>=0, X=0, X<=n, X<n, X!=n
            if op == '>=' and n == 0:
                return self.ids
            if op == '=' and n == 0:
                return self.ids - self.key(name, '>', 0)
            if op == '<=':
                return self.ids - self.key(name, '>', n)
            if op == '<':
                if n == 0:
                    raise SyntaxError(f'{name} < 0 does not make sense!')
                return self.ids - self.key(name, '>=', n)
            if op == '!=':
                if n == 0:
                    return self.key(name, '>', 0)
                return self.ids - self.key(name, '=', n)

        if name in self.integers:
            if not isinstance(value, (int, bool)):
                raise SyntaxError(f'Bad value for {name}: "{value}".  '
                                  'Please use an integer.')
            return self.integer_key(name, op, value)

        if name in self.floats:
            assert isinstance(value, (int, float))
            return self.float_key(name, op, value)

        if name in self.strings:
            value = str(value)
            if op == '=':
                if value in self.strings[name]:
                    return self.strings[name][value]
                return set()
            if op == '!=':
                result = set()
                for val, ids in self.strings[name].items():
                    if val != value:
                        result.update(ids)
                return result
            raise SyntaxError(f'{name} {op} {value} does not make sense!')

        if name in self.bools:
            return self.bool_key(name, op, value)

        return set()

    def bool_key(self,
                 name: str,
                 op: str,
                 value: bool | int | float | str) -> set[int]:
        """Check boolean "name = value".

        At the moment, op must be "=".  Value must be one of:
        0, 1, "true", "false", "True", "False", "No", "Yes", "no", "yes".
        """
        if op != '=':
            raise SyntaxError(f'Illegal operator for boolean: {op}')

        if isinstance(value, str):
            bool_value = {'no': False, 'false': False,
                          'yes': True, 'true': True}.get(value.lower())
            if bool_value is None:
                raise SyntaxError(f'Illegal value for boolean: {value}')
            value = bool_value

        if value:
            return self.bools_true[name]
        return self.bools[name] - self.bools_true[name]

    def float_key(self, name: str, op: str, value: float) -> set[int]:
        values, ids = self.floats[name]
        if op == '!=':
            return self.ids - self.float_key(name, '=', value)
        if op == '<=':
            value = np.nextafter(value, value + 1)
            op = '<'
        if op == '>':
            value = np.nextafter(value, value + 1)
            op = '>='
        j1 = np.searchsorted(values, value)
        n = len(values)
        if op == '=':
            result = set()
            while j1 < n and values[j1] == value:
                result.add(ids[j1])
                j1 += 1
            return result
        if op == '<':
            return set(ids[:j1])
        if op == '>=':
            return set(ids[j1:])
        raise SyntaxError('Syntax nonsense')

    def integer_key(self, name: str, op: str, n: int) -> set[int]:
        nmin, nmax, ids, indices = self.integers[name]
        if op == '=':
            if nmin <= n <= nmax:
                d = n - nmin
                j1, j2 = indices[d:d + 2]
                return set(ids[j1:j2])
            return set()
        if op == '!=':
            return (self.integer_key(name, '<=', n - 1) |
                    self.integer_key(name, '>', n))
        if op == '<':
            return self.integer_key(name, '<=', n - 1)
        if op == '>=':
            return self.integer_key(name, '>', n - 1)
        if op == '<=':
            if n < nmin:
                return set()
            n = min(n, nmax)
            d = n - nmin
            j = indices[d + 1]
            return set(ids[:j])
        if op == '>':
            if n >= nmax:
                return set()
            if n < nmin:
                n = nmin - 1
            d = n - nmin
            j = indices[d + 1]
            return set(ids[j:])
        assert False, op

    def formula(self, count: dict[str, int]) -> set[int]:
        natoms = sum(count.values())
        if natoms == 1:
            ((symbol, n),) = count.items()
            return self.key(symbol, '>', 0)
        nunits = np.gcd.reduce(list(count.values()))

        # Convert to "AB2" format:
        reduced = formula_dict_to_string(
            {symbol: n // nunits
             for symbol, n in
             sorted(count.items(), key=lambda x: (x[1], x[0]))})
        ids = self.reduced[reduced]
        if nunits == 1:
            return ids
        return {id for id in ids if self.natoms[id] % natoms == 0}
