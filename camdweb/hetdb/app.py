"""Heterostructure database web-app.

The camdweb.hetdb.copy module has code to convert ~cmr/???/tree/
folders and friends (see PATTERNS variable below) to a canonical tree
layout.

Also contains web-app that can run off the tree of folders.

The goal is to have the code decoupled from ASE, GPAW, CMR and ASR.
Right now ASR webpanel() functions are still used
(see camdweb.hetdb.asr_panel module).
"""
from __future__ import annotations

import argparse
import json
from pathlib import Path

import rich.progress as progress

from camdweb.html import (Range, RangeX, stoichiometry_input, image,
                          table, SearchFilter)
from camdweb.materials import Material, Materials
from camdweb.optimade.app import add_optimade
from camdweb.panels.atoms import AtomsPanel
from camdweb.panels.misc import MiscPanel
from camdweb.panels.panel import Panel
from camdweb.utils import cod, doi, icsd, c2db
from camdweb.web import CAMDApp
from camdweb.hetdb.bs_panel import BS_Panel


class HetDBAtomsPanel(AtomsPanel):
    info = """ XXX: HetDB description:
    """

    def create_column_one(self,
                          material: Material) -> str:
        html1 = table(['Structure info', ''],
                      self.table_rows(material,
                                      ['uid_a', 'uid_b']))
        html2 = table(['Stacking info', ''],
                      self.table_rows(material,
                                      ['twist_angle',
                                       'interlayer_distance', 'maxstrain']))
        html3 = table(['Basic properties', ''],
                      self.table_rows(material,
                                      ['pbe_gap_nosoc', 'pbe_gap_dir_nosoc',
                                       'pbe_gap_soc', 'pbe_gap_dir_soc',
                                       'scs_gap_nosoc', 'scs_gap_dir_nosoc',
                                       'scs_gap_soc', 'scs_gap_nosoc']))
        return '\n'.join([html1, html2, html3])


def main(argv: list[str] | None = None) -> CAMDApp:
    """Create HetDB app."""
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'path', nargs='+',
        help='Path to tree.  Examples: "AB2" (same as "AB2/*/*"), '
        '"AB2/1MoS2" (same as "AB2/1MoS2/*/") or "AB2/1MoS2/1".')
    # args = parser.parse_args(argv)

    folders = []
    for path in Path('materials').glob('A*/*/*/'):
        folders.append(path)

    mlist: list[Material] = []
    with progress.Progress() as pb:
        pid = pb.add_task('Reading matrerials:', total=len(folders))
        for f in folders:
            uid = f'{f.parent.name}-{f.name}'
            material = Material.from_file(f / 'structure.xyz', uid)
            data = json.loads((f / 'data.json').read_text())
            material.columns.update(data)
            mlist.append(material)
            pb.advance(pid)

    panels: list[Panel] = [
        HetDBAtomsPanel(),
        BS_Panel('PBE'),
        BS_Panel('SCS'),
        MiscPanel(),
    ]

    materials = Materials(mlist, panels)

    materials.column_descriptions.update(
        uid_a='C2DB unique id for lower layer.',
        uid_b='C2DB unique id for upper layer',
        twist_angle='Twist angle [\u00B0]',
        maxstrain='Maximum strain eigenvalue [%]',
        interlayer_distance='Interlayer distance [Å]',
        pbe_evac='Vacuum level (PBE) [eV]',
        scs_evac='Vacuum level (SCS) [eV]',
        energy='Energy [eV]',
        pbe_efermi_nosoc='Fermi level wrt. vacuum w/o SOC (PBE) [eV]',
        scs_efermi_nosoc='Fermi level wrt. vacuum w/o SOC (SCS) [eV]',
        pbe_efermi_soc='Fermi level wrt. vacuum w. SOC (PBE) [eV]',
        scs_efermi_soc='Fermi level wrt. vacuum w. SOC (SCS) [eV]',
        label='Structure origin',
        pbe_gap_soc='Band gap w. SOC (PBE) [eV]',
        scs_gap_soc='Band gap w. SOC (SCS) [eV]',
        pbe_gap_dir_soc='Direct band gap w. SOC (PBE) [eV]',
        scs_gap_dir_soc='Direct band gap w. SOC (SCS) [eV]',
        pbe_gap_nosoc='Band gap w/o SOC (PBE) [eV]',
        scs_gap_nosoc='Band gap w/o SOC (SCS) [eV]',
        pbe_gap_dir_nosoc='Direct band gap w/o SOC (PBE) [eV]',
        scs_gap_dir_nosoc='Direct band gap w/o SOC (SCS) [eV]',
        pbe_vbm_nosoc='Valence band maximum wrt. vacuum w/o SOC (PBE) [eV]',
        scs_vbm_nosoc='Valence band maximum wrt. vacuum w/o SOC (SCS) [eV]',
        pbe_vbm_soc='Valence band maximum wrt. vacuum w. SOC (PBE) [eV]',
        scs_vbm_soc='Valence band maximum wrt. vacuum w. SOC (SCS) [eV]',
        pbe_cbm_nosoc='Conduction band minimum wrt. vacuum w/o SOC (PBE) [eV]',
        scs_cbm_nosoc='Conduction band minimum wrt. vacuum w/o SOC (SCS) [eV]',
        pbe_cbm_soc='Conduction band minimum wrt. vacuum w. SOC (PBE) [eV]',
        scs_cbm_soc='Conduction band minimum wrt. vacuum w. SOC (SCS) [eV]',
        folder='Original file-system folder',)

    materials.html_formatters.update(
        cod_id=cod,
        icsd_id=icsd,
        doi=doi,
        uid_a=c2db,
        uid_b=c2db)

    initial_columns = ['formula', 'uid', 'uid_a', 'uid_b',
                       'pbe_gap_soc', 'pbe_gap_dir_soc']

    root = folders[0].parent.parent.parent
    app = CAMDApp(materials,
                  initial_columns=initial_columns,
                  root=root)
    app.title = 'HetDB'
    app.logo = image('hetdb-logo.png', alt='hetdb-logo')
    app.links = [
        ('CMR', 'https://cmr.fysik.dtu.dk'),
        ('About', 'https://cmr.fysik.dtu.dk/hetdb/hetdb.html')]
    app.form_parts[0] = SearchFilter(placeholder="Example: 'MoS2'")
    app.form_parts += [
        stoichiometry_input(materials.stoichiometries()),
        Range('Twist angle [\u00B0]:', 'twist_angle'),
        RangeX('Band gap range [eV]:', 'bg',
               ['pbe_gap_nosoc', 'scs_gap_nosoc',
                'pbe_gap_nosoc', 'scs_gap_soc'],
               ['PBE w/o SOC', 'SCS w/o SOC',
                'PBE w. SOC', 'SCS w/o SOC'])]

    return app


def create_app():  # pragma: no cover
    """Create the WSGI app."""
    app = main(['materials'])
    return app.app


if __name__ == '__main__':
    app = main()
    try:
        import lark
    except ImportError:
        pass
    else:
        print('Lark:', lark.__version__)
        add_optimade(app)
    app.app.run(host='0.0.0.0', port=8081, debug=True)
