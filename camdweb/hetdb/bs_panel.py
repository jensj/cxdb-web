import json
import dataclasses
import numpy as np
import plotly.graph_objs as go

import plotly

from camdweb.html import table, HTML_PLOTLY as HTML
from camdweb.panels.panel import Panel, PanelData, SkipPanel
from camdweb.c2db.asr_panel import Row
from camdweb.bandstructure import PlotUtilPBE
from camdweb.figures import PLOTLY_SCRIPT as SCRIPT


class BS_Panel(Panel):
    def __init__(self, name: str):
        super().__init__()
        self.name = name
        self.info = INFO[name]
        if name == 'PBE':
            self.mode = 'pbe'
        elif name == 'SCS':
            self.mode = 'scs'
        else:
            raise ValueError('Unknown name: {}'.format(name))

        self.title = f'Electronic band structure ({self.mode}@PBE)'

    def get_data(self, material):
        bs = material.folder / f'bs_{self.mode}.json'
        if f'{self.mode}_gap_dir_nosoc' not in material.columns:
            raise SkipPanel
        if not (bs.is_file() or bs.with_suffix('.json.gz').is_file()):
            raise SkipPanel
        row = Row(material)
        try:
            plotter = self.plotter_from_row(row)
        except KeyError:
            # Not all materials are calculated yet.
            raise SkipPanel
        fig = plotter.plot()

        keys = [f'{self.mode}_gap_dir_nosoc', f'{self.mode}_gap_nosoc',
                f'{self.mode}_vbm', f'{self.mode}_cbm']

        # XXX: Check if metallic, this decision should be
        #      centralized one place in the code
        gaps = row.data[f'bs_{self.mode}.json']
        if gaps.get(f'{self.mode}_dir_gap_nosoc', 0.0) <= 0.0:
            keys.append(f'{self.mode}_efermi_nosoc')

        col = table(['Properties [eV]', ''],
                    [[key.replace('[eV]', ''), val]  # remove units from rows
                     for key, val in self.table_rows(material, keys)])

        bs_json = json.dumps(
            fig, cls=plotly.utils.PlotlyJSONEncoder)
        return PanelData(
            html=HTML.format(col=col,
                             name=self.name),
            script=SCRIPT.format(data=bs_json, id=self.name),
            title=self.title,
            info=self.info)

    def plotter_from_row(self, row):
        dct = row.data.get(f'bs_{self.mode}.json')

        vbm = dct['vbm_nosoc']
        cbm = dct['cbm_nosoc']
        fermilevel = dct['efermi_nosoc']

        energy_nosoc_skn = dct['eigenvalues_nosoc']
        energy_mk = energy_nosoc_skn[0].T
        dens_mk = dct['dens_nosoc'][0].T

        return PlotUtilvdW(
            path=dct['path'],
            energy_nosoc_skn=energy_nosoc_skn,
            energy_soc_mk=energy_mk,
            spin_zprojection_soc_mk=dens_mk,
            fermilevel=fermilevel,
            spin_axisname='Layer projection',
            emin=(fermilevel if vbm is None else vbm) - 3,
            emax=(fermilevel if cbm is None else cbm) + 3
        ).subtract_reference_energy(vbm_reference=vbm)


# XXX: Panel descriptions. Maybe this should have its own
# file for all descriptions?
INFO = {
    'PBE':
    """Stuff""",
    'SCS':
    """Other stuff"""}


@dataclasses.dataclass
class PlotUtilvdW(PlotUtilPBE):
    def plot(self) -> go.Figure:
        """Plot band structure.

        This plots:
         * The ordinary (non-spin–orbit-coupled) band structure
         * The spin–orbit coupled band structure coloured by spin projection
         * Reference energy (Fermi level).
        """

        e_max = self.emax
        e_min = self.emin
        KN = 500

        m_filter = np.max(self.energy_soc_mk, axis=1) > e_min
        m_filter = np.logical_and(
            m_filter, np.min(self.energy_soc_mk, axis=1) < e_max)
        self.energy_soc_mk = self.energy_soc_mk[m_filter]
        if self.spin_zprojection_soc_mk is not None:
            self.spin_zprojection_soc_mk = \
                self.spin_zprojection_soc_mk[m_filter]

        nfilter = np.max(self.energy_nosoc_skn, axis=(0, 1)) > e_min
        nfilter = np.logical_and(
            nfilter, np.min(self.energy_nosoc_skn, axis=(0, 1)) < e_max)
        self.energy_nosoc_skn = self.energy_nosoc_skn[..., nfilter]

        xcoords_p = self.xcoords
        self.xcoords = np.linspace(xcoords_p.min(), xcoords_p.max(), KN)

        from scipy.interpolate import make_interp_spline
        self.energy_soc_mk = make_interp_spline(
            xcoords_p, self.energy_soc_mk, k=1, axis=1)(self.xcoords)
        self.spin_zprojection_soc_mk = make_interp_spline(
            xcoords_p, self.spin_zprojection_soc_mk, k=1, axis=1)(self.xcoords)
        self.energy_nosoc_skn = make_interp_spline(
            xcoords_p, self.energy_nosoc_skn, k=1, axis=1)(self.xcoords)

        data = [self.plot_bands_boring(),]
        if self.spin_zprojection_soc_mk is not None:
            data.append(self.plot_bands_fancy())
        if self.fermilevel is not None:
            data.append(self.plot_reference_energy_as_line())
            ylabel = '<i>E</i> - <i>E</i><sub>F,SOC</sub> [eV]'
        else:
            ylabel = '<i>E</i> - <i>E</i><sub>VBM,SOC</sub> [eV]'
        return go.Figure(
            data=data,
            layout=self.layout(ylabel=ylabel))
