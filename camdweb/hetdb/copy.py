"""Copy HetDB files from ASR-layout to uniform tree structure.
"""
from __future__ import annotations

import warnings
import argparse
import json
import shutil
from collections import defaultdict
from pathlib import Path
import numpy as np

import rich.progress as progress
from ase import Atoms
from ase.formula import Formula

from camdweb import ColVal
from camdweb.c2db.asr_panel import read_result_file
from camdweb.c2db.uid_translation import convert_uid
from camdweb.utils import all_dirs, process_pool, read_atoms

ROOT = Path('/home/scratch3/mohsa/moire/calcs/')
UID_MAP_PATH = 'uid_translation.json'
PATTERNS = [
    '*/']
RESULT_FILES = [
    'bilayer-info',
    'gs_pbe',
    'bs_pbe',
    'gs_scs',
    'bs_scs']


def atoms_to_uid_name(atoms: Atoms) -> str:
    f = atoms.symbols.formula
    stoi, reduced, nunits = f.stoichiometry()
    return f'{nunits}{reduced}'


def create_uid(bilayer_info: dict, uid_map_path: str) -> tuple[str, str, str]:
    uid_a = bilayer_info['uid_a']
    uid_b = bilayer_info['uid_b']
    angle = bilayer_info['twist_angle']

    if Path(uid_map_path).is_file():
        with Path(uid_map_path).open('r') as uid_f:
            uid_map = json.load(uid_f)
        uid_a = convert_uid(uid_a, uid_map)
        uid_b = convert_uid(uid_b, uid_map)
    else:
        warnings.warn(
            'Cannot find UID translation file. Using old UIDs.'
        )

    uid = f'{uid_a}_{uid_b}_{angle:.0f}'
    return uid, uid_a, uid_b


def copy_materials(root: Path,
                   patterns: list[str],
                   uid_map_path: str = UID_MAP_PATH,
                   processes: int = 1) -> None:
    dirs = all_dirs(root, patterns)
    print(len(dirs), 'folders')

    names: defaultdict[str, int] = defaultdict(int)
    work = []
    with progress.Progress() as pb:
        pid = pb.add_task('Finding UIDs:', total=len(dirs))
        for dir in dirs:
            try:
                name = atoms_to_uid_name(
                    read_atoms(dir / 'structure-pw.json'))
            except FileNotFoundError:
                pb.advance(pid)
                continue
            names[name] += 1
            number = names[name]

            try:
                uid, uid_a, uid_b = create_uid(
                    json.loads((dir / 'bilayer-info.json').read_text()),
                    uid_map_path=uid_map_path)
            except FileNotFoundError:
                pb.advance(pid)
                continue
            stoichiometry, _, nunits = Formula(name).stoichiometry()
            folder = Path(f'materials/{stoichiometry}/{name}/{number}')
            work.append((dir, folder, uid, uid_a, uid_b))
            pb.advance(pid)

    parent_folders = set()
    for _, folder, _, _, _ in work:
        parent_folders.add(folder.parent)
    for folder in parent_folders:
        folder.mkdir(exist_ok=True, parents=True)

    with process_pool(processes) as pool:
        with progress.Progress() as pb:
            pid = pb.add_task('Copying materials:', total=len(work))
            for _ in pool.imap_unordered(worker, work):
                pb.advance(pid)

    # Put logo in the right place:
    logo = Path('hetdb-logo.png')
    if not logo.is_file():
        shutil.copyfile(Path(__file__).parent / 'logo.png', logo)

    # copy the webpage validation workflow script
    shutil.copyfile(Path(__file__).parent / 'wf.py', Path('wf.py'))

    print('DONE')


def worker(args):  # pragma: no cover
    """Used by Pool."""
    copy_material(*args)


def copy_material(fro: Path,
                  to: Path,
                  uid: str,
                  uid_a: str | None = None,
                  uid_b: str | None = None) -> None:  # pragma: no cover
    structure_file = fro / 'structure-pw.json'
    if not structure_file.is_file():
        return
    atoms = read_atoms(structure_file)

    to.mkdir(exist_ok=True)

    def rrf(name: str) -> dict:
        return read_result_file(fro / f'{name}.json')

    # None values will be removed later
    data: dict[str, ColVal | None] = {'folder': str(fro)}

    data['uid'] = uid
    if uid_a is not None:
        data['uid_a'] = uid_a
    if uid_b is not None:
        data['uid_b'] = uid_b

    try:
        bl_info = rrf('bilayer-info')
    except FileNotFoundError:
        pass
    else:
        data['maxstrain'] = bl_info['maxstrain']
        data['twist_angle'] = bl_info['twist_angle']

    atoms_a = atoms[atoms.get_tags() == 0]
    atoms_b = atoms[atoms.get_tags() == 1]

    data['interlayer_distance'] = \
        np.mean(atoms_b.positions[:, 2]) - np.mean(atoms_a.positions[:, 2])

    def append_bs_info(name):
        try:
            bs = rrf(f'bs_{name}')
        except FileNotFoundError:
            pass
        else:
            vacuumlevel = bs['evac']
            data[f'{name}_evac'] = vacuumlevel

            data[f'{name}_gap_dir_nosoc'] = bs['dir_gap_nosoc']
            data[f'{name}_gap_nosoc'] = bs['hl_gap_nosoc']
            data[f'{name}_efermi_nosoc'] = bs['efermi_nosoc']
            if data[f'{name}_gap_nosoc'] > 0:
                data[f'{name}_vbm_nosoc'] = bs['vbm_nosoc'] - vacuumlevel
                data[f'{name}_cbm_nosoc'] = bs['cbm_nosoc'] - vacuumlevel

            try:
                data[f'{name}_gap_dir_soc'] = bs['dir_gap_soc']
                data[f'{name}_gap_soc'] = bs['hl_gap_soc']
                data[f'{name}_efermi_soc'] = bs['efermi_soc']
                if data[f'{name}_gap_soc'] > 0:
                    data[f'{name}_vbm_soc'] = bs['vbm_soc'] - vacuumlevel
                    data[f'{name}_cbm_soc'] = bs['cbm_soc'] - vacuumlevel
            except KeyError:
                pass

    append_bs_info('pbe')
    append_bs_info('scs')

    data['energy'] = atoms.get_potential_energy()

    try:
        info = json.loads((fro / 'info.json').read_text())
    except FileNotFoundError:
        pass
    else:
        for key in ['icsd_id', 'cod_id', 'doi']:
            if key in info:
                data[key] = info[key]

    atoms.write(to / 'structure.xyz')

    # Copy result json-files:
    for name in RESULT_FILES:
        result = fro / f'{name}.json'
        target = to / result.name
        gzipped = target.with_suffix('.json.gz')
        if result.is_file() and not (target.is_file() or gzipped.is_file()):
            shutil.copyfile(result, target)

    path = to / 'data.json'

    # Remove None values:
    data = {key: value for key, value in data.items() if value is not None}

    path.write_text(json.dumps(data, indent=0))


def main(argv: list[str] | None = None):
    parser = argparse.ArgumentParser()
    parser.add_argument('root', help='Root of ASR-tree to copy from. '
                        'Example: "~cmr/HetDB-ASR/".')
    patterns = ', '.join(f'"{p}"' for p in PATTERNS)
    parser.add_argument(
        'pattern', nargs='+',
        help='Glob pattern like "tree/A*/*/*/". '
        f'Use "ALL" to get all the standard patterns: {patterns}.')
    parser.add_argument('-u', '--uid-map', type=str, default=UID_MAP_PATH)
    parser.add_argument('-p', '--processes', type=int, default=1)
    args = parser.parse_args(argv)
    if args.pattern == ['ALL']:  # pragma: no cover
        args.pattern = PATTERNS
    copy_materials(Path(args.root), args.pattern, args.uid_map,
                   args.processes)


if __name__ == '__main__':
    main()
