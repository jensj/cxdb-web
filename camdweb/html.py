from __future__ import annotations

import abc
from pathlib import Path
from typing import Iterable, Sequence
from ase.formula import Formula

HTML_ROW = """
<div class="row row-cols-1 row-cols-lg-2">
  {table}
</div>
"""
HTML_1COL = """
<div class="row row-cols-1 row-cols-lg-2">
  <div class="col">
    {col1}
  </div>
</div>
"""
HTML_2COL = """
<div class="row row-cols-1 row-cols-lg-2">
  <div class="col">
    {col1}
  </div>
  <div class="col">
    {col2}
  </div>
</div>
"""
HTML_4COL = """
<div class="row row-cols-1 row-cols-lg-2">
  <div class="col">
    {col1}
    {col2}
  </div>
  <div class="col">
    {col3}
    {col4}
  </div>
</div>
"""
HTML_PLOTLY = """
<div class="row row-cols-1 row-cols-lg-2">
  <div class="col">
    <div id='{name}'></div>
  </div>
  <div class="col">
    {col}
  </div>
</div>
"""


def stoichiometry_input(stoichiometries: list[str]):
    if len(stoichiometries) > 20:
        # too many to list them all
        return StoichiometryInput()
    return Select('Stoichiometry:', 'stoichiometry',
                  [''] + stoichiometries, ['-'] + stoichiometries)


def structure_origin(labels: list[str]):
    if len(labels) > 20:
        # too many to list them all
        return StoichiometryInput()
    return Select('Structure origin:', 'label',
                  [''] + labels, ['-'] + labels)


def complex_table_head(header: list[list[str]],
                       col_spacing: str = 'class="w-50"') -> str:
    """
        table head allowing for multiple row header and cellspanning
    """
    head = '  <thead>\n'
    for headrow in header:
        head += '   <tr {col_spacing}>\n'
        for headcell in headrow:
            if '_span=' in headcell:
                celllist = headcell.split('_span=')
                cellcontent = celllist[0]
                spanlist = celllist[1].split(',')
                colspan = int(spanlist[0])
                rowspan = int(spanlist[1])

                if colspan > 1:
                    colspan_str = f'colspan="{colspan}"'
                else:
                    colspan_str = ''

                if rowspan > 1:
                    rowspan_str = f'rowspan="{rowspan}"'
                else:
                    rowspan_str = ''

                head += (f'    <th {col_spacing} {colspan_str}' +
                         f'{rowspan_str}>{cellcontent}</th>\n')
            else:
                head += f'    <th {col_spacing}>{headcell}</th>\n'
        head += '   </tr>\n'
    head += '  </thead>\n'
    return head


def table(header: list[str] | None | str,
          rows: Sequence[Iterable],
          *,
          col_spacing: str = 'class="w-50"',
          responsive: bool = True,
          striped: bool = True) -> str:
    """Create HTML table.

    Example:

    === =====
    A   B
    === =====
    1.2 hello
    2.0 hi!
    === =====

    >>> print(table(['A', 'B'], [[1.2, 'hello'], [2.0, 'hi!']]))
    <div class="table-responsive">
     <table class="table table-striped">
      <thead>
       <tr class="w-50">
        <th class="w-50">A</th>
        <th class="w-50">B</th>
       </tr>
      </thead>
      <tbody>
       <tr class="w-50">
        <td class="w-50">1.2</td>
        <td class="w-50">hello</td>
       </tr>
       <tr class="w-50">
        <td class="w-50">2.0</td>
        <td class="w-50">hi!</td>
       </tr>
      </tbody>
     </table>
    </div>
    """
    if header is None:
        head = ''
    elif isinstance(header, str):
        head = header
    else:
        head = (f'  <thead>\n   <tr {col_spacing}>\n    <th {col_spacing}>' +
                f'</th>\n    <th {col_spacing}>'.join(header) +
                '</th>\n   </tr>\n  </thead>\n')

    classes = 'table table-striped' if striped else 'table'
    html_table = (
        f' <table class="{classes}">\n{head}' +
        f'  <tbody>\n   <tr {col_spacing}>\n    ' +
        f'\n   </tr>\n   <tr {col_spacing}>\n    '.join(
            '\n    '.join(f'<td {col_spacing}>{x}</td>' for x in row)
            for row in rows) +
        '\n   </tr>\n  </tbody>\n </table>')

    return f'<div class="table-responsive">\n{html_table}\n</div>' \
        if responsive else html_table


def downloadable(path: Path | str, file: str) -> str:
    """Create a downloadable data file line and button.

    >>> print(downloadable('abc', 'my.json'))
    <a download="my.json" href="/downloadable/abc/my.json"
       class="btn btn-info btn-sm">JSON</a>
    """
    ext = file.split('.')[-1].upper()
    return (f'<a download="{file}" href="/downloadable/{path}/{file}"\n'
            f'   class="btn btn-info btn-sm">{ext}</a>')


def image(path: Path | str, alt=None) -> str:
    """Create <img> tag.

    >>> image('abc/def.png', alt='Short description')
    '<img alt="Short description" src="/png/abc/def.png" class="img-fluid">'
    """
    return f'<img alt="{alt or path}" src="/png/{path}" class="img-fluid">'


class FormPart(abc.ABC):
    def __init__(self, text: str, name: str):
        self.text = text
        self.name = name

    @abc.abstractmethod
    def render(self) -> str:
        raise NotImplementedError

    def get_filter_strings(self, query: dict) -> list[str]:
        val = query.get(self.name, '')
        if val:
            return [f'{self.name}={val}']
        return []


class Select(FormPart):
    def __init__(self, text, name, options, names=None, default=None):
        super().__init__(text, name)
        self.options = options
        self.names = names
        self.default = default or options[0]

    def render(self) -> str:
        """Render select block.

        >>> s = Select('Bla-bla', 'xyz', ['A', 'B', 'C'])
        >>> html = s.render()
        >>> s.get_filter_strings({'xyz': 'C'})
        ['xyz=C']
        """
        parts = [
            '<div class="form-group row pb-1 align-items-center">',
            f'<label for="{self.name}" class="col-4 col-form-label-sm">'
            f'  {self.text}</label>'
            '<div class="col">',
            f'<select class="form-select" name="{self.name}" id="{self.name}">'
        ]
        names = self.names or self.options
        for val, txt in zip(self.options, names):
            selected = ' selected' if val == self.default else ''
            parts.append(f'  <option value="{val}"{selected}>{txt}</option>')
        parts.append('</select></div></div>')
        return '\n'.join(parts)


class Input(FormPart):
    def __init__(self, text, name, placeholder='...'):
        super().__init__(text, name)
        self.placeholder = placeholder

    def render(self) -> str:
        """Render input block.

        >>> s = Input('Bla-bla', 'xyz')
        >>> html = s.render()
        """
        parts = [
            '<div class="form-group row pb-1 align-items-center">',
            '<label class="col-4 col-form-label-sm">',
            f'  {self.text}',
            '</label>',
            '<div class="col">',
            '<input',
            '  class="form-control"',
            '  type="text"',
            f'  name="{self.name}"',
            '  value=""',
            f'  placeholder="{self.placeholder}" /></div></div>']
        return '\n'.join(parts)


class StoichiometryInput(Input):
    def __init__(self):
        super().__init__('Stoichiometry:', 'stoichiometry', 'A, AB2, ABC, ...')

    def get_filter_strings(self, query: dict) -> list[str]:
        """Make sure A2B and AB2 both work.

        >>> s = StoichiometryInput()
        >>> s.get_filter_strings({'stoichiometry': 'A2B'})
        ['stoichiometry=AB2']
        >>> s.get_filter_strings({})
        []
        >>> s.get_filter_strings({'stoichiometry': 'garbage'})
        ['stoichiometry=garbage']
        """
        val = query.get(self.name, '')
        if not val:
            return []
        # Reduce A2B2 to AB and so on.
        try:
            f = Formula(val)
        except ValueError:
            pass
        else:
            val = f.reduce()[0].stoichiometry()[0].format('ab2')
        return [f'{self.name}={val}']


class Range(FormPart):
    def __init__(self,
                 text: str,
                 name: str,
                 nonnegative: bool = False,
                 default: tuple[str, str] = ('', '')):
        super().__init__(text, name)
        self.nonnegative = nonnegative
        self.default = default

    def render(self) -> str:
        """Render range block.

        >>> s = Range('Band gap', 'gap')
        >>> html = s.render()
        """
        v1, v2 = self.default
        parts = [
            '<div class="form-group row pb-1 align-items-center">',
            f'<label for="from_{self.name}"',
            '  class="col-4 col-form-label-sm">',
            f'  {self.text}',
            '</label>',
            '<div class="col">',
            '<input class="form-control"',
            '  type="text"',
            f'  name="from_{self.name}"',
            f'  id="from_{self.name}"',
            f'  value="{v1}" />\n</div>',
            f'<label for="to_{self.name}" class="col-auto gx-0">-</label>',
            '<div class="col">',
            '<input class="form-control"',
            '  type="text"',
            f'  id="to_{self.name}" ',
            f'  name="to_{self.name}"',
            f'  value="{v2}" /></div></div>']
        return '\n'.join(parts)

    def get_filter_strings(self, query: dict) -> list[str]:
        filters = []
        fro = query.get(f'from_{self.name}', '')
        if fro:
            limit = float(fro)
            if not self.nonnegative or limit > 0.0:
                filters.append(f'{self.name}>={fro}')
        to = query.get(f'to_{self.name}', '')
        if to:
            filters.append(f'{self.name}<={to}')
        return filters


class RangeX(Range):
    def __init__(self, text, name, options, names=None):
        super().__init__(text, name)
        self.options = options
        self.names = names

    def render(self) -> str:
        parts = [
            '<div class="form-group row pb-1 align-items-center">',
            f'<label for="from_{self.name}"',
            '  class="col-4 col-form-label-sm">',
            f'  {self.text}',
            '</label>',
            '<div class="col">',
            '<input',
            '  class="form-control"',
            '  type="text"',
            f'  name="from_{self.name}"',
            '  value="" />',
            '</div>',
            f'<label for="to_{self.name}" class="col-auto g-0">-</label>',
            '<div class="col">',
            '<input',
            '  class="form-control"',
            '  type="text"',
            f'  name="to_{self.name}"',
            '  value="" />',
            '</div>'
            '<div class="col-auto">',
            f'<select name="{self.name}" class="form-select">']
        names = self.names or self.options
        for val, txt in zip(self.options, names):
            selected = ' selected' if val == '' else ''
            parts.append(f'  <option value="{val}"{selected}>{txt}</option>')
        parts.append('</select></div></div>')
        return '\n'.join(parts)

    def get_filter_strings(self, query: dict) -> list[str]:
        filters = []
        x = query.get(self.name)
        fro = query.get(f'from_{self.name}', '')
        if fro:
            filters.append(f'{x}>={fro}')
        to = query.get(f'to_{self.name}', '')
        if to:
            filters.append(f'{x}<={to}')
        return filters


class RangeS(Range):
    def __init__(self,
                 text: str,
                 name: str,
                 options: list[str],
                 names: list[str] | None = None,
                 default: tuple[str, str] = ('', '')):
        super().__init__(text, name, default=default)
        self.options = options
        self.names = names

    def render(self) -> str:
        names = self.names or self.options
        v1, v2 = self.default

        parts = [
            '<div class="form-group row pb-1 align-items-center">',
            f'<label for="from_{self.name}"',
            '  class="col-4 col-form-label-sm">',
            f'  {self.text}',
            '</label>',
            '<div class="col">',
            f'<select name="from_{self.name}" class="form-select">']
        for val, txt in zip(self.options, names):
            selected = ' selected' if val == v1 else ''
            parts.append(f'  <option value="{val}"{selected}>{txt}</option>')
        parts.append('</select></div>')

        parts += [
            '<label class="col-auto gx-0">-</label>',
            '<div class="col">',
            f'<select name="to_{self.name}" class="form-select">']
        for val, txt in zip(self.options, names):
            selected = ' selected' if val == v2 else ''
            parts.append(f'  <option value="{val}"{selected}>{txt}</option>')
        parts.append('</select></div></div>')

        return '\n'.join(parts)

    def get_filter_strings(self, query: dict) -> list[str]:
        filters = []
        fro = query.get(f'from_{self.name}', '')
        if fro:
            filters.append(f'{self.name}>={fro}')
        to = query.get(f'to_{self.name}', '')
        if to:
            filters.append(f'{self.name}<={to}')
        return filters


SEARCH = """
<div class="row">
  <div class="col">
    <div class="mb-3">
      <div class="input-group">
        <input class="form-control shadow-sm" type="text"
               name="filter" value=""
               placeholder="{placeholder}">
        <button class="btn btn-primary shadow-sm" type="submit">Search</button>
      </div>
    </div>
  </div>
  <div class="col-auto d-flex justify-content-end">
    <div class="mb-3">
      <div class="input-group">
        <button class="btn btn-info shadow-sm ms-2"
                type="button"
                onclick="javascript:window.open('{help_url}', '_blank')">
        Help
        </button>
      </div>
    </div>
  </div>
</div>
"""
HELP_URL = 'https://cmr.fysik.dtu.dk/searching.html'


class SearchFilter(FormPart):
    def __init__(self, placeholder: str = 'Example: NaCl'):
        self.placeholder = placeholder
        super().__init__('', '')

    def render(self) -> str:
        return SEARCH.format(placeholder=self.placeholder,
                             help_url=HELP_URL)

    def get_filter_strings(self, query: dict) -> list[str]:
        filter = query.get('filter', '').strip()
        if filter:
            return [filter]
        return []
