from __future__ import annotations

from typing import Callable, Generator, Sequence

from ase.formula import Formula

from camdweb import ColVal
from camdweb.filter import Index
from camdweb.material import Material
from camdweb.paging import Pages, get_pages_object
from camdweb.panels.panel import Panel, default_formatter
from camdweb.parse import parse, select
from camdweb.session import Session
from camdweb.utils import html_format_formula


def formula_formatter(value: ColVal, link: bool = False) -> str:
    """Use AB2 sorting of elements.

    >>> formula_formatter('CSi2')
    'CSi<sub>2</sub>'
    """
    assert isinstance(value, str)
    return html_format_formula(value)


def formula_formatter_periodic(value: ColVal, link: bool = False) -> str:
    """Use period-first-then-group sorting of elements.

    >>> formula_formatter_periodic('CSi2')
    'Si<sub>2</sub>C'
    """
    assert isinstance(value, str)
    return Formula(Formula(value).format('periodic')).format('html')


class Materials:
    def __init__(self,
                 materials: Sequence[Material],
                 panels: Sequence[Panel]):
        self.panels = panels
        self._materials = {material.uid: material for material in materials}
        for material in self:
            for panel in panels:
                panel.update_material(material)

        self.index = Index([(mat.count, mat.columns) for mat in self])
        self.i2uid = {i: mat.uid for i, mat in enumerate(self)}
        self.column_descriptions = {
            'formula': 'Formula',
            'reduced': 'Reduced formula',
            'stoichiometry': 'Stoichiometry',
            'nspecies': 'Number of species',
            'natoms': 'Number of atoms',
            'uid': 'Unique ID',
            'length': 'Unit cell length [Å]',
            'area': 'Unit cell area [Å<sup>2</sup>]',
            'volume': 'Unit cell volume [Å<sup>3</sup>]'}

        self.html_formatters = {
            'formula': formula_formatter_periodic,
            'reduced': formula_formatter_periodic,
            'stoichiometry': formula_formatter}

        self._update_panels()

    def _update_panels(self) -> None:
        for panel in self.panels:
            panel.update_column_descriptions(self.column_descriptions)
            panel.update_html_formatters(self.html_formatters)

        keys: set[str] = set()
        for material in self:
            keys.update(material.columns)

        for name in self.column_descriptions.keys() - keys:
            del self.column_descriptions[name]

    def __iter__(self) -> Generator[Material, None, None]:
        yield from self._materials.values()

    def __len__(self) -> int:
        return len(self._materials)

    def get_callbacks(self) -> dict[str, Callable[[Material, int], str]]:
        callbacks = {}
        for panel in self.panels:
            callbacks.update(panel.callbacks)
        return callbacks

    def stoichiometries(self) -> list[str]:
        """Construct list of stoichiometries present."""
        s = set()
        for material in self:
            s.add(material.stoichiometry)
        return sorted(s)

    def bravais(self) -> list[str]:
        """Construct list of bravais_search present."""
        s = set()
        for material in self:
            bravais = material.columns.get('bravais_search', None)
            if bravais is not None:
                s.add(str(bravais))
        return sorted(s)

    def labels(self) -> list[str]:
        """Construct list of labels present."""
        s = set()
        for material in self:
            label = material.columns.get('label', None)
            if label is not None:
                s.add(str(label))
        return sorted(s)

    def __getitem__(self, uid: str) -> Material:
        return self._materials[uid]

    def __contains__(self, uid):
        return uid in self._materials

    def get_rows(self,
                 session: Session) -> tuple[list[tuple[str, list[str]]],
                                            list[tuple[str, str]],
                                            Pages,
                                            list[tuple[str, str]],
                                            str]:
        """Filter rows for table.

        Example::

            rows, header, pages, new_columns, error = materials.get_rows(
                session)

        The returned values are:

        rows:
            list of rows, where each row is a tuple of uid and list of
            HTML-strings.

        header:
            list of (column name, column HTML-string) tuples.

        pages:
            stuff for pagination buttons (see get_pages_object() function).

        new_columns:
            list of (column name, columns HTML-string) tuples for columns not
            shown.

        error:
            Error message.
        """
        filter = session.filter
        try:
            col_numbers = list(select(parse(filter), self.index))
        except SyntaxError as ex:
            error = ex.args[0]
            col_numbers = []
        except Exception:
            print(f'Syntax error: {filter!r}')
            error = 'Syntax error'
            col_numbers = []
        else:
            error = ''

        rows = [self[self.i2uid[i]] for i in col_numbers]

        if rows and session.sort:
            # Materials without the key used for sorting should be shown last:
            ok = []
            missing = []
            for material in rows:
                value = material.columns.get(session.sort)
                if value is None:
                    missing.append(material)
                else:
                    ok.append((value, material))

            rows = [material for _, material in
                    sorted(ok, key=lambda x: x[0],
                           reverse=session.direction == -1)]
            rows += missing

        page = session.page
        n = session.rows_per_page
        pages = get_pages_object(page, len(rows), n)
        rows = rows[n * page:n * (page + 1)]

        formatters = [self.html_formatters.get(name, default_formatter)
                      for name in session.columns]
        table = []
        for material in rows:
            columns = []
            for name, formatter in zip(session.columns, formatters):
                value = material.columns.get(name)
                if value is None:
                    columns.append('')
                else:
                    columns.append(formatter(value))
            table.append((material.uid, columns))

        headers = [(name, self.column_descriptions.get(name, name))
                   for name in session.columns]

        new_columns = [(name, value)
                       for name, value in self.column_descriptions.items()
                       if name not in session.columns]

        return (table, headers, pages, new_columns, error)
