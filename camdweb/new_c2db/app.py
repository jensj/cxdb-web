from camdweb.c2db.app import main as c2dbmain


def create_app():
    app = c2dbmain(['A*/*/*'])
    return app.app


if __name__ == '__main__':
    app = c2dbmain()
    app.app.run(host='0.0.0.0', port=8090, debug=True)
