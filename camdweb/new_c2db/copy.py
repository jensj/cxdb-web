"""Copy C2DB files from ASRLIB layout to uniform tree structure.

Build your web tree like this (last step is repeatable):

    $ mkdir web
    $ cd web
    $ python -m camdweb.new_c2db.copy <base-dir>

Generate the convex hull like this (last step is repeatable):

    $ wget https://cmr.fysik.dtu.dk/_downloads/oqmd123.db
    $ python -m camdweb.c2db.oqmd123 <path-to-oqmd123.db>
    $ python -m camdweb.c2db.convex_hull

Boot the application like this (repeatable):

    $ python -m camdweb.c2db.app

To do: Calculate PBE+D3 references for the convex hull XXX
"""
from __future__ import annotations

import json
import numpy as np
from pathlib import Path

from ase import Atoms
from ase.formula import Formula

from camdweb import ColVal
from camdweb.atomsdata import AtomsData


def main(base: Path):
    from taskblaster.state import State
    from taskblaster.repository import Repository
    from taskblaster.records import get_record_from_repo

    # Go through the subworkflows, ordered by chronology:
    for subwf, extractor in zip(['core'], [core_data_extractor]):
        root = base / subwf / 'tree'
        with Repository.find(root) as repo:
            # Loop through each node of the taskblaster repo
            for node in repo.tree([root.resolve()]).nodes():
                # Copy only data of done tasks
                if node.state is not State.done:
                    continue
                # Extract output and copy results
                record = get_record_from_repo(node.name, repo)
                copy_result(root, node.name, record.output, extractor)


def core_data_extractor(task: str, output):
    data: dict[str, ColVal | dict[str, ColVal]] = {}
    if task == 'iterative_relax/results':
        # Extract atoms
        data['structure'] = output.atoms
        # Extract structure data
        data['thickness'] = np.ptp(output.atoms.positions[:, 2])
        symresults = output.relaxation.symresults
        data['number'] = symresults.space_group
        data['international'] = symresults.international_symbol
        data['pointgroup'] = symresults.point_group
        data['has_inversion_symmetry'] = symresults.has_inversion_symmetry
        data['lgnum'] = symresults.layer_group
        data['layergroup'] = symresults.international_layer_group_symbol
        data['bravais_type'] = symresults.layer_bravais_type
        # Extract magnetic classification
        data['is_magnetic'] = output.classification.spinpol
    elif task == 'ground_state/results':
        # Extract energy
        data['energy'] = output.energy
        # Extract magnetic moment(s)
        data['magmom'] = output.properties.magmom
        data['magmoms'] = {'magmoms': output.properties.magmoms.tolist()}
        # Extract dipole data
        data['dipz'] = output.out_of_plane_dipole
        data['evacdiff'] = output.vacuum_level_difference
        # Extract band information
        # NB: once a spin-orbit coupling step is added to the ground state
        # workflow, we need to adjust the data extraction here XXX
        data['efermi'] = output.band_info.efermi
        if output.band_info.is_gapped():
            data['gap'] = output.band_info.gap
            data['gap_dir'] = output.band_info.direct_gap
            data['vbm'] = max(output.band_info.gap_info.evbm_s)
            data['cbm'] = min(output.band_info.gap_info.ecbm_s)
        else:
            data['gap'] = 0.0
            if output.band_info.is_half_metallic():
                data['halfmetal_gap'] = output.band_info.half_metallic_gap
                data['halfmetal_gap_dir'] = \
                    output.band_info.half_metallic_direct_gap
            elif output.band_info.nspins > 1:
                data['halfmetal_gap'] = 0.0
        # Rescale energies with respect to the vacuum level
        evac = output.vacuum_level()
        data['evac'] = evac
        for key in ['efermi', 'vbm', 'cbm']:
            if key in data:
                data[key] -= evac
    else:  # Nothing to copy
        return
    return data


def copy_result(root: Path, node_name: str, output, extractor):
    uid, task = node_name.split('/', 1)

    # Extract data to a single dictionary
    data = extractor(task, output)
    if data is None:
        return
    data = {
        'folder': str((root / uid).resolve()),
        # mandatory keys
        'hform': np.nan,
        'ehull': np.nan,
        'uid': uid,  # this is in fact the old uid XXX
        **data,
    }

    # Identify destination folder
    dest = get_destination(uid)
    print(node_name, '->', dest)

    # Read any preexisting data
    data_file = dest / 'data.json'
    if data_file.is_file():
        old_data = json.loads(data_file.read_text())
    else:
        assert not dest.is_dir()
        dest.mkdir(parents=True)
        old_data = {}
    assert dest.is_dir()

    # Copy over atoms instances
    for key in [key for key, value in data.items()
                if isinstance(value, Atoms)]:
        atoms = data.pop(key)
        AtomsData.from_atoms(atoms).write_json(dest / f'{key}.json')
    # Copy over data dictionaries
    for key in [key for key, value in data.items()
                if isinstance(value, dict)]:
        dict_file = dest / f'{key}.json'
        dict_file.write_text(json.dumps(data.pop(key), indent=0))

    # Write to data file
    all_data = {**old_data, **data}
    data_file.write_text(json.dumps(all_data, indent=0))


def get_destination(uid: str):
    name, tag = uid.split('-')
    stoichiometry, _, _ = Formula(name).stoichiometry()
    return Path(f'materials/{stoichiometry}/{name}/{tag}')


if __name__ == '__main__':
    import sys

    assert len(sys.argv) == 2
    main(Path(sys.argv[1]))
