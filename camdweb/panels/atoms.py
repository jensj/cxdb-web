"""Panel for showing the atomic structure and additional tables.

Content::

  +-----------+-------------------------------+
  | column 1  | repeat-unit-cell button       |
  | ...       | download button               |
  |           +-------------------------------+
  |           | ball and stick plotly plot    |
  |           | ...                           |
  |           |                               |
  |           +-------------------------------+
  |           | unit cell: vectors            |
  |           +-------------------------------+
  |           | unit cell: lengths and angles |
  +-----------+-------------------------------+
"""
from __future__ import annotations

import sys
from functools import cache
from math import nan

import numpy as np
import plotly.graph_objects as go
from ase import Atoms
from ase.data import covalent_radii
from ase.data.colors import jmol_colors
from ase.geometry.cell import cell_to_cellpar
from ase.neighborlist import neighbor_list
from scipy.spatial import ConvexHull

from camdweb.html import table, HTML_2COL as HTML
from camdweb.material import Material
from camdweb.materials import formula_formatter_periodic
from camdweb.panels.panel import Panel, PanelData
from camdweb.utils import read_atoms
from camdweb.figures import PLOTLY_SCRIPT as SCRIPT, plotly2json


def default_repeat(material):
    cell_cv = material.atoms.cell
    pbc_c = material.atoms.pbc
    if material.default_repeat_unitcell is not None:
        return material.default_repeat_unitcell
    if not np.any(pbc_c):
        return 1
    V = np.abs(np.linalg.det(cell_cv[pbc_c][:, pbc_c]))
    return min(4, int(np.round(15 / V**(1 / np.sum(pbc_c)))))


def repeat_options(selected, maximum):
    return "".join([f'<option value="{value}"'
                    f'{" selected" if value == selected else ""}>'
                    f'{value}</option>'
                    for value in range(1, maximum + 1)])


COLUMN2 = """
    <label>Repeat:</label>
    <select onchange="cb(this.value, 'atoms', '{uid}')">
    {options}
    </select>

    &emsp;

    <label>Download:</label>
    <a download="{uid}.xyz"
       class="btn btn-info btn-sm" href={uid}/download/xyz>XYZ</a>
    <a download="{uid}.cif"
       class="btn btn-info btn-sm" href={uid}/download/cif>CIF</a>
    <a download="{uid}.json"
       class="btn btn-info btn-sm" href={uid}/download/json>JSON</a>

    <div id='atoms' class='atoms'></div>
    {axes}
"""

DIMS = ['', 'length', 'area', 'volume']


class AtomsPanel(Panel):
    title = 'Overview'

    def __init__(self) -> None:
        super().__init__()
        self.callbacks = {'atoms': self.plot}

    def get_data(self,
                 material: Material) -> PanelData:
        col1 = self.create_column_one(material)
        col2, script = self.create_column_two(material)
        self.replace_shortname_ref('Haastrup2018')
        self.replace_shortname_ref('Fu2024')
        self.replace_shortname_ref('Pakdel2024')

        return PanelData(
            HTML.format(col1=col1,
                        col2=col2),
            title=(f'{self.title}: '
                   f'{formula_formatter_periodic(material.formula)}'),
            expanded=True,
            script=script)

    def create_column_one(self,
                          material: Material) -> str:
        return ''

    def create_column_two(self,
                          material: Material) -> tuple[str, str]:
        defrep = default_repeat(material)
        return (
            COLUMN2.format(
                axes=self.axes(material),
                options=repeat_options(defrep, 4),
                uid=material.uid,
                id='atoms'),
            SCRIPT.format(data=self.plot(material, defrep), id='atoms'))

    def axes(self, material: Material) -> str:
        atoms = material.atoms
        tbl1 = table(
            ['Axis', 'x [Å]', 'y [Å]', 'z [Å]', 'Periodic'],
            [[i + 1, *[f'{x:.3f}' for x in axis], 'Yes' if p else 'No']
             for i, (axis, p) in enumerate(zip(atoms.cell, atoms.pbc))],
            striped=False,
            col_spacing='')
        C = cell_to_cellpar(atoms.cell)
        tbl2 = table(
            None,
            [['Lengths [Å]', *[f'{x:.3f}' for x in C[:3]]],
             ['Angles [°]', *[f'{x:.3f}' for x in C[3:]]]],
            striped=False,
            col_spacing='')
        return tbl1 + tbl2

    def plot(self, material: Material, repeat: int = 1) -> str:
        assert repeat < 5, 'DOS!'
        atoms = material.atoms.atoms_object()
        unitcell = atoms.cell.copy()
        atoms2 = atoms.copy()
        atoms2.wrap(eps=0.01)
        show_magmoms = True
        try:
            atoms2.set_initial_magnetic_moments(atoms.calc.results['magmoms'])
        except (AttributeError, KeyError):
            show_magmoms = False
        atoms2 = atoms2.repeat([repeat if p else 1 for p in atoms.pbc])
        fig = plot_atoms(atoms2, unitcell, show_magmoms=show_magmoms)
        return plotly2json(fig)


# The 12 edges of a cube:
UNITCELL = [[0, 0, 0], [1, 0, 0], [1, 1, 0], [0, 1, 0], [0, 0, 0],
            [0, 0, 1], [1, 0, 1], [1, 1, 1], [0, 1, 1], [0, 0, 1],
            [nan, nan, nan], [1, 0, 0], [1, 0, 1],
            [nan, nan, nan], [0, 1, 0], [0, 1, 1],
            [nan, nan, nan], [1, 1, 0], [1, 1, 1]]


def get_bonds(atoms):
    i, j, D, S, d = neighbor_list('ijDSd', atoms,
                                  cutoff=covalent_radii[atoms.numbers] * 1.2)

    # For bonds inside the cell (i.e., cell displacement S==(0, 0, 0)),
    # bonds are double-counted.  This mask un-doublecounts them:
    doublecount_mask = (i < j) | S.any(1)
    i = i[doublecount_mask]
    j = j[doublecount_mask]
    D = D[doublecount_mask]
    S = S[doublecount_mask]
    d = d[doublecount_mask]
    return i, j, D, S, d


def plot_atoms(atoms: Atoms,
               unitcell: np.ndarray | None = None,
               show_magmoms: bool = False) -> go.Figure:
    """Ball and stick plotly figure."""

    # Horible scaling stuff because plotly is horrible
    xmin = np.inf
    xmax = -np.inf
    ymin = np.inf
    ymax = -np.inf
    zmin = np.inf
    zmax = -np.inf

    data = []
    # Atoms:
    points, triangles = triangulate_sphere(len(atoms) > 50)
    i, j, k = triangles.T
    for Z, symbol, xyz, magmom in zip(atoms.numbers,
                                      atoms.symbols,
                                      atoms.positions,
                                      atoms.get_initial_magnetic_moments()):
        x, y, z = (points * covalent_radii[Z] * 0.5 + xyz).T

        # Horible scaling stuff because plotly is horrible
        if len(x) > 0 and len(y) > 0 and len(z) > 0:
            xmin = min(xmin, np.nanmin(x))
            xmax = max(xmax, np.nanmax(x))
            ymin = min(ymin, np.nanmin(y))
            ymax = max(ymax, np.nanmax(y))
            zmin = min(zmin, np.nanmin(z))
            zmax = max(zmax, np.nanmax(z))

        magmoms = f'<i>Magmom</i> {magmom:.2f}' if show_magmoms else ''
        hovertemplate = (
            f'<b>{symbol}</b><br><i>Coord.</i>'
            f'{xyz[0]:.2f} {xyz[1]:.2f} {xyz[2]:.2f}<br>{magmoms}')

        mesh = go.Mesh3d(x=x, y=y, z=z,
                         i=i, j=j, k=k, opacity=1,
                         color=color(Z),
                         text=symbol,
                         name='',
                         hovertemplate=hovertemplate)
        data.append(mesh)

    # Bonds:
    i, j, D, S, d = get_bonds(atoms)
    p = atoms.positions

    # Only add hover text to bond center
    text = []
    for L in d:
        bond_txt = f'{L:.2f} Å'
        text += ['', bond_txt, '', '']

    xyz = np.empty((3, len(i) * 4))

    # bond consists of 3 actual points (0, 1 and 2) and final nan value (3)
    # to break the line, and this is repeated in sequence for each bond
    xyz[:, 0::4] = p[i].T
    xyz[:, 1::4] = (p[i] + D * 0.5).T
    xyz[:, 2::4] = (p[i] + D).T
    # If bond exits supercell, shorten it by half
    xyz[:, 2::4][:, S.any(1)] = np.nan
    xyz[:, 3::4] = np.nan

    x, y, z = xyz
    # Horible scaling stuff because plotly is horrible
    if len(x) > 0 and len(y) > 0 and len(z) > 0:
        xmin = min(xmin, np.nanmin(x))
        xmax = max(xmax, np.nanmax(x))
        ymin = min(ymin, np.nanmin(y))
        ymax = max(ymax, np.nanmax(y))
        zmin = min(zmin, np.nanmin(z))
        zmax = max(zmax, np.nanmax(z))

    data.append(go.Scatter3d(x=x, y=y, z=z, text=text, mode='lines',
                             hovertemplate='%{text}',
                             name='',
                             line=dict(color='grey', width=5),
                             showlegend=False))

    # Unit cell:
    if unitcell is None:
        unitcell = atoms.cell

    x, y, z = (UNITCELL @ unitcell).T
    # Horible scaling stuff because plotly is horrible
    if len(x) > 0 and len(y) > 0 and len(z) > 0:
        xmin = min(xmin, np.nanmin(x))
        xmax = max(xmax, np.nanmax(x))
        ymin = min(ymin, np.nanmin(y))
        ymax = max(ymax, np.nanmax(y))
        zmin = min(zmin, np.nanmin(z))
        zmax = max(zmax, np.nanmax(z))

    data.append(go.Scatter3d(x=x, y=y, z=z, mode='lines',
                             hovertemplate='',
                             hoverinfo='skip',
                             name='',
                             line=dict(color='red', width=3),
                             showlegend=False))

    fig = go.Figure(data=data)
    fig.update_layout(scene=dict(xaxis_visible=False,
                                 yaxis_visible=False,
                                 zaxis_visible=False,
                                 xaxis_range=[xmin, xmax],
                                 yaxis_range=[ymin, ymax],
                                 zaxis_range=[zmin, zmax]),
                      autosize=True,  # XXX maybe this does nothing
                      margin=dict(l=0, r=0, b=0, t=0),
                      template='simple_white')

    plotscalling = 2

    fig.update_xaxes(showgrid=False)
    fig.update_scenes(aspectmode='manual',
                      aspectratio=dict(x=plotscalling,
                                       y=plotscalling *
                                       (ymax - ymin) / (xmax - xmin),
                                       z=plotscalling *
                                       (zmax - zmin) / (xmax - xmin)),
                      camera=dict(projection=dict(type='orthographic'),
                                  up=dict(x=0, y=1, z=0),
                                  eye=dict(x=0, y=0, z=10),
                                  center=dict(x=0, y=0, z=0)))

    return fig


# 50 Lebedev quadrature points:
SPHERE_POINTS = np.array(
    [[-1.0, 0.0, 0.0],
     [1.0, 0.0, 0.0],
     [0.0, -1.0, 0.0],
     [0.0, 1.0, 0.0],
     [0.0, 0.0, -1.0],
     [0.0, 0.0, 1.0],
     [0.0, -0.70710678118654757, -0.70710678118654757],
     [0.0, -0.70710678118654757, 0.70710678118654757],
     [0.0, 0.70710678118654757, -0.70710678118654757],
     [0.0, 0.70710678118654757, 0.70710678118654757],
     [-0.70710678118654757, 0.0, -0.70710678118654757],
     [0.70710678118654757, 0.0, -0.70710678118654757],
     [-0.70710678118654757, 0.0, 0.70710678118654757],
     [0.70710678118654757, 0.0, 0.70710678118654757],
     [-0.70710678118654757, -0.70710678118654757, 0.0],
     [-0.70710678118654757, 0.70710678118654757, 0.0],
     [0.70710678118654757, -0.70710678118654757, 0.0],
     [0.70710678118654757, 0.70710678118654757, 0.0],
     [-0.57735026918962573, -0.57735026918962573, -0.57735026918962573],
     [-0.57735026918962573, -0.57735026918962573, 0.57735026918962573],
     [-0.57735026918962573, 0.57735026918962573, -0.57735026918962573],
     [-0.57735026918962573, 0.57735026918962573, 0.57735026918962573],
     [0.57735026918962573, -0.57735026918962573, -0.57735026918962573],
     [0.57735026918962573, -0.57735026918962573, 0.57735026918962573],
     [0.57735026918962573, 0.57735026918962573, -0.57735026918962573],
     [0.57735026918962573, 0.57735026918962573, 0.57735026918962573],
     [-0.90453403373329089, -0.30151134457776357, -0.30151134457776357],
     [-0.90453403373329089, -0.30151134457776357, 0.30151134457776357],
     [-0.90453403373329089, 0.30151134457776357, -0.30151134457776357],
     [-0.90453403373329089, 0.30151134457776357, 0.30151134457776357],
     [0.90453403373329089, -0.30151134457776357, -0.30151134457776357],
     [0.90453403373329089, -0.30151134457776357, 0.30151134457776357],
     [0.90453403373329089, 0.30151134457776357, -0.30151134457776357],
     [0.90453403373329089, 0.30151134457776357, 0.30151134457776357],
     [-0.30151134457776357, -0.90453403373329089, -0.30151134457776357],
     [0.30151134457776357, -0.90453403373329089, -0.30151134457776357],
     [-0.30151134457776357, -0.90453403373329089, 0.30151134457776357],
     [0.30151134457776357, -0.90453403373329089, 0.30151134457776357],
     [-0.30151134457776357, 0.90453403373329089, -0.30151134457776357],
     [0.30151134457776357, 0.90453403373329089, -0.30151134457776357],
     [-0.30151134457776357, 0.90453403373329089, 0.30151134457776357],
     [0.30151134457776357, 0.90453403373329089, 0.30151134457776357],
     [-0.30151134457776357, -0.30151134457776357, -0.90453403373329089],
     [-0.30151134457776357, 0.30151134457776357, -0.90453403373329089],
     [0.30151134457776357, -0.30151134457776357, -0.90453403373329089],
     [0.30151134457776357, 0.30151134457776357, -0.90453403373329089],
     [-0.30151134457776357, -0.30151134457776357, 0.90453403373329089],
     [-0.30151134457776357, 0.30151134457776357, 0.90453403373329089],
     [0.30151134457776357, -0.30151134457776357, 0.90453403373329089],
     [0.30151134457776357, 0.30151134457776357, 0.90453403373329089]])

# 110 Lebedev quadrature points:
SPHERE_POINTS_fine = np.array(
    [[1.000000000000000, 0.000000000000000, 0.000000000000000],
     [-1.000000000000000, 0.000000000000000, 0.000000000000000],
     [0.000000000000000, 1.000000000000000, 0.000000000000000],
     [0.000000000000000, -1.000000000000000, 0.000000000000000],
     [0.000000000000000, 0.000000000000000, 1.000000000000000],
     [0.000000000000000, 0.000000000000000, -1.000000000000000],
     [0.577350269189626, 0.577350269189626, 0.577350269189626],
     [-0.577350269189626, 0.577350269189626, 0.577350269189626],
     [0.577350269189626, -0.577350269189626, 0.577350269189626],
     [0.577350269189626, 0.577350269189626, -0.577350269189626],
     [-0.577350269189626, -0.577350269189626, 0.577350269189626],
     [0.577350269189626, -0.577350269189626, -0.577350269189626],
     [-0.577350269189626, 0.577350269189626, -0.577350269189626],
     [-0.577350269189626, -0.577350269189626, -0.577350269189626],
     [0.185115635344736, 0.185115635344736, 0.965124035086594],
     [-0.185115635344736, 0.185115635344736, 0.965124035086594],
     [0.185115635344736, -0.185115635344736, 0.965124035086594],
     [0.185115635344736, 0.185115635344736, -0.965124035086594],
     [-0.185115635344736, -0.185115635344736, 0.965124035086594],
     [-0.185115635344736, 0.185115635344736, -0.965124035086594],
     [0.185115635344736, -0.185115635344736, -0.965124035086594],
     [-0.185115635344736, -0.185115635344736, -0.965124035086594],
     [-0.185115635344736, 0.965124035086594, 0.185115635344736],
     [0.185115635344736, -0.965124035086594, 0.185115635344736],
     [0.185115635344736, 0.965124035086594, -0.185115635344736],
     [-0.185115635344736, -0.965124035086594, 0.185115635344736],
     [-0.185115635344736, 0.965124035086594, -0.185115635344736],
     [0.185115635344736, -0.965124035086594, -0.185115635344736],
     [-0.185115635344736, -0.965124035086594, -0.185115635344736],
     [0.185115635344736, 0.965124035086594, 0.185115635344736],
     [0.965124035086594, 0.185115635344736, 0.185115635344736],
     [-0.965124035086594, 0.185115635344736, 0.185115635344736],
     [0.965124035086594, -0.185115635344736, 0.185115635344736],
     [0.965124035086594, 0.185115635344736, -0.185115635344736],
     [-0.965124035086594, -0.185115635344736, 0.185115635344736],
     [-0.965124035086594, 0.185115635344736, -0.185115635344736],
     [0.965124035086594, -0.185115635344736, -0.185115635344736],
     [-0.965124035086594, -0.185115635344736, -0.185115635344736],
     [0.690421048382292, 0.690421048382292, 0.215957291845848],
     [-0.690421048382292, 0.690421048382292, 0.215957291845848],
     [0.690421048382292, -0.690421048382292, 0.215957291845848],
     [0.690421048382292, 0.690421048382292, -0.215957291845848],
     [-0.690421048382292, -0.690421048382292, 0.215957291845848],
     [-0.690421048382292, 0.690421048382292, -0.215957291845848],
     [0.690421048382292, -0.690421048382292, -0.215957291845848],
     [-0.690421048382292, -0.690421048382292, -0.215957291845848],
     [-0.690421048382292, 0.215957291845848, 0.690421048382292],
     [0.690421048382292, -0.215957291845848, 0.690421048382292],
     [0.690421048382292, 0.215957291845848, -0.690421048382292],
     [-0.690421048382292, -0.215957291845848, 0.690421048382292],
     [-0.690421048382292, 0.215957291845848, -0.690421048382292],
     [0.690421048382292, -0.215957291845848, -0.690421048382292],
     [-0.690421048382292, -0.215957291845848, -0.690421048382292],
     [0.690421048382292, 0.215957291845848, 0.690421048382292],
     [0.215957291845848, 0.690421048382292, 0.690421048382292],
     [-0.215957291845848, 0.690421048382292, 0.690421048382292],
     [0.215957291845848, -0.690421048382292, 0.690421048382292],
     [0.215957291845848, 0.690421048382292, -0.690421048382292],
     [-0.215957291845848, -0.690421048382292, 0.690421048382292],
     [-0.215957291845848, 0.690421048382292, -0.690421048382292],
     [0.215957291845848, -0.690421048382292, -0.690421048382292],
     [-0.215957291845848, -0.690421048382292, -0.690421048382292],
     [0.395689473055942, 0.395689473055942, 0.828769981252592],
     [-0.395689473055942, 0.395689473055942, 0.828769981252592],
     [0.395689473055942, -0.395689473055942, 0.828769981252592],
     [0.395689473055942, 0.395689473055942, -0.828769981252592],
     [-0.395689473055942, -0.395689473055942, 0.828769981252592],
     [-0.395689473055942, 0.395689473055942, -0.828769981252592],
     [0.395689473055942, -0.395689473055942, -0.828769981252592],
     [-0.395689473055942, -0.395689473055942, -0.828769981252592],
     [-0.395689473055942, 0.828769981252592, 0.395689473055942],
     [0.395689473055942, -0.828769981252592, 0.395689473055942],
     [0.395689473055942, 0.828769981252592, -0.395689473055942],
     [-0.395689473055942, -0.828769981252592, 0.395689473055942],
     [-0.395689473055942, 0.828769981252592, -0.395689473055942],
     [0.395689473055942, -0.828769981252592, -0.395689473055942],
     [-0.395689473055942, -0.828769981252592, -0.395689473055942],
     [0.395689473055942, 0.828769981252592, 0.395689473055942],
     [0.828769981252592, 0.395689473055942, 0.395689473055942],
     [-0.828769981252592, 0.395689473055942, 0.395689473055942],
     [0.828769981252592, -0.395689473055942, 0.395689473055942],
     [0.828769981252592, 0.395689473055942, -0.395689473055942],
     [-0.828769981252592, -0.395689473055942, 0.395689473055942],
     [-0.828769981252592, 0.395689473055942, -0.395689473055942],
     [0.828769981252592, -0.395689473055942, -0.395689473055942],
     [-0.828769981252592, -0.395689473055942, -0.395689473055942],
     [0.478369028812150, 0.878158910604066, 0.000000000000000],
     [-0.478369028812150, 0.878158910604066, 0.000000000000000],
     [0.478369028812150, -0.878158910604066, 0.000000000000000],
     [-0.478369028812150, -0.878158910604066, 0.000000000000000],
     [0.878158910604066, 0.478369028812150, 0.000000000000000],
     [-0.878158910604066, 0.478369028812150, 0.000000000000000],
     [0.878158910604066, -0.478369028812150, 0.000000000000000],
     [-0.878158910604066, -0.478369028812150, 0.000000000000000],
     [0.478369028812150, 0.000000000000000, 0.878158910604066],
     [-0.478369028812150, 0.000000000000000, 0.878158910604066],
     [0.478369028812150, 0.000000000000000, -0.878158910604066],
     [-0.478369028812150, 0.000000000000000, -0.878158910604066],
     [0.878158910604066, 0.000000000000000, 0.478369028812150],
     [-0.878158910604066, 0.000000000000000, 0.478369028812150],
     [0.878158910604066, 0.000000000000000, -0.478369028812150],
     [-0.878158910604066, 0.000000000000000, -0.478369028812150],
     [0.000000000000000, 0.478369028812150, 0.878158910604066],
     [0.000000000000000, -0.478369028812150, 0.878158910604066],
     [0.000000000000000, 0.478369028812150, -0.878158910604066],
     [0.000000000000000, -0.478369028812150, -0.878158910604066],
     [0.000000000000000, 0.878158910604066, 0.478369028812150],
     [0.000000000000000, -0.878158910604066, 0.478369028812150],
     [0.000000000000000, 0.878158910604066, -0.478369028812150],
     [0.000000000000000, -0.878158910604066, -0.478369028812150]])


@cache
def triangulate_sphere(coarse: bool) -> tuple[np.ndarray, np.ndarray]:
    points = SPHERE_POINTS if coarse else SPHERE_POINTS_fine
    hull = ConvexHull(points)
    tri_tv = hull.simplices.copy()

    # Make sure surface normals are pointing out from the sphere surface
    tri_tvc = points[tri_tv]
    n_tc = np.cross(tri_tvc[:, 1, :] - tri_tvc[:, 0, :],
                    tri_tvc[:, 2, :] - tri_tvc[:, 0, :])
    flip_n = np.sum(n_tc * tri_tvc[:, 0, :], axis=1) < 0
    tri_tv[flip_n, 1], tri_tv[flip_n, 2] = tri_tv[flip_n, 2], tri_tv[flip_n, 1]

    return points, tri_tv


@cache
def color(Z: int) -> str:
    a, b, c = (jmol_colors[Z] * 255).astype(int)
    return f'rgb({a},{b},{c})'


if __name__ == '__main__':
    atoms = read_atoms(sys.argv[1])
    plot_atoms(atoms).show()
