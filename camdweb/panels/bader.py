import json

from camdweb.panels.panel import Panel, PanelData
from camdweb.material import Material
from camdweb.html import table


HTML = """
<div class="row">
{table}
</div>
"""


class BaderPanel(Panel):
    datafiles = ['bader.json']
    title = 'Bader charges'
    info = """
    The Bader charge analysis ascribes a net charge to an atom by partitioning
    the electron density according to its zero-flux surfaces. Note that there
    is no unique/correct way to ascribe a charge to an atom based on a DFT
    calculation.

    <br><br>Relevant articles:

    <br><a href=https://doi.org/10.1088/0953-8984/21/8/084204>
    W. Tang et al.
    A grid-based Bader analysis algorithm without lattice bias.
    J. Phys.: Condens. Matter 21 084204 (2009).
    </a>
    """

    def get_data(self,
                 material: Material) -> PanelData:
        path = material.folder / self.datafiles[0]
        charges = json.loads(path.read_text())['charges']
        html = HTML.format(
            table=table(['Atom No.', 'Chemical symbol', 'Charges [|e|]'],
                        [(n, s, f'{c:.2f}') for n, (s, c)
                         in enumerate(zip(material.atoms.symbols,
                                          charges))]))
        return PanelData(html,
                         title=self.title,
                         info=self.info)
