"""Panel base class."""
from __future__ import annotations

import abc
from bottle import template
from typing import TYPE_CHECKING, Callable, Iterable
from dataclasses import dataclass, field
from camdweb import ColVal

if TYPE_CHECKING:
    from camdweb.material import Material


class SkipPanel(Exception):
    """Don't show this panel."""


@dataclass
class PanelData:
    html: str
    title: str
    info: str = ''
    script: str = ''
    expanded: bool = False
    subpanels: list[PanelData] = field(default_factory=list)

    def determine_panel_type(self, super_pid: int | None = None) -> str:
        if len(self.subpanels) == 0 and super_pid is None:
            return 'Panel'
        elif super_pid is None:
            return 'SuperPanel'
        elif super_pid is not None and len(self.subpanels) == 0:
            return 'SubPanel'
        else:
            raise ValueError('Sub-panels do not support sub-panels.')

    def build_sidebar(self, super_pid: int | None = None):
        panel_case = self.determine_panel_type(super_pid=super_pid)

        sidebar_html = template(
            'sidebar_element.html',
            case=panel_case,
            title=self.title,
            pid=id(self),
            ppid=super_pid)

        html_strings = [sidebar_html]
        for subpanel in self.subpanels:
            html_strings.append(subpanel.build_sidebar(super_pid=id(self)))
        return '\n'.join(html_strings)

    def build_body(self, super_pid: int | None = None):
        panel_case = self.determine_panel_type(super_pid=super_pid)
        html_strings = [self.html]

        for subpanel in self.subpanels:
            html_strings.append(subpanel.build_body(super_pid=id(self)))
        html = '\n'.join(html_strings)

        body_html = template(
            'panel_element.html',
            case=panel_case,
            title=self.title,
            html=html,
            info=self.info,
            expanded=self.expanded,
            pid=id(self),
            ppid=super_pid)

        return body_html


class Panel(abc.ABC):
    title: str = 'Unnamed'
    info = ''
    datafiles: list[str] = []
    column_descriptions: dict[str, str] = {}
    html_formatters: dict[str, Callable[..., str]] = {}
    callbacks: dict[str, Callable[[Material, int], str]] = {}
    subpanels: list[Panel] = []

    def replace_shortname_ref(self, shortname: str):
        from camdweb.references import LINKED_PAPERS
        self.info = self.info.replace(shortname, LINKED_PAPERS[shortname])

    def construct_superpanels(self,
                              material: Material
                              ) -> PanelData:
        """Automatic return SubPanel PanelData when only one subpanel is
        found."""
        subpanels = self.add_subpanels(material=material)
        if len(subpanels) == 1:
            return subpanels[0]

        return PanelData(html='',
                         title=self.title,
                         info=self.info,
                         subpanels=subpanels)

    def create_panel_data(self,
                          material: Material) -> PanelData:
        if not all((material.folder / datafile).is_file()
                   for datafile in self.datafiles):
            raise SkipPanel
        data = self.get_data(material)
        if not data.info and self.info:
            data.info = self.info
        return data

    @abc.abstractmethod
    def get_data(self,
                 material: Material) -> PanelData:
        raise NotImplementedError

    def update_material(self, material: Material) -> None:
        pass

    def update_column_descriptions(self,
                                   column_descriptions: dict[str, str]
                                   ) -> None:
        column_descriptions.update(self.column_descriptions)
        self.column_descriptions = column_descriptions
        for panel in self.subpanels:
            panel.update_column_descriptions(column_descriptions)

    def update_html_formatters(self,
                               html_formatters: dict[
                                   str,
                                   Callable[..., str]]
                               ) -> None:
        html_formatters.update(self.html_formatters)
        self.html_formatters = html_formatters
        for panel in self.subpanels:
            panel.update_html_formatters(html_formatters)

    def add_subpanels(self,
                      material: Material):
        if not hasattr(self, 'subpanels'):
            raise AttributeError('Panel does not contain subpanels')

        panels = []
        for panel in self.subpanels:
            try:
                panels.append(panel.create_panel_data(material=material))
            except SkipPanel:
                pass

        if len(panels) == 0:
            raise SkipPanel

        return panels

    def table_rows(self,
                   material: Material,
                   names: Iterable[str],
                   updated_names: dict[str, str] = {}
                   ) -> list[list[str]]:
        rows = []
        for name in names:
            value = material.columns.get(name)
            if value is not None:
                formatter = self.html_formatters.get(name, default_formatter)
                # check if we have an updated name for this value
                if name in updated_names:
                    field_name = updated_names[name]
                else:
                    field_name = self.column_descriptions.get(name, name)
                rows.append([field_name,
                             formatter(value, link=True)])
        return rows

    def table_dict(self, dct: dict, names: Iterable[str]) -> list[list[str]]:
        rows = []
        for name in names:
            value = dct.get(name)
            if value is not None:
                formatter = self.html_formatters.get(name, default_formatter)
                rows.append([self.column_descriptions.get(name, name),
                             formatter(value, link=True)])
        return rows


def default_formatter(value: ColVal, link: bool = False) -> str:
    """Default formatting of str, float, bool and int.

    >>> default_formatter('abc')
    'abc'
    >>> default_formatter(1 / 3)
    '0.333'
    >>> default_formatter(False)
    'No'
    """
    if isinstance(value, str):
        return value
    if isinstance(value, float):
        return f'{value:.3f}'
    if isinstance(value, bool):
        return 'Yes' if value else 'No'
    return str(value)
