from camdweb.panels.panel import Panel, PanelData
from camdweb.material import Material
from camdweb.html import table


HTML = """
<div class="row row-cols-1 row-cols-lg-2">
  <div class="col">
    {col1}
  </div>
  <div class="col">
    {col2}
  </div>
</div>
"""


class CrystalStructurePanel(Panel):
    title = 'Crystal structure'
    info = """
    The layer group of the monolayer and the space group of the bulk structure
    obtained by stacking the monolayer in AA order (see Fu et al.). The point
    group and the Bravais lattice type are also listed. The thickness of the
    material is defined as the vertical distance between the outermost atoms of
    the structure.

    <br><br>Relevant articles:

    <br>Fu2024
    """

    def get_data(self, material: Material) -> PanelData:
        col1 = self.create_column_one(material)
        col2 = self.create_column_two(material)
        self.replace_shortname_ref('Fu2024')
        return PanelData(
            HTML.format(col1=col1, col2=col2),
            title=self.title,
            info=self.info
        )

    def create_column_one(self, material: Material) -> str:
        return table(
            ['Symmetries', ''],
            self.table_rows(material, [
                'crystal_system', 'bravais_type', 'lgnum', 'layergroup',
                'number', 'international', 'pointgroup',
                'has_inversion_symmetry']))

    def create_column_two(self, material: Material) -> str:
        return table(
            ['Structure data', ''],
            self.table_rows(
                material,
                ['crystal_type', 'formula', 'stoichiometry', 'natoms',
                 'area', 'volume', 'thickness']))
