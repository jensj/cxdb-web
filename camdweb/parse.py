import functools
import re
import sys
from typing import Sequence
from pprint import pp
from ase.formula import Formula

from camdweb.filter import Index

CMP = {'<', '<=', '=', '>=', '>', '!='}

# Pseudo typehint for Tree:
#  (),
#  ('and', Tree, Tree),
#  ('or', Tree, Tree),
#  ('not', Tree),
#  ('formula', dict[str, int]),
#  ('has', str),
#  ('symbol', str, str, int),
#  ('key', str, str, bool | int | float | str])
Tree = tuple


def tokenize(s: str) -> list[bool | int | float | str | dict]:
    """...

    >>> tokenize('H2')
    [{'H': 2}]
    >>> tokenize('H2,xc=PBE')
    [{'H': 2}, ',', 'xc', '=', 'PBE']
    >>> tokenize('H2,Ag=0')
    [{'H': 2}, ',', {'Ag': 1}, '=', 0]
    >>> tokenize('H>7,(Ag=1|N=1)')
    [{'H': 1}, '>', 7, ',', '(', {'Ag': 1}, '=', 1, '|', {'N': 1}, '=', 1, ')']
    """
    s = s.replace(' ', '')
    tokens: list[bool | int | float | str | dict] = []
    while s:
        match = re.search(r'>=|<=|!=|[<>=,~\|\(\)]', s)
        rhs = tokens and tokens[-1] in CMP  # right-hand side
        if match is None:
            tokens.append(str2obj(s, rhs=rhs))
            break
        i1, i2 = match.span()
        t = s[:i1]
        if t:
            tokens.append(str2obj(t, rhs=rhs))
        tokens.append(match[0])
        s = s[i2:]
    return [token for token in tokens if token != '']


def tokens2tree(tokens: Sequence[bool | int | float | str | dict | Tree]
                ) -> Tree:
    """Convert tokens to tree structure.

    >>> tokens2tree([{'H': 2}])
    ('formula', {'H': 2})
    >>> tokens2tree([{'H': 2}, ',', 'xc', '=', 'PBE'])
    ('and', ('formula', {'H': 2}), ('key', 'xc', '=', 'PBE'))
    >>> tokens2tree([{'H': 2}, ',', {'Ag': 1}, '=', 0])
    ('and', ('formula', {'H': 2}), ('symbol', 'Ag', '=', 0))
    >>> t = tokens2tree([{'H': 1}, '>', 7, ',',
    ...                  '(', {'Ag': 1}, '=', 1, '|', {'Cu': 1}, '=', 1, ')'])
    >>> pp(t)
    ('and',
     ('symbol', 'H', '>', 7),
     ('or', ('symbol', 'Ag', '=', 1), ('symbol', 'Cu', '=', 1)))
    """
    assert isinstance(tokens, list)

    # Parenthesis:
    try:
        i1 = tokens.index('(')
    except ValueError:
        pass
    else:
        try:
            i2 = len(tokens) - 1 - tokens[::-1].index(')')
        except ValueError:
            raise SyntaxError('Mismatched parenthesis')
        if i2 <= i1 + 1:
            raise SyntaxError('Mismatched parenthesis')
        return tokens2tree(tokens[:i1] +
                           [tokens2tree(tokens[i1 + 1:i2])] +
                           tokens[i2 + 1:])

    # "and":
    try:
        i = tokens.index(',')
    except ValueError:
        pass
    else:
        return ('and', tokens2tree(tokens[:i]), tokens2tree(tokens[i + 1:]))

    # "or":
    try:
        i = tokens.index('|')
    except ValueError:
        pass
    else:
        return ('or', tokens2tree(tokens[:i]), tokens2tree(tokens[i + 1:]))

    if len(tokens) == 1 and isinstance(tokens[0], tuple):
        # Already done ...
        return tokens[0]

    if tokens[0] == '~':
        return ('not', tokens2tree(tokens[1:]))

    if len(tokens) == 1:
        if isinstance(tokens[0], dict):
            return ('formula', tokens[0])
        assert isinstance(tokens[0], str)
        return ('has', tokens[0])

    if len(tokens) != 3:
        raise SyntaxError('Bad filter string')
    thing, cmp, value = tokens
    if not (isinstance(cmp, str) and cmp in CMP):
        raise SyntaxError('Bad filter string')
    if isinstance(thing, dict):
        ((symbol, n),) = thing.items()
        assert n == 1
        assert isinstance(value, int)
        return ('symbol', symbol, cmp, value)

    assert isinstance(thing, str)
    assert isinstance(value, (bool, int, float, str))
    return ('key', thing, cmp, value)


@functools.lru_cache
def parse(s: str) -> Tree:
    if not s:
        return ()
    return tokens2tree(tokenize(s))


def select(tree: Tree, index: Index) -> set[int]:
    """Convert filter string to Python function.

    >>> from camdweb.filter import Index
    >>> i = Index([({'H': 2}, {'xc': 'PBE'})])
    Rows: 1
    Strings: 1
    Integers: 1
    Bools: 0
    Floats: 0
    Int-floats: 0
    >>> select(parse('H2,xc=PBE'), i)
    {0}
    >>> i = Index([({'H': 2}, {'gap': 10.0}),
    ...            ({'Si': 2}, {'gap': 1.1})])
    Rows: 2
    Strings: 0
    Integers: 2
    Bools: 0
    Floats: 1
    Int-floats: 0
    >>> select(parse('gap > 5.0'), i)
    {0}
    """
    if not tree:
        return index.ids
    kind, *extra = tree
    if kind == 'formula':
        (count,) = extra
        return index.formula(count)
    if kind == 'and':
        t1, t2 = extra
        return select(t1, index) & select(t2, index)
    if kind == 'or':
        t1, t2 = extra
        return select(t1, index) | select(t2, index)
    if kind == 'not':
        t1, = extra
        return index.ids - select(t1, index)
    if kind == 'has':
        t1, = extra
        return index.has(t1)
    symbol, cmp, n = extra
    return {int(i) for i in index.key(symbol, cmp, n)}


def str2obj(s: str, rhs=True) -> bool | int | float | str | dict:
    """Convert string to object.

    >>> str2obj('Hello')
    'Hello'
    >>> str2obj('7.4')
    7.4
    >>> str2obj('H2O')
    'H2O'
    >>> str2obj('H2O', rhs=False)
    {'H': 2, 'O': 1}
    """
    x = {'True': True, 'False': False}.get(s)
    if x is not None:
        return x
    for type in [int, float]:
        try:
            return type(s)
        except ValueError:
            pass
    if not rhs:
        try:
            return Formula(s, strict=True)._count
        except ValueError:
            pass
    return s


if __name__ == '__main__':
    pp(tokens2tree(tokenize(' '.join(sys.argv[1:]))))
