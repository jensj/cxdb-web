"""QPOD web-app.

This module has code to convert ~cmr/WIP/defects/tree/ folders and friends
(see PATTERNS variable below) to canonical tree layout.

Also contains simple web-app that can run off the tree of folders.

Goal is to have the code decoupled from ASE, GPAW and ASR.
"""
from __future__ import annotations

import argparse
import json
from pathlib import Path

import rich.progress as progress

from camdweb.materials import Material, Materials
from camdweb.html import SearchFilter, Select, table, image, Input, Range
from camdweb.panels.atoms import AtomsPanel
from camdweb.panels.panel import Panel, PanelData
from camdweb.qpod.charge_neutrality import ChargeNeutralitySuperpanel
from camdweb.qpod.slater_janak import SlaterJanakPanel
from camdweb.web import CAMDApp
from camdweb.utils import c2db

HTML = """
  <div class="row row-cols-1 row-cols-lg-2">
    <div class="col">
      {column1}
    </div>
    <div class="col">
      {column2}
    </div>
  </div>
"""


class QPODAtomsPanel(AtomsPanel):
    title = 'Summary'

    def __init__(self) -> None:
        super().__init__()
        self.callbacks = {'atoms': self.plot}

    def get_data(self,
                 material: Material) -> PanelData:
        col1 = self.create_column_one(material)
        col2, script = self.create_column_two(material)

        host = material.columns['host_name']
        defect = material.columns['defect_name']
        charge = material.columns['charge_state']

        return PanelData(HTML.format(column1=col1, column2=col2),
                         title=f'{defect} in {host} (charge {charge})',
                         script=script,
                         expanded=True)

    def create_column_one(self,
                          material: Material) -> str:
        html1 = table(['Host crystal info', ''],
                      self.table_rows(
                          material,
                          ['host_name', 'host_crystal', 'host_layergroup',
                           'host_lgnum', 'host_gap_pbe', 'host_gap_hse',
                           'host_uid']))
        html2 = table(['Defect properties', ''],
                      self.table_rows(material,
                                      ['r_nn', 'defect_pointgroup',
                                       'magnetic']))
        return '\n'.join([html1, html2])


class QPODApp(CAMDApp):
    """QPOD app with /row/<olduid> endpoint."""

    title = 'QPOD'
    logo = image('qpod-logo.png', alt='QPOD-logo')
    links = [
        ('CMR', 'https://cmr.fysik.dtu.dk'),
        ('About', 'https://cmr.fysik.dtu.dk/qpod/qpod.html')]

    def __init__(self,
                 materials: Materials,
                 initial_columns: list[str],
                 root: Path | None = None):
        super().__init__(materials,
                         initial_columns=initial_columns,
                         initial_filter_string='charge_state=0',
                         root=root)


def main(argv: list[str] | None = None) -> CAMDApp:
    """Create QPOD app."""
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'pattern', nargs='*', default=['*/*/charge*'],
        help='Glob-pattern: materials/<pattern>.  Default is "*/*/charge*".')
    args = parser.parse_args(argv)

    root = Path()
    folders = []
    for pattern in args.pattern:
        for p in (root / 'materials').glob(pattern):
            folders.append(p)

    mlist: list[Material] = []
    with progress.Progress() as pb:
        pid = pb.add_task('Reading materials:', total=len(folders))
        for f in folders:
            data_file = f / 'data.json'
            if data_file.is_file():
                with open(data_file, 'r') as json_file:
                    data = json.load(json_file)
                    uid = data.get('uid')
                    material = Material.from_json(f / 'structure.json', uid)
                    material.columns.update(data)
                    mlist.append(material)
            else:
                print('Warning: '
                      f'data.json file not found or not readable in {f}')
            pb.advance(pid)

    panels: list[Panel] = [QPODAtomsPanel(),
                           SlaterJanakPanel(),
                           ChargeNeutralitySuperpanel()]
    materials = Materials(mlist, panels)
    materials.html_formatters.update(host_uid=c2db)
    initial_columns = ['host_name', 'defect_name', 'charge_state',
                       'defect_pointgroup']
    materials.column_descriptions.update(
        host_name='Host material',
        defect_name='Defect',
        charge_state='Charge State',
        r_nn='Defect-defect distance [Å]',
        host_gap_pbe='Band gap (PBE) [eV]',
        host_gap_hse='Band gap (HSE) [eV]',
        host_spacegroup="Host space group",  # not shown
        host_layergroup="Layer group",
        host_lgnum="Layer group number",
        host_pointgroup="Point group",
        host_uid="Host unique ID (C2DB)",
        defect_pointgroup='Point group')

    app = QPODApp(materials,
                  initial_columns,
                  root)
    app.form_parts[0] = SearchFilter(placeholder="Example: 'MoS2'")
    app.form_parts += [
        Input('Host material:', 'host_name',
              placeholder='MoS2, BN, ...'),
        Input('Defect:', 'defect_name',
              placeholder='v_Mo, Mo_S, N_B, ...'),
        Select('Charge state:', 'charge_state',
               [str(i) for i in range(-3, 4)],
               default='0'),
        Range('Host band gap [eV]:', 'host_gap_pbe',
              nonnegative=True)]

    return app


def create_app():  # pragma: no cover
    """Create the WSGI app."""
    app = main()
    return app.app


def check_all():  # pragma: no cover
    """Generate png-files."""
    qpod = main()
    for material in qpod.materials:
        print(material.uid)
        qpod.material_page(material.uid)


if __name__ == '__main__':
    main().app.run(host='0.0.0.0', port=8083, debug=True)
