"""Copy QPOD files from ASR-layout to uniform tree structure.

Tree structure::

   <host material>/<defects>/<charge>/
   <host material>/<pristine>/

Example::

   MoS2/v_Mo/charge_1/
   WS2/pristine_sc/
   ...

Build tree like this::

    $ cd /tmp
    $ mkdir tree
    $ cd tree

Generate uid_translation.json file::
    $ python -m camdweb.c2db.uid_translation

Copy files to tree structure::
    $ python -m camdweb.qpod.copy <root-dir> <pattern> <pattern> ...

TODO: Add handling of UIDs due to duplicate structures from different projects.
"""
import json
import shutil
import sys
from collections import defaultdict
from pathlib import Path
from typing import Any
import subprocess

import rich.progress as progress
from camdweb.c2db.asr_panel import read_result_file
from camdweb.utils import all_dirs, read_atoms
from camdweb.c2db.uid_translation import convert_uid
from camdweb.atomsdata import AtomsData

ROOT = Path('/home/niflheim2/cmr/WIP/defects/rerun-qpod')
RESULT_FILES = [
    'magstate',
    'magnetic_anisotropy',
    'structureinfo',
    'gs',
    'gs@calculate']
CHARGE_RESULT_FILES = ['defectinfo', 'sj_analyze']
PATTERNS = [
    'tree-*/A*/*/*/defects.*/charge_*',
    'tree-*/A*/*/*/defects.pristine*'
]


def make_uid_name(dir: Path) -> tuple[str, str]:
    defectinfo_path = dir / 'results-asr.defectinfo.json'
    defectinfo = read_result_file(defectinfo_path)
    host_olduid = defectinfo['host_uid']

    uid_map_path = Path('uid_translation.json')

    if not uid_map_path.exists():
        user_input = input(
            "uid_translation.json not found. Do you want to run 'python -m "
            "camdweb.c2db.uid_translation' to generate it? (yes/no): "
        )
        if user_input.lower() in ['yes', 'y']:
            subprocess.run(['python', '-m', 'camdweb.c2db.uid_translation'],
                           check=True)
        else:
            raise FileNotFoundError(
                "uid_translation.json not found. Please run 'python -m "
                "camdweb.c2db.uid_translation' first."
            )

    with uid_map_path.open('r') as f:
        uid_map = json.load(f)
    host_uid = convert_uid(host_olduid, uid_map)

    defect_name = str(defectinfo['defect_name'])
    charge_str = str(defectinfo['charge_state'])

    # charge_str is a string like '(charge 3)', '(charge -1)', etc.
    # We want to extract the number and add a '+' sign if it's positive.
    charge_state = charge_str.split(' ')[1].rstrip(')') or ''
    if charge_state and int(charge_state) > 0:
        charge_state = f"+{charge_state}"

    return f'{host_uid}.{defect_name}.{charge_state}', host_uid


def create_qpod_uids(root: Path = ROOT,
                     patterns: list[str] = PATTERNS) -> None:
    names: defaultdict[str, int] = defaultdict(int)
    new = []
    # Filter out patterns containing 'pristine'
    patterns = [pattern for pattern in patterns if 'pristine' not in pattern]

    for dir in all_dirs(root, patterns):
        gpw = dir / 'gs.gpw'
        if not gpw.is_file():
            # print('Skipping - no gpw file in:', dir)
            continue

        uid = None
        olduid = None
        try:
            uid_data = json.loads((dir / 'uid.json').read_text())
        except FileNotFoundError:
            pass
        else:
            uid = uid_data['uid']
            olduid = uid_data.get('olduid')

        if olduid is None:
            fp = dir / 'results-asr.database.material_fingerprint.json'
            try:
                olduid = read_result_file(fp)['uid']
            except FileNotFoundError:
                pass

        name, _ = make_uid_name(dir)
        if uid:
            number = int(uid.split('.')[-1])
            names[name] = max(names[name], number)
        else:
            new.append((name, dir, olduid))

    # This handles polytpes with the same name
    # and adds a number to the end of the uid.

    # TODO: How to distinguish between same structure
    # from different projects? -- TO BE DISCUSSED
    # Simplest thing would be to add a manual check.
    for name, dir, olduid in sorted(new):
        number = names[name] + 1
        uid = f'{name}.{number}'
        names[name] = number
        uid_data = {'uid': uid}
        if olduid:
            uid_data['olduid'] = olduid
        (dir / 'uid.json').write_text(json.dumps(uid_data, indent=2))


def copy_materials(root: Path, patterns: list[str]) -> None:
    dirs = all_dirs(root, patterns)
    print('Folders:', len(dirs))

    with progress.Progress() as pb:
        pid = pb.add_task('Copying materials:', total=len(dirs))
        for dir in dirs:
            copy_material(dir)
            pb.advance(pid)

    logo = Path('qpod-logo.png')
    if not logo.exists() and 'tmp-qpodtree' not in str(root):
        shutil.copy(Path(__file__).parent / 'qpod-logo.png', logo)

    # copy the webpage validation workflow script
    shutil.copyfile(Path(__file__).parent / 'wf.py', Path('wf.py'))


def copy_material(dir: Path) -> None:
    gpw = dir / 'gs.gpw'
    if not gpw.is_file() and 'tmp-qpodtree' not in str(dir):
        return

    parts = str(dir).split('/')
    defects_index = next((i for i, part in enumerate(parts)
                          if 'defects.' in part), None)

    if defects_index is None or defects_index + 1 > len(parts):
        return

    # Handle pristine and defect folders
    material_folder = Path('materials', parts[defects_index - 1].split('-')[0])

    # Avoiding modifying global variable
    result_files = RESULT_FILES.copy()

    if 'defects.pristine' in str(dir):
        subfolder = parts[defects_index].split('.')[-1]
        folder = Path(material_folder) / subfolder
        result_files.extend(['defectinfo'])
        result_files.extend(['charge_neutrality'])
    else:
        defects_parts = parts[defects_index].split('.')
        subfolder = defects_parts[-1]
        charge_folder = parts[defects_index + 1]
        folder = Path(material_folder) / subfolder / charge_folder
        result_files.extend(['database.material_fingerprint'])

        if (dir / 'results-asr.defect_symmetry.json').is_file():
            result_files.extend(['get_wfs', 'defect_symmetry'])

        if charge_folder == 'charge_0':
            result_files.extend(CHARGE_RESULT_FILES)

    def rrf(name: str) -> dict:
        return read_result_file(dir / f'results-asr.{name}.json')

    data: dict[str, Any] = {}

    if 'defects.pristine' not in str(dir):
        uid_file_path = dir / 'uid.json'
        if not uid_file_path.is_file():
            raise FileNotFoundError(f"Error: '{uid_file_path}' not found.")

        data['uid'] = json.loads((dir / 'uid.json').read_text()).get('uid')
        data['qpod_olduid'] = json.loads(
            (dir / 'uid.json').read_text()).get('olduid')
        _, data['host_uid'] = make_uid_name(dir)
        try:
            defectinfo = rrf('defectinfo')
            # Maybe take data from json directly at some later point
            data['host_olduid'] = defectinfo['host_uid']
            data['host_name'] = str(defectinfo['host_name'])
            data['defect_name'] = str(defectinfo['defect_name'])
            charge_str = str(defectinfo['charge_state'])
            data['charge_state'] = charge_str.split(' ')[1].rstrip(')') or ''
            data['host_gap_pbe'] = defectinfo['host_gap_pbe']
            data['host_gap_hse'] = defectinfo['host_gap_hse']
            data['host_hof'] = defectinfo['host_hof']
            data['host_spacegroup'] = str(defectinfo['host_spacegroup']) or ''
            data['host_pointgroup'] = str(defectinfo['host_pointgroup']) or ''
            data['host_layergroup'] = str(defectinfo['host_layergroup']) or ''
            data['host_lgnum'] = str(defectinfo['host_lgnum']) or ''
            # Defect-defect distance; 0 inplace of None:
            data['r_nn'] = defectinfo['R_nn'] or 0.0
        except FileNotFoundError:
            pass

    try:
        defect_symmetry = rrf('defect_symmetry')
        data['defect_pointgroup'] = defect_symmetry['defect_pointgroup']
    except FileNotFoundError:
        pass

    try:
        atoms = read_atoms(dir / 'structure.json')
    except FileNotFoundError:
        print('ERROR - structure.json not found:', dir)

    folder.mkdir(exist_ok=False, parents=True)
    AtomsData.from_atoms(atoms).write_json(folder / 'structure.json')

    # Copy result json-files:
    for name in result_files:
        result = dir / f'results-asr.{name}.json'

        if 'defects.pristine' in str(dir) and name == 'magstate':
            continue

        if result.is_file():
            shutil.copyfile(result, folder / result.name)

    # Remove None values:
    data = {key: value for key, value in data.items() if value is not None}

    (folder / 'data.json').write_text(json.dumps(data, indent=0))


if __name__ == '__main__':
    if len(sys.argv) == 1:
        create_qpod_uids(ROOT, PATTERNS)
        copy_materials(ROOT, PATTERNS)
    else:
        create_qpod_uids(Path(sys.argv[1]), sys.argv[2:])
        copy_materials(Path(sys.argv[1]), sys.argv[2:])
