"""Collect relevant information to fill in webpage templates quickly"""

LINKS = {
    'Haastrup2018': 'https://doi.org/10.1088/2053-1583/aacfc1',
    'Gjerding2021': 'https://doi.org/10.1088/2053-1583/ac1059',
    'Pakdel2024': 'https://www.nature.com/articles/s41467-024-45003-w',
    'Fu2024': "https://iopscience.iop.org/article/10.1088/2053-1583/ad3e0c",
    'COD': 'https://www.crystallography.net/cod/',
    'ICSD': 'https://icsd.products.fiz-karlsruhe.de/en/',
    'C2DB_MAT': 'https://c2db.fysik.dtu.dk/material/',
    'DOI': 'https://doi.org/',
    'OQMD': 'https://oqmd.org/materials/entry'
}

LINKED_PAPERS = {
    'Haastrup2018': f"""
    <a href={LINKS['Haastrup2018']}>S. Haastrup et al., The Computational 2D
    Materials Database: High-throughput modeling and discovery of atomically
    thin crystals, 2D Mater. 5 042002 (2018).</a><br>""".strip(),
    'Gjerding2021': f"""
    <a href={LINKS['Gjerding2021']}>M. N. Gjerding et al., Recent Progress of
    the Computational 2D Materials Database (C2DB), 2D Materials 8 044002
    (2021).</a><br>""".strip(),
    'Pakdel2024': f"""
    <a href={LINKS['Pakdel2024']}>S. Pakdel et al. High-throughput
    computational stacking reveals emergent properties in natural van der
    Waals bilayers, Nature Comm. 15, 932 (2024).</a><br>""".strip(),
    'Fu2024': f"""
    <a href={LINKS['Fu2024']}>Fu et al., Layer group classification of
    two-dimensional materials, 2D Mater. 11 035009 (2024).</a><br>""".strip()
}

INFO = {
    'pbe':
    """The single-particle band structure calculated with the PBE xc-functional
    including spin-orbit coupling (SOC). The color code shows the
    expectation value of the spin S<sub>i</sub> (where i=z for non-magnetic
    materials and otherwise is the magnetic easy axis). The effect of SOC on
    the band energies can be switching on/off.

    <br><br>Relevant articles:

    <br>Haastrup2018""",
    'pbe+u':
    """The single-particle band structure calculated with the PBE+U
    xc-functional including spin-orbit coupling (SOC). The color code shows the
    expectation value of the spin S<sub>i</sub> (where i=z for non-magnetic
    materials and otherwise is the magnetic easy axis). The effect of SOC on
    the band energies can be switching on/off.

    <br><br>Relevant articles:

    <br>Haastrup2018""",
    'hse':
    """The single-particle band structure calculated with the HSE06
    xc-functional. The calculations are performed non-self-consistently with
    the wave functions from a PBE calculation. Spin-orbit interactions are
    included.

    <br><br>Relevant articles:

    <br>Haastrup2018
    """,
    'gw':
    """The quasiparticle (QP) band structure calculated within the G0W0
    approximation from a PBE starting point. The treatment of frequency
    dependence is numerically exact. A truncated Coulomb interaction is used
    to avoid screening from periodic images. The QP energies are
    extrapolated as 1/N to the infinite plane wave basis set limit.
    Spin-orbit interactions are included.

    <br><br>Relevant articles:

    <br>Haastrup2018

    <br><a href=https://doi.org/10.1103/PhysRevB.94.155406>
    F. Rasmussen et al. Efficient many-body calculations for two-dimensional
    materials using exact limits for the screened potential: Band gaps of MoS2,
    h-BN, and phosphorene, Phys. Rev. B 94 155406 (2016).
    </a>"""}
