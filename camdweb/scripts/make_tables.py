from camdweb.cmr.projects import projects
from pathlib import Path
import csv


def main():
    for name, proj in projects.items():
        print(name)
        if not proj.column_descriptions:
            continue
        with (Path(name) / 'keytable.csv').open('w') as fd:
            writer = csv.writer(fd)
            writer.writerow(['key', 'description', 'unit'])
            for key, desc in proj.column_descriptions.items():
                desc, _, unit = desc.partition(' [')
                if unit:
                    unit = unit[:-1].replace('m<sub>e</sub>', ':math:`m_e`')
                    assert '<' not in unit
                writer.writerow([f'``{key}``', desc, unit])


if __name__ == '__main__':
    main()
