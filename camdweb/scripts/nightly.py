"""Test that all apps work: from raw test-data to web-apps."""

import argparse
import os
from pathlib import Path
from camdweb.c2db.copy import copy_materials
from camdweb.c2db.app import main as c2db_main
from camdweb.c2db.uid_translation import main as c2db_uid_translate
from camdweb.cmr.app import main as cmr_main
from camdweb.qpod.app import main as qpod_main
from camdweb.qpod.copy import copy_materials as copy_qpod_materials

NO_DATA_ERROR = """
Please copy test-data from sylg:~cmr/CAMD-Web-test-data/:

    $ scp -r sylg:~cmr/CAMD-Web-test-data/ .
"""


def test_c2db():
    data = Path('../CAMD-Web-test-data/C2DB')
    oqmd123 = data / 'oqmd123.json.gz'
    try:
        Path(oqmd123.name).symlink_to(oqmd123)
    except FileExistsError:
        pass
    copy_materials(data, ['*/*/*/'])
    app = c2db_main([str(path) for path in Path().glob('A*/')])
    test_pages(app)


def test_qpod():
    data = Path('../CAMD-Web-test-data/QPOD/tree-test')
    c2db_uid_translate()
    copy_qpod_materials(data, ['A*/*/*/defects.*/charge_*',
                               'A*/*/*/defects.pristine*'])
    app = qpod_main()
    test_pages(app)


def test_cmr():
    cmr_app = cmr_main([str(dbfile) for dbfile
                        in Path('../CAMD-Web-test-data/CMR').glob('*.db')])
    for name, app in cmr_app.project_apps.items():
        print(name)
        test_pages(app)


def test_pages(app):
    for i, material in zip(range(10), app.materials):
        print(i, material.uid)
        app.material_page(material.uid)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('project', choices=['c2db', 'cmr', 'bidb', 'qpod'])
    args = parser.parse_args()

    if not any(path.name.lower() == args.project
               for path in Path('CAMD-Web-test-data').glob('*/')):
        parser.error(NO_DATA_ERROR)

    dir = Path(args.project)
    dir.mkdir(exist_ok=True)
    os.chdir(dir)
    globals()[f'test_{args.project}']()


if __name__ == '__main__':
    main()
