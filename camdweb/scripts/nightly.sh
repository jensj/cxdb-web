set -e
cd /scratch/jensj
mkdir CAMD-Web-test
cd CAMD-Web-test
scp -r sylg:~cmr/CAMD-Web-test-data/ .
. /etc/bashrc
module load Python
python -m venv venv
. venv/bin/activate
pip install -U pip ase spglib
pip install -q git+https://gitlab.com/taskblaster/taskblaster.git
pip install -q git+https://gitlab.com/asr-dev/asr.git
git clone git@gitlab.com:camd/camd-web
(cd camd-web && pip install -e .)
export MPLBACKEND=AGG
mkdir c2db
(cd c2db && python -m camdweb.c2db.uid_translation)
for name in c2db cmr qpod
do
    python -m camdweb.scripts.nightly $name > $name.out
done
cd /scratch/jensj
rm -rf CAMD-Web-test-OK
mv CAMD-Web-test CAMD-Web-test-OK
