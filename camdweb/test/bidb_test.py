import os
from pathlib import Path

from ase import Atoms
from ase.db import connect

from camdweb.bidb.app import main
from camdweb.bidb.copy import copy_files
from camdweb.utils import check_as_workflow


def test_bidb(tmp_path):
    os.chdir(tmp_path)
    dbfile = tmp_path / 'bidb.db'
    f = str(tmp_path)
    with connect(dbfile) as db:
        atoms = Atoms('H2', [(0, 0, 0), (0.7, 0, 0)], pbc=(1, 1, 0))
        atoms.center(vacuum=1)
        db.write(atoms,
                 number_of_layers=2,
                 monolayer_uid='H-xyz',
                 bilayer_uid='H-xyz-stacking',
                 cod_id='A23462346',
                 extra=27,
                 layer_group='Some Group',
                 layer_group_number=144,
                 binding_energy_gs=0.015,  # eV/Å^2
                 binding_energy_zscan=0.0,
                 slide_stability='Stable',
                 folder=f)

    copy_files(dbfile)
    assert Path('wf.py').is_file()
    check_as_workflow(main, 100, '*/*', ['-p'])
    assert Path('paths.json').is_file()
    check_as_workflow(main, 100, '*/*', ['-t'])

    app = main(['A*/*'])
    app.index_page()
    html = app.material_page('1H-1-stacking')
    assert '15.000' in html  # meV/Å^2
