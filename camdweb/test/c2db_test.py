import gzip
import json
import os
import shutil

import pytest
from pathlib import Path
from ase import Atoms
from ase.calculators.singlepoint import SinglePointCalculator
from ase.db import connect

from camdweb.c2db.app import main
from camdweb.c2db.copy import copy_materials, main as copymain
from camdweb.utils import check_as_workflow
from camdweb.c2db.oqmd123 import db2json
from camdweb.test.check_html import check_html
from camdweb.test.generate_c2db_test_data import create_tree


@pytest.fixture
def oqmd_db_file(tmp_path_factory):
    tmp_path = tmp_path_factory.mktemp('tmp-oqmd')
    data = [('Mo', -11.202, 0.0),
            ('S48', -195.747, 0.0),
            ('Mo2S4', -44.212, -0.916)]
    path = tmp_path / 'oqmd123.db'
    db = connect(path)
    for formula, energy, hform in data:
        atoms = Atoms(formula)
        atoms.calc = SinglePointCalculator(atoms, energy=energy)
        db.write(atoms, hform=hform, uid=formula)
    gz = tmp_path / 'oqmd123.json.gz'
    db2json(path, gz)
    return gz


@pytest.fixture
def uid_translation_file(tmp_path_factory):
    tmp_path = tmp_path_factory.mktemp('tmp-uid')
    path = tmp_path / 'uid_translation.json'
    path.write_text('{"MnBr2-2afc82f66833": "1MnBr2-1"}')
    return path


def test_c2db_missing_phonons(tmp_path, uid_translation_file):
    create_tree(tmp_path)
    os.chdir(tmp_path)
    shutil.copy(uid_translation_file, '.')
    (tmp_path / 'MoS2/results-asr.phonons.json').unlink()
    (tmp_path / 'MoS2/results-asr.bader.json').unlink()

    copy_materials(tmp_path, ['MoS2*'], update_chull=False)
    # OK to do it again:
    copy_materials(tmp_path, ['MoS2*'], update_chull=False)

    with pytest.raises(FileNotFoundError):
        copy_materials(tmp_path, [])


def test_write_new_uid_files(tmp_path, uid_translation_file):
    create_tree(tmp_path)
    os.chdir(tmp_path)
    shutil.copy(uid_translation_file, '.')

    for write, tag in [(False, '1temp'),
                       (True, '1'),
                       (False, '1')]:
        copy_materials(tmp_path, ['MoS2*'], update_chull=False,
                       write_new_uid_files=write)
        uid = json.loads(
            (tmp_path / f'materials/AB2/1MoS2/{tag}/data.json'
             ).read_text())['uid']
        assert uid == f'1MoS2-{tag}'


def test_everything(oqmd_db_file):
    root = oqmd_db_file.parent
    create_tree(root)
    os.chdir(root)
    copymain([str(root), 'MoS2*'])

    # test the pages wf script
    assert Path('wf.py').is_file()
    check_as_workflow(main, 100, 'A*/*/*', ['-p'])
    assert Path('paths.json').is_file()
    check_as_workflow(main, 100, 'A*/*/*', ['-t'])

    app = main(['AB2/*/*'])
    assert len(app.materials) == 1
    app = main(['AB2/1MoS2/*/'])
    assert len(app.materials) == 1
    app = main(['AB2/1MoS2/1temp'])
    assert len(app.materials) == 1

    html = app.index_page()
    check_html(html)

    # Compress one of the result files:
    mat = root / 'materials/'
    bs = mat / 'AB2/1MoS2/1temp/results-asr.bandstructure.json'
    with gzip.open(bs.with_suffix('.json.gz'), 'wt') as fd:
        fd.write(bs.read_text())
    bs.unlink()

    html = app.material_page('1MoS2-1temp')
    key = 'Charges [|e|]'
    passed = key in html
    assert passed
    check_html(html)

    (mat / 'AB2/1MoS2/1temp/bader.json').unlink()
    (mat / 'AB2/1MoS2/1temp/results-asr.shift.json').unlink()
    html = app.material_page('1MoS2-1temp')
    passed = key not in html  # Bader charge
    assert passed

    # Test spontaneous polarization panel
    keys = ['<i>P<sub>x</sub></i>', '505.51']
    passed = all([key in html for key in keys])
    assert passed
