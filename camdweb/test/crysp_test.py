import os
from pathlib import Path
import pytest

from camdweb.crysp.app import main
from camdweb.crysp.copy import main as copymain
from camdweb.test.check_html import check_html
from camdweb.utils import check_as_workflow

DATA = """{
"uid": "2Si-1",
"gap": 0.5884620493557158,
"gap_dir": 2.5118970171822674,
"energy": -11.09091716443407,
"is_magnetic": false,
"n_avg": 3.5242182121375243,
"ehull": -0.0006226066647156969,
"hform": -0.0006226066647156969
}
"""
STRUCTURE = """
{"1":
     {"numbers": [14, 14],
      "positions": [[-0.4555, -0.6832, -0.4555], [0.9110, -2.0497, -1.8220]],
      "cell": [[2.733, -2.733, 0.0], [-2.733, 0.0, -2.733],
       [2.733, 2.733, 0.0]],
      "pbc": [true, true, true],
      "energy": -11.09091716443407}}
"""


@pytest.fixture
def cryspdata(tmp_path_factory):
    root = tmp_path_factory.mktemp('tmp-crysp')
    path = root / 'materials/A/2Si/1'
    path.mkdir(parents=True)
    (path / 'data.json').write_text(DATA)
    (path / 'structure.json').write_text(STRUCTURE)
    return root


def test_everything(cryspdata):
    os.chdir(cryspdata)
    copymain([str(cryspdata), 'Si2*'])

    assert Path('wf.py').is_file()
    pattern = 'A*/*/*/'
    check_as_workflow(main, 100, pattern, ['-p'])
    assert Path('paths.json').is_file()
    check_as_workflow(main, 100, pattern, ['-t'])

    app = main([pattern])
    assert len(app.materials) == 1
    app = main(['A/2Si/*/'])
    assert len(app.materials) == 1

    html = app.index_page()
    check_html(html)
    html = app.material_page('2Si-1')
    assert '0.588' in html  # band gap
