import pytest
import os
from camdweb.crystalbank.app import main

DATA = """{
"oqmd_entry_id": 588771,
"etot": -0.014277391139242677,
"gap": 8.620173558602595,
"gap_dir": 8.620173558602595,
"magstate": "NM",
"fmax": 2.2102264799446573e-29,
"smax": 0.003093088350535923
}
"""
STRUCTURE = """
{"1":
     {"cell": [[3.8, 0, 0], [1.9, 3.3, 0], [1.9, 1.1, 3.1]],
      "numbers": [18],
      "pbc": [true, true, true],
      "positions": [[0, 0, 0]],
      "initial_magmoms": [0.0]}}
"""


@pytest.fixture
def crystalbank(tmp_path_factory):
    root = tmp_path_factory.mktemp('tmp-xb')
    path = root / 'materials/A/1Ar/588771'
    path.mkdir(parents=True)
    (path / 'data.json').write_text(DATA)
    (path / 'structure.json').write_text(STRUCTURE)
    return root


def test_everything(crystalbank):
    os.chdir(crystalbank)
    app = main(['A/1Ar/*/'])
    assert len(app.materials) == 1
    app.index_page()
    html = app.material_page('1Ar-588771')
    assert '8.62' in html  # band gap
