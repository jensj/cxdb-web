from pathlib import Path

import ase.io.ulm as ulm
import numpy as np
from ase import Atoms
from ase.build import mx2
from ase.io.jsonio import encode
from ase.io.trajectory import write_atoms
from ase.calculators.singlepoint import SinglePointCalculator


def create_data(dir: Path, atoms: Atoms) -> None:

    # Create fake gpw-file:
    writer = ulm.Writer(dir / 'gs.gpw', tag='gpaw')
    with writer:
        writer.write(version=4)
        write_atoms(writer.child('atoms'), atoms)
        writer.child('results').write(energy=-5.9)
        wfs = writer.child('wave_functions')
        wfs.write(fermi_levels=np.array([-0.123]))

    atoms.write(dir / 'structure.json')

    # Create fake ASR result-files:
    (dir / 'results-asr.magstate.json').write_text(
        '{"is_magnetic": false, "magstate": "NM",'
        '"magmom": 0.0, "magmoms": null}')
    (dir / 'results-asr.magnetic_anisotropy.json').write_text(
        '{"spin_axis": "z", "dE_zx": 0.1, "dE_zy": 0.2}')
    (dir / 'results-asr.c2db.labels.json').write_text(
        '{"label": "fake structure origin"}')
    (dir / 'bravais_type.json').write_text(
        '{"bravais_type": "Hexagonal (op)"}')
    (dir / 'results-asr.structureinfo.json').write_text(
        """{"has_inversion_symmetry": false,
            "layergroup": "p-6m2",
            "lgnum": 78,
            "spacegroup": "P-6m2",
            "pointgroup": "-6m2",
            "spgnum": 187,
            "spglib_dataset":
                {"rotations":{"__ndarray__":[
                                  [1, 3, 3],
                                  "int32",
                                  [1,0,0,0,1,0,0,0,1]]}}}""")
    (dir / 'results-asr.gs.json').write_text(
        """{"kwargs": {"data": {"gap": 1.8,
                                "gap_dir": 1.8,
                                "gap_dir_nosoc": 1.9,
                                "k_cbm_c": [0.0, 0.0, 0.0],
                                "k_vbm_c": [0.1, 0.0, 0.0],
                                "vbm": 0.5,
                                "cbm": 2.3,
                                "evac": 4.5,
                                "efermi": 1.5,
                                "gaps_nosoc": {"vbm": 0.5, "cbm": 2.5}}}}""")
    (dir / 'results-asr.gs@calculate.json').write_text(
        '{}')
    (dir / 'results-asr.database.material_fingerprint.json').write_text(
        '{"kwargs": {"data": {"uid": "MoS2-b3b4685fb6e1"}}}')

    # Band-structure:
    for name in ['results-asr.bandstructure.json',
                 'results-asr.phonons.json']:
        (dir / name).write_text(
            (Path(__file__).parent / name).read_text())

    # Bader:
    (dir / 'results-asr.bader.json').write_text("""
    {"kwargs":
      {"data":
       {"bader_charges":
        {"__ndarray__": [[3], "float64", [1.24, -0.62, -0.62]]},
        "sym_a": ["Mo", "S", "S"]}}}""")

    # Shift:
    freqs = np.linspace(0, 10, 11)
    sigma = freqs * (1 + 0j)
    dct = {
        'symm': {'zero': 'xxx=xxz=...', 'yyy': 'yyy=-xyx=-yxx=-xxy'},
        'sigma': {'yyy': sigma},
        'freqs': freqs}
    (dir / 'results-asr.shift.json').write_text(encode(dct))

    # Spontaneous polarization
    dct = {
        'Ferroelectric': True,
        'Px': 505.51,
        'Py': 10.01,
        'Pz': -40.0,
        'P': np.linalg.norm([505.51, 10.01, -40.0])}
    (dir / 'results-asr.spontaneous_polarization.json').write_text(encode(dct))

    # Berry phase
    dct = {
        'phi0_km': np.array(([0.0, 1.0, 2.0], [1.0, 2.0, 3.0])),
        's0_km': np.array(([3.0, 4.0, 5.0], [4.0, 5.0, 6.0]))}
    (dir / 'results-asr.berry@calculate.json').write_text(encode(dct))

    (dir / 'results-asr.berry.json').write_text('{"Topology": "Not checked"}')


def create_tree(dir: Path):
    path = dir / 'MoS2'
    path.mkdir()
    atoms = mx2('MoS2')
    atoms.center(vacuum=5, axis=2)
    atoms.calc = SinglePointCalculator(atoms, energy=-5.9)
    create_data(path, atoms)


if __name__ == '__main__':
    create_tree(Path())
