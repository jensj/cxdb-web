from pathlib import Path

from ase import Atoms
from ase.build import mx2


def create_data(dir: Path, atoms: Atoms) -> None:

    if 'v_Mo' in str(dir):
        atoms.pop(2)  # remove one Mo atom

    atoms.write(dir / 'structure.json')

    # Create fake ASR result-files:
    # Making all files in each folder for now, but can be changed
    # to only make the files that are needed in each folder
    (dir / 'results-asr.magstate.json').write_text(
        '{"kwargs": {"data": {"magstate": "NM", "is_magnetic": false}}}')
    (dir / 'results-asr.magnetic_anisotropy.json').write_text(
        '{"spin_axis": "z"}')
    (dir / 'results-asr.structureinfo.json').write_text(
        """{"has_inversion_symmetry": false,
            "layergroup": "p-6m2",
            "lgnum": 78,
            "spglib_dataset":
                {"rotations":{"__ndarray__":[
                                  [1, 3, 3],
                                  "int32",
                                  [1,0,0,0,1,0,0,0,1]]}}}""")
    (dir / 'results-asr.gs.json').write_text(
        """{"kwargs": {"data": {"gap": 2,
                                "gap_dir": 2,
                                "gap_dir_nosoc": 2.1,
                                "k_cbm_c": [0.0, 0.0, 0.0],
                                "k_vbm_c": [0.1, 0.0, 0.0],
                                "evac": 4.5,
                                "efermi": 1.5,
                                "gaps_nosoc": {"vbm": 0.5, "cbm": 2.5}}}}""")

    if 'pristine' in str(dir):
        (dir / 'results-asr.database.material_fingerprint.json').write_text(
            '{"kwargs": {"data": {"uid": "MoS2-fakeuid4pris"}}}')

        # ChargeNeutrality:
        (dir / 'results-asr.charge_neutrality.json').write_text(
            """{"kwargs": {"data": {
                "scresults": [{"kwargs": { "data": {
                    "condition": "Mo-poor",
                    "efermi_sc": 1.5,
                    "n0": 0.2,
                    "p0": 0.2,
                    "defect_concentrations": [{"kwargs": {"data": {
                        "defect_name": "v_Mo",
                        "concentrations": [[8.92e-6, 0, 5.93],
                                           [2.21e-5, 1, 6.32]]}}}],
                    "dopability": "intrinsic"
                    }}}],
                "temperature": 300.0,
                "conc_unit": "cm^-2",
                "gap": 1.66}}}""")

    if 'v_Mo' in str(dir):
        # Slater-Janak:
        (dir / 'results-asr.sj_analyze.json').write_text(
            """{"kwargs": {"data": {
                "eform": [[7.3, 0], [7.2, 1]],
                "transitions": [{"kwargs": {"data": {
                    "transition_name": "0/1",
                    "transition_values": {"kwargs": {"data":
                        {"transition": -1.5,
                        "erelax": -0.1,
                        "evac": -0.2}}}}}}],
                "pristine": {"kwargs": {"data": {
                    "vbm": 0.5,
                    "cbm": 2.5,
                    "evac": 4.5}}},
                "standard_states": [
                    {"kwargs": {"data": {"element": "v", "eref": 0.0}}},
                    {"kwargs": {"data": {"element": "Mo", "eref": -11}}}
                    ],
                "hof": -0.9}}}""")
        # Defect-Symmetry:
        (dir / 'results-asr.defect_symmetry.json').write_text(
            """
            {"kwargs": {"data":
            {
                "defect_pointgroup": "D3h",
                "defect_name": "v_Mo",
                "symmetries":
                [{"kwargs": {"data":
                {
                    "irreps": [
                        {"kwargs": {"data": {
                                        "sym_name": "A'1",
                                        "sym_score": 0.05}}
                         },
                        {"kwargs": {"data": {
                                        "sym_name": "A'2",
                                        "sym_score": 0.05}}
                         },
                        {"kwargs": {"data": {
                                        "sym_name": "E'1",
                                        "sym_score": 0.7}}
                         },
                        {"kwargs": {"data": {
                                        "sym_name": "A''1",
                                        "sym_score": 0.05}}
                         },
                        {"kwargs": { "data": {
                                        "sym_name": "A''2",
                                        "sym_score": 0.05}}
                         },
                        {"kwargs": {"data": {
                                        "sym_name": "E''1",
                                        "sym_score": 0.05}}}],
                    "best": "E'1",
                    "error": 0.9,
                    "loc_ratio": 5,
                    "state": "1",
                    "spin": "0",
                    "energy": -5.5}}},
                 {"kwargs": {"data": {
                    "irreps": [{"kwargs": {"data": {
                                               "sym_name": "A'1",
                                               "sym_score": 0.05}}
                                },
                               {"kwargs": {"data": {
                                               "sym_name": "A'2",
                                               "sym_score": 0.05}}
                                },
                               {"kwargs": {"data": {
                                               "sym_name": "E'1",
                                               "sym_score": 0.7}}
                                },
                               {"kwargs": {"data": {
                                               "sym_name": "A''1",
                                               "sym_score": 0.05}}
                                },
                               {"kwargs": { "data": {
                                               "sym_name": "A''2",
                                               "sym_score": 0.05}}
                                },
                               {"kwargs": {"data": {
                                               "sym_name": "E''1",
                                               "sym_score": 0.05}}}],
                    "best": "E'1",
                    "error": 0.9,
                    "loc_ratio": 5,
                    "state": "1",
                    "spin": "1",
                    "energy": -5.5}}}],
                "pristine": {"kwargs": {"data": {
                                            "vbm": 0.5,
                                            "cbm": 2.5,
                                            "gap": 2.0}}}}}}""")
        if 'charge_0' in str(dir):
            (dir / 'uid.json').write_text(
                '{"uid": "MoS2fake.v_Mo.0.1",'
                '"olduid": "MoS2-fakeuid4charge0"}')
            (dir / 'results-asr.database.material_fingerprint.json'
             ).write_text(
                 '{"kwargs": {"data": {"uid": "MoS2-fakeuid4charge0"}}}')

            # get_wfs:
            (dir / 'results-asr.get_wfs.json').write_text(
                """{"kwargs": {"data": {
                    "wfs": [
                        {"kwargs": {"data": {
                            "state": 1, "spin": 0, "energy": -5.5}}},
                        {"kwargs": {"data": {
                            "state": 1, "spin": 1, "energy": -5.5}}}],
                    "above_below": [false, true],
                    "eref": 4.4}}}""")

            # DefectInfo:
            (dir / 'results-asr.defectinfo.json').write_text(
                """{"kwargs": {"data": {
                    "defect_name": "v_Mo",
                    "charge_state": "(charge 0)",
                    "host_gap_pbe": 2.0,
                    "host_gap_hse": 2.1,
                    "host_hof": -0.92,
                    "host_uid": "MoS2-fake-olduid-c2db",
                    "host_name": "MoS2",
                    "host_crystal": "AB2-187-bi",
                    "host_spacegroup": "P-6m2",
                    "host_pointgroup": "-6m2",
                    "host_layergroup": "p-6m2",
                    "host_lgnum": 78,
                    "R_nn": 15.0}}}""")
        if 'charge_1' in str(dir):
            (dir / 'uid.json').write_text(
                '{"uid": "MoS2fake.v_Mo.+1.1",'
                '"olduid": "MoS2-fakeuid4charge1"}')
            (dir / 'results-asr.database.material_fingerprint.json'
             ).write_text(
                 '{"kwargs": {"data": {"uid": "MoS2-fakeuid4charge1"}}}')

            # get_wfs:
            (dir / 'results-asr.get_wfs.json').write_text(
                """{"kwargs": {"data": {
                    "wfs": [
                        {"kwargs": {"data": {
                            "state": 1, "spin": 0, "energy": -5.5}}},
                        {"kwargs": {"data": {
                            "state": 1, "spin": 1, "energy": -5.8}}}],
                    "above_below": [false, true],
                    "eref": 4.8}}}""")
            # DefectInfo:
            (dir / 'results-asr.defectinfo.json').write_text(
                """{"kwargs": {"data": {
                    "defect_name": "v_Mo",
                    "charge_state": "(charge 1)",
                    "host_gap_pbe": 2.0,
                    "host_gap_hse": 2.1,
                    "host_hof": -0.92,
                    "host_uid": "MoS2-fake-olduid-c2db",
                    "host_name": "MoS2",
                    "host_crystal": "AB2-187-bi",
                    "host_spacegroup": "P-6m2",
                    "host_pointgroup": "-6m2",
                    "host_layergroup": "p-6m2",
                    "host_lgnum": 78,
                    "R_nn": 15.0}}}""")


def create_tree(dir: Path):
    dir_names = ['tree-test/AB2/187/AB2-187/defects.MoS2_000.v_Mo/charge_0',
                 'tree-test/AB2/187/AB2-187/defects.MoS2_000.v_Mo/charge_1',
                 'tree-test/AB2/187/AB2-187/defects.pristine_sc']
    for dir_name in dir_names:
        path = dir / dir_name
        path.mkdir(parents=True, exist_ok=True)
        atoms = mx2('MoS2')
        atoms.center(vacuum=5, axis=2)
        atoms.repeat((2, 1, 1))
        create_data(path, atoms)


if __name__ == '__main__':
    create_tree(Path())
