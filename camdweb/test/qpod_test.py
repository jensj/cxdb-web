from pathlib import Path
import os
from camdweb.qpod.app import main
from camdweb.qpod.copy import copy_materials
from camdweb.test.qpod import create_tree
from camdweb.utils import check_as_workflow


def test_everything(tmp_path_factory):
    root = tmp_path_factory.mktemp('tmp-qpod')
    path_tree = Path(root / 'tmp-qpodtree')
    path_tree.mkdir()
    path_app = Path(root / 'tmp-qpodapp')
    path_app.mkdir()

    # Creating a fake uid_translation.json file
    uid_translation = path_app / 'uid_translation.json'
    uid_translation.write_text(
        '{"MoS2-fake-olduid-c2db": "MoS2-fakeuidc2db"}')

    create_tree(path_tree)
    os.chdir(path_app)

    # with patch('builtins.input', return_value='yes'):
    copy_materials(path_tree, ['tree-*/A*/*/*/defects.*/charge_*',
                               'tree-*/A*/*/*/defects.pristine*'])
    assert Path('wf.py').is_file()
    pattern = '*/*/charge*'
    check_as_workflow(main, 1, pattern, ['-p'])
    assert Path('paths.json').is_file()
    check_as_workflow(main, 10, pattern, ['-t'])

    app = main([pattern])
    assert len(app.materials) == 2
    html = app.material_page('MoS2fake.v_Mo.0.1')
    assert 'Defect-defect distance' in html  # Summary panel check
    assert 'Intrinsic doping type' in html  # ChargeNeutralityPanel check
    assert 'v_Mo (0/1)' in html  # SJPanel check
#    assert 'Symmetry' in html  # DefectSymmetryPanel check
