from camdweb.html import table, complex_table_head

table_test_result = """<div class="table-responsive">
 <table class="table table-striped">
  <thead>
   <tr {col_spacing}>
    <th class="w-50" rowspan="2">Atom index</th>
    <th class="w-50" rowspan="2">Atom type</th>
    <th class="w-50" colspan="2">Local moment</th>
   </tr>
   <tr {col_spacing}>
    <th class="w-50">spin</th>
    <th class="w-50">orbital</th>
   </tr>
  </thead>
  <tbody>
   <tr class="w-50">
    <td class="w-50">0</td>
    <td class="w-50">Mn</td>
    <td class="w-50">4.3</td>
    <td class="w-50">6.5</td>
   </tr>
   <tr class="w-50">
    <td class="w-50">1</td>
    <td class="w-50">Fe</td>
    <td class="w-50">1.3</td>
    <td class="w-50">2.5</td>
   </tr>
  </tbody>
 </table>
</div>"""


def test_table():

    header = [['Atom index_span=1,2', 'Atom type_span=1,2',
               'Local moment_span=2,1'], ['spin', 'orbital']]
    head = complex_table_head(header)
    table_content = table(head, [[0, 'Mn', 4.3, 6.5], [1, 'Fe', 1.3, 2.5]])
    assert (table_content == table_test_result)
