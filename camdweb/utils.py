from __future__ import annotations

import json
import multiprocessing as mp
import re
from contextlib import contextmanager
from pathlib import Path

import numpy as np
from ase import Atoms
from ase.data import chemical_symbols
from ase.io import read

from camdweb import ColVal
from camdweb.references import LINKS


def formula_dict_to_string(count: dict[str, int]) -> str:
    """Convert dict to string representation.

    >>> formula_dict_to_string({'H': 2, 'O': 1})
    'H2O'
    """
    s = ''
    for symbol, c in count.items():
        s += symbol
        if c > 1:
            s += str(c)
    return s


def html_format_formula(f: str) -> str:
    """Convert formula string to HTML.

    >>> html_format_formula('H2O')
    'H<sub>2</sub>O'
    """
    return re.sub(r'(\d+)', r'<sub>\1</sub>', f)


def fft(atomic_numbers: list[int] | np.ndarray) -> tuple[dict[str, int],
                                                         str, str, str]:
    """Fast formula-transformations.

    Return dict mapping chemical symbols to number of chemical symbols.
    Three dicts are returned:

    >>> fft([1, 1, 8, 1, 1, 8])
    ({'O': 2, 'H': 4}, 'O2H4', 'OH2', 'AB2')

    full, reduced and stoichiometry.

    (We need to do this a million times and the ase.formula module is
    too slow).
    """
    values, counts = np.unique(atomic_numbers, return_counts=True)
    nunits = np.gcd.reduce(counts)
    symbols = [chemical_symbols[v] for v in values]
    count = {symbol: int(c)
             for c, symbol in sorted((c, symbol)
                                     for symbol, c in zip(symbols, counts))}
    reduced = {symbol: c // nunits for symbol, c in count.items()}
    abc = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    stoichiometry = {abc[i]: c
                     for i, (symbol, c) in enumerate(reduced.items())}
    return (count,
            formula_dict_to_string(count),
            formula_dict_to_string(reduced),
            formula_dict_to_string(stoichiometry))


def read_atoms(path: Path | str) -> Atoms:
    atoms = read(path)
    assert isinstance(atoms, Atoms)
    return atoms


def hyperref(href: str, text: str) -> str:
    return f'<a href="{href}">{text}</a>'.strip()


def c2db(value: ColVal, link: bool = False) -> str:
    assert isinstance(value, str)
    if link:
        return hyperref(f'{LINKS["C2DB_MAT"]}{value}', value)
    return value


def doi(value: ColVal, link: bool = False) -> str:
    """Create HTML anchor link to DOI.

    >>> doi('10.1103/ABC.95.216', True)
    '<a href="https://doi.org/10.1103/ABC.95.216">10.1103/ABC.95.216</a>'
    >>> doi('10.1103/ABC.95.216', False)
    '10.1103/ABC.95.216'
    """
    assert isinstance(value, str)
    return hyperref(f'{LINKS["DOI"]}{value}', value) if link else value


def cod(value: ColVal, link: bool = False) -> str:
    assert isinstance(value, (str, int))
    if link:
        return hyperref(f'{LINKS["COD"]}{value}.html', f'COD {value}')
    return str(value)


def icsd(value: ColVal, link: bool = False) -> str:
    assert isinstance(value, (str, int))
    if link:
        return hyperref(LINKS["ICSD"], str(value))
    return str(value)


def oqmd(id, link=False):
    if link:
        return hyperref(f'{LINKS["OQMD"]}/{id}', id)
    return id


class NoPool:
    def imap_unordered(self, worker, work, chunksize=1):
        for args in work:
            yield worker(args)


@contextmanager
def process_pool(processes: int):
    if processes == 1:
        yield NoPool()
    else:
        yield mp.Pool(processes=processes)


def thickness(atoms: Atoms) -> float:
    return np.ptp(atoms.positions[:, 2])


def all_dirs(root: Path,
             patterns: list[str]) -> list[Path]:
    return [dir
            for pattern in patterns
            for dir in root.glob(pattern)
            if dir.name[0] != '.']


def check(n: int, N: int, app):
    names = json.loads(Path('paths.json').read_text())
    myapp = app([name.split('/', 1)[1] for name in names[n::N]])
    for material in myapp.materials:
        print(material.uid)
        myapp.material_page(material.uid)


def check_as_workflow(app, N: int, pattern, argv: list[str] | None = None):
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--paths', help='Regenerate paths.json',
                        default=False, action='store_true')
    parser.add_argument('-t', '--test', help='Test a single material',
                        default=False, action='store_true')
    args = parser.parse_args(argv)

    paths = Path('paths.json')
    if not paths.is_file() or args.paths:
        names = [str(p) for p in Path().glob(f'materials/{pattern}')]
        Path('paths.json').write_text(json.dumps(names))

    if args.test:
        check(n=0, N=N, app=app)
