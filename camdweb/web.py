"""Base web-app class."""
import json
import csv
import subprocess
from collections import defaultdict
from functools import partial
from io import BytesIO, StringIO
from pathlib import Path

from ase.io import write
from bottle import (TEMPLATE_PATH, Bottle, HTTPError, request, static_file,
                    template)

from camdweb.html import FormPart, SearchFilter, Select, table
from camdweb.materials import Materials
from camdweb.panels.panel import SkipPanel
from camdweb.session import Sessions

TEMPLATE_PATH[:] = [str(Path(__file__).parent)]

ROBOTS = """\
User-agent: *
Disallow: /
"""


class CAMDApp:
    title = 'CAMD'
    logo = ''
    links = [
        ('CMR', 'https://cmr.fysik.dtu.dk')]
    home = ''

    def __init__(self,
                 materials: Materials,
                 initial_columns: list[str],
                 *,
                 initial_filter_string: str = '',
                 root: Path | None = None):
        self.materials = materials
        self.root = root or Path()

        self.route()

        # For updating plots:
        self.callbacks = self.materials.get_callbacks()

        # User sessions (selected columns, sorting, filter string, ...)
        self.sessions = Sessions(initial_columns,
                                 filter_string=initial_filter_string)

        self.form_parts: list[FormPart] = [SearchFilter()]

        # For nspecies selection:
        maxnspecies = max(len(material.count) for material in self.materials)
        self.form_parts.append(
            Select('Number of chemical species:', 'nspecies',
                   [''] + [str(i) for i in range(1, maxnspecies + 1)],
                   ['-'] + [str(i) for i in range(1, maxnspecies + 1)]))

        self.hits: defaultdict[str, int] = defaultdict(int)

    def route(self):
        self.app = Bottle()
        self.app.route('/')(self.index_page)
        self.app.route('/table')(self.table_html)
        self.app.route('/material/<uid>')(self.material_page)
        self.app.route('/callback')(self.callback)
        self.app.route('/png/<path:path>')(self.png)
        self.app.route('/favicon.ico')(self.favicon)
        self.app.route('/static/styles.css')(self.static)
        self.app.route('/reload/<code:int>/<x:int>')(self.reload)
        self.app.route('/stats')(self.stats)
        self.app.route('/robots.txt')(lambda: ROBOTS)

        for fmt in ['xyz', 'cif', 'json']:
            self.app.route(f'/material/<uid>/download/{fmt}')(
                partial(self.download, fmt=fmt))

        self.app.route('/downloadable/<path:path>/<file>')(
            self.download_datafile)

    def download_datafile(self, path: str, file: str) -> bytes | str:
        filepath = Path(path) / file
        # we don't typically write csv files, so when csv files are
        # requested, we assume we are converting a json to csv
        if not filepath.with_suffix('.json').is_file():
            return HTTPError(body=f"File '{file}' not found in {path}")

        if file.endswith('.json'):
            try:
                return filepath.read_text()
            except json.JSONDecodeError:
                return HTTPError(body=f"File '{file}'is not valid JSON")

        if file.endswith('.csv'):
            data = json.loads(filepath.with_suffix('.json').read_text())
            buffer = StringIO()
            array_keys = [k for k in data.keys() if isinstance(data[k], list)]
            writer = csv.DictWriter(buffer, fieldnames=array_keys)
            writer.writeheader()
            for i in range(len(data[array_keys[0]])):
                row = {key: data[key][i] for key in array_keys}
                writer.writerow(row)
            content = buffer.getvalue()
            buffer.close()
            return content

        return HTTPError(body=f"File '{file}' invalid downloadable format")

    def download(self, uid: str, fmt: str) -> bytes | str:
        try:
            atoms = self.materials[uid].atoms
        except KeyError:
            return HTTPError(body='Bad request')

        if fmt == 'json':
            return json.dumps({'1': atoms.as_dict()}, indent=1)

        if fmt == 'xyz':
            # Only the extxyz writer includes cell, pbc etc.
            fmt = 'extxyz'

        # (Can also query ASE's IOFormat for whether bytes or str,
        # in fact, ASE should make this easier.)
        buf: BytesIO | StringIO = BytesIO() if fmt == 'cif' else StringIO()

        write(buf, atoms.atoms_object(), format=fmt)
        return buf.getvalue()

    def index_page(self) -> str:
        """Page showing table of selected materials."""
        query = request.query
        session = self.sessions.get(int(query.get('sid', '-1')))
        search = '\n'.join(fp.render() for fp in self.form_parts)
        table = self.table_html(session)
        sidebar = self.persistent_sidebar()
        return template('index.html',
                        title=self.title,
                        search=search,
                        session=session,
                        table=table,
                        sidebar=sidebar)

    def table_html(self, session=None) -> str:
        """Get HTML for table."""
        if session is None:
            query = request.query
            try:
                sid = int(query.get('sid'))
            except (ValueError, TypeError):
                return HTTPError(body='Bad request')
            session = self.sessions.get(sid)
            if 'filter' in query:
                filter_string = self.get_filter_string(query)
                session.update(filter=filter_string)
            else:
                session.update(query=query)
        rows, header, pages, new_columns, error = self.materials.get_rows(
            session)
        summary_string = pages.summary(len(self.materials))
        return template('table.html',
                        session=session,
                        pages=pages,
                        rows=rows,
                        summary_string=summary_string,
                        header=header,
                        new_columns=sorted(new_columns, key=lambda x: x[1]),
                        error=error)

    def get_filter_string(self, query: dict) -> str:
        """Generate filter string from URL query.

        Example::

            {'filter': Cu=1,gap>1.5',
             'stoichiometry': 'AB2',
             'nspecies': ''}

        will give the string "Cu=1,gap>1.5,stoichiometry=AB2".
        """
        filters = []
        for s in self.form_parts:
            filters += s.get_filter_strings(query)
        return ','.join(filters)

    def material_page(self, uid: str) -> str:
        """Page showing one material."""
        sidebar = self.persistent_sidebar()
        if uid not in self.materials:
            return HTTPError(body='No such material!')
        material = self.materials[uid]
        self.hits[uid] += 1
        webpanels = []
        for panel in self.materials.panels:
            try:
                data = panel.create_panel_data(material)
            except SkipPanel:
                continue
            webpanels.append(data)

        return template('material.html',
                        title=uid,
                        panels=webpanels,
                        sidebar=sidebar)

    def persistent_sidebar(self):
        """Provide persistent sidebar for all pages."""
        return template('sidebar.html',
                        logo=self.logo,
                        home=self.home,
                        sidebar_links=self.links)

    def callback(self) -> str:
        """Send new json data.

        Currently only used for the atoms-plot.
        """
        query = request.query
        try:
            name = query['name']
            uid = query['uid']
        except KeyError:
            return HTTPError(body='Bad callback!')
        material = self.materials[uid]
        return self.callbacks[name](material, int(query['data']))

    def png(self, path: str) -> bytes:
        """Return binary data for png-figures."""
        return static_file(path, self.root)

    def static(self, filename='/static/styles.css'):
        return static_file(filename, Path(__file__).parent)

    def favicon(self) -> bytes:
        path = Path(__file__).with_name('favicon.ico')
        return static_file(path.name, path.parent)

    def reload(self, code: int, x: int) -> str:
        py = self.root / 'reload.py'
        if py.is_file():
            result = subprocess.run(
                ['python3', str(py), str(code), str(x)],
                capture_output=True)
            return result.stdout.decode()
        return ''

    def stats(self):
        """Show some statistics."""
        return table(
            None,
            [[f'<a href="/material/{uid}">{uid}</a>', n]
             for n, uid
             in sorted(((n, uid) for uid, n in self.hits.items()),
                       reverse=True)])
